%%
%% $Id: integration.tex,v 1.29 2005-06-14 15:43:41 hehi Exp $
%%
\chapter{Forward Detector integration in \ALICE{}}
\label{cha:integration}

\noindent
The Forward Detector integration and installation procedure is driven
by the consideration that in \ALICE{}, in contrast with other \LHC{}
experiments, the bake-out of the central part of the beam pipe will
not be possible once the Inner Tracking System (\ITS{}) and Forward
Detectors (\FWD{}) are installed. The beam pipe bake-out will in fact most
probably be needed not only at the installation of the experiment but
also rather regularly in the long shut-downs, in order to guarantee 
high vacuum quality during the whole operation period. Furthermore,
access to the \ITS{} and many of the \FWD{} and their services
will be impossible once the \TPC{} is in place (standard position).
This is particularly the case on the \RB{26} side, where the detectors
are completely hidden between the \ITS{} and the muon arm.

Consequently, it must be foreseen that the \ITS{} and \FWD{} 
will be removed from the operating position and re-installed 
at regular intervals both for
their maintenance and for beam pipe bake-out.
This requires not only appropriate design, but also
the definition of a detailed and precise sequence in their
installation and removal. \ITS{} and \FWD{} are parts of the
same mechanical complex; therefore some reference will be made 
to the \ITS{} and its services, although they are, strictly, outside the
scope of this TDR.

Another particularity of \ALICE{} is the presence of the Muon Arm on
one side of the Interaction Point (IP) and the resulting asymmetry
of the experimental set--up. Consequently, the \FWD{} situated at
the \RB{24} (shaft) side are different in position and
configuration to the corresponding detectors on the \RB{26}
(muon arm) side.

One of the essential technical targets of
\ALICE{} is a high level of thermal protection of the \TPC{} from heat sources
in its environment. Particular care must therefore be taken in the
design of the cooling and ventilation system in the volume inside
the \TPC{} inner cylinder. No liquid cooling is, however, currently
foreseen for the \FWD{} because the heat dissipation of these
detectors is small compared to the dissipation from the \ITS{}
cables, for which a solution based on a general ventilation of
the full volume inside the \TPC{} is being sought.

None of the \FWD{} are gas detectors.  Services described here
thus comprise only cables, optical fibres and possibly air
ventilation ducts.

\section{Common Design Features}

Needing to be able to bake out the beam pipe without the
presence of the \FWD{} means that these detectors will have to be
mounted and remounted while the central beam pipe is installed. Thus,
all the Forward Detectors and their support structures will be divided
in half and assembled around the beam pipe. A radial clearance
to the beam pipe must be respected according to following criteria:
\begin{itemize}
\item In those cases where the beam pipe and a detector are supported by
  the same mechanical structure, a radial clearance of \unit[5]{mm}
  will be respected between them, with due consideration of all the design
  and fabrication tolerances.

\item Where the beam pipe and a detector are supported by
  different mechanical structures, a radial clearance of
  \unit[10]{mm} will be observed between them, again considering all
  design and fabrication tolerances.
\end{itemize}

\section{Mechanical support}

\subsection{\RB{24} Side}

As shown in \figsref{fig:integration:1} and \ref{fig:integration:1a},
both halves of \TZERO{A}, \VZERO{A}, and the \FMD{1}{} are mounted on
a cantilevered carbon fibre structure fixed to the vacuum valve
support. The valve, its support and the central vacuum pipe are all
held by a mechanical structure bolted to the Service Support Wheel
(SSW) of the \TPC{}.  Although the deflection of the cantilevered
support under the weight of the three detectors is calculated to be
below $\unit[1]{mm}$, two arms will connect the free edge of the
support to the structure to compensate the deflection and avoid
dangerous vibrations.

\FMD{2}{} is mounted inside two half-cylinders fixed to the \ITS{} 
mechanical structure. The second support of the central beam pipe is 
attached to the same cylinder by four stretched wires behind \FMD{2}{}.

Both support systems secure the detectors in a fixed position with
respect to the beam pipe.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{integration/figures/integration_1}
  \caption{Three--dimensional drawing of the Mini Frame and Baby Frame
    structures that support the \TZERO{A} (hidden behind the PMD),
    \VZERO{A}, and \FMD{1}{} detectors and their services. The cables
    and optical fibres exit the detectors radially and connect to
    shoeboxes mounted on the periphery of the Mini Frame.}
  \label{fig:integration:1}
\end{figure}


\subsection{\RB{26} Side}

\VZERO{C} is directly fixed to the front face of the muon absorber
while \TZERO{C} is mounted on a short cantilevered carbon fibre
cylindrical structure bolted to the front face of the muon absorber
and protruding through the inner circumference of \VZERO{C}.

\FMD{3}{} is mounted inside the two halves of the same carbon fibre
conical structure to which the first support of the central beam
pipe is also fixed by four stretched wires. 
The half-cones are mounted on the \ITS{} support structure.

While the muon absorber supports the beam pipe inside its own
volume, bellows allow small movements relative to the vacuum pipe
at the \ITS{} and \FMD{3}{} positions. The fixture of the beam
pipe to the \FMD{}{} cone and the \ITS{} allows for smaller tolerances
between these objects and a fixed relative alignment.

\section{Services}

The services (power cables, signal cables, and optical fibres) are
identical for the Forward Detectors installed on either side
of the IP.\ However, the routing of these services are significantly
different.

\subsection{\RB{24} Side}

Apart from \FMD{2}{}, all services on the \RB{24} side are
concentrated on the Mini Frame (MF) structure of \ALICE{}, which also
supports the PMD detector, as shown in \figref{fig:integration:1a}.
The cables for \FMD{2}{} follow the channels of the \ITS{} services
until they also connect to the MF.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{integration/figures/integration_5}
  \caption{Details of the cabling for the Forward Detectors on the
    RB24 side. Together with the \ITS{} services, the cables from
    \FMD{2}{} follow in the cylindrical and conical channels and
    connect to shoeboxes on the Mini Frame structure. Cables and
    optical fibres from \FMD{1}{}, \VZERO{A} and \TZERO{A} exit
    radially towards their shoeboxes near the periphery of the Mini
    Frame.}
  \label{fig:integration:1a}
\end{figure}

\subsubsection*{\TZERO{A}}

The power and signal cables from the 12 photomultipliers of \TZERO{A} are
connected first to a patch panel mounted above the detector on the
Mini Frame and subsequently to the \TRD{} shoebox. Here, the signals are
split and sent to both the proper \TZERO{A} electronics placed
outside the \LTHREE{} magnet, and to the \TRD{} electronics to provide its
wake--up signal.

\subsubsection*{\VZERO{A}}

For \VZERO{A}, the optical fibres connected to the 32 scintillating 
sections are grouped in eight units of four bundles, each following the octagonal
structure of the detector. Each bundle is connected to one of the
32 photomultipliers, themselves grouped into eight units of four and
mounted to the Mini Frame structure. All fibres are adjusted to have an
identical length, and kept shorter than \unit[5]{m} to minimize
signal attenuation.
The power and signal cables for the photomultipliers are similarly routed in
four groups of eight to four shoeboxes mounted on the Mini Frame.
As for \TZERO{A}, the signals are split in the shoeboxes and sent
to two different electronic systems: the proper \VZERO{A}
electronics placed outside the \LTHREE{} Magnet and the \TRD{} detector
shoebox to provide its wake--up signal.

\subsubsection*{\FMD{1}{}}

Cables connect the \FMD{1}{} detector to a shoebox containing the RCU
circuit and the patch panel for power and signal cables. The shoebox
is fixed to the Mini Frame below \FMD{1}{} at a distance
less than \unit[3]{m} from the digitizer circuits on the detector.
From the shoebox, cables connect to a patch panel mounted on the
outer part of the Mini Frame. Cables connecting from this patch panel to
the \FMD{}{} electronics outside the \LTHREE{} magnet are attached to
the Baby Space Frame (BSF), from where they hang as flexible and
short pigtails.

\subsubsection*{\FMD{2}{}}

\FMD{2}{} uses the \ITS{} service support. The cables are grouped 
at the bottom of the detector support and
connected to the patch panel mounted on the \ITS{} structure
as flexible and short pigtails. From the patch panel, the
cables are routed through the \FMD{}{} duct and emerge as flexible
and short pigtails, to be connected to the \FMD{2}{} shoebox
mounted on the Mini Frame.
A new section of cables connects this shoebox to a patch panel
on the outer part of the Mini Frame. The final cabling that reaches the
outside electronics is attached to BSF, from where they will again hang
as flexible and short pigtails.

\subsection{\RB{26} Side}

On the \RB{26} side, all services have to pass through narrow ducts
positioned along the conical surface of the muon absorber. On the tip
of the absorber, near the \FWD{} and \ITS{} detectors, patch panels
allow (dis)connection of the cables and pipes at this point
during detector installation. \figref{fig:integration:4} shows the
layout of the patch panels and ducts on the circumference of the
absorber.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{integration/figures/integration_4}
  \caption{Overview of the patch panel and cable duct distribution on
    the surface of the muon absorber near its front face.}
  \label{fig:integration:4}
\end{figure}

\subsubsection*{\VZERO{C}}

\VZERO{C} has a total of 96 fibre bundles, distributed in four groups of
24 each, coming out of the detector at \unit[45]{${}^{\circ}$} with
respect to the $x$ and $y$ axes (see \figref{fig:integration:4}). 
The four groups are channelled directly
into four ducts mounted on the muon absorber (see \figref{fig:integration:2})
and reserved exclusively
for the \VZERO{C}; they reach four boxes placed at the end of the
ducts, containing eight PMs each. All the fibres have an identical length
of less than \unit[5]{m}.  The power and signal cables are connected
to the PMTs of each box, whose services are routed to four
shoeboxes mounted on the muon absorber support.  In analogy to the
\VZERO{A}, the signals are split and sent both to the \VZERO{C}
electronics outside the \LTHREE{} magnet and to the \TRD{}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{integration/figures/integration_2}
  \caption{Muon absorber on the \RB{26} side, showing the placement of the
    \VZERO{C} services patch panels near the detector, the PM boxes
    and the patch panels for connection outside the \LTHREE{} magnet.}
  \label{fig:integration:2}
\end{figure}


\subsubsection*{\TZERO{C}}

The \TZERO{C} services coming from the 12 photomultipliers are
connected to a patch panel mounted almost on top of the muon absorber,
then to the shoebox, where the signals are split and sent to
the \TZERO{C} electronics placed outside the \LTHREE{} magnet and to the
\TRD{} electronics to provide its wake--up. \figref{fig:integration:3}
shows the position of the patch panels and shoebox on the muon absorber.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{integration/figures/integration_3}
  \caption{Muon absorber on the \RB{26} side, showing the placement of the
    \TZERO{C} patch panel near the detector and the shoebox with 
    a patch panel for the connection to the outside of the \LTHREE{} magnet. 
    Also shown is the position of the \FMD{3}{} patch panel and the shoebox
    containing the \RCU{} module.}
  \label{fig:integration:3}
\end{figure}

\subsubsection*{FMD}

The cabling scheme of \FMD{3}{} is constrained by several
parameters, including:
\begin{itemize}
\item the only available place for the \FMD{}{} patch panel and duct
  is near the bottom of the muon absorber;
\item the maximum length of the cables carrying the signals from
  the \FMD{}{} digitizers to the RCU is \unit[3]{m}; and
\item each of the \FMD{}{} halves must be mounted in its
  half conical carbon fibre support and completely pre--cabled before its
  installation in the set--up.
\end{itemize}

For these reasons, the cables coming from the two halves of
\FMD{3}{} first merge together at the horizontal edge of their
junction, and are then routed around the border of their conical
support to the \FMD{}{} patch panel and duct.
From the patch panel, the cables follow their duct up to the
\FMD{}{} shoebox placed at the end of the duct, just outside the
\TPC{} (see \figref{fig:integration:3}), 
in order to keep the signal cable lengths close to the
desired \unit[3]{m} (see \figref{fig:integration:3}). From the
shoebox, power and signal cables continue their path to the
\FMD{}{} electronics outside the \LTHREE{} magnet.

\section{Installation Sequence}

The sequence of installation steps of the forward detectors and
the \ITS{} is complex and has been explored in detail through a
series of 3D installation drawings and in full-scale mock-ups.
\Figref{fig:fmd:integration:sequence} and
\ref{fig:fmd:integration:sequence_cont} show selected steps of the
installation procedure of those Forward Detectors that are situated inside
the \TPC{} inner cylinder, i.e. \VZERO{C}, \TZERO{C}, \FMD{3}{}
and \FMD{2}{}. Some details follow below.

\begin{sidewaysfigure}[htbp]
  \centering
  \includegraphics[width=.45\textwidth]{fmd/figures/install1}
  \includegraphics[width=.45\textwidth]{fmd/figures/install2}
  \includegraphics[width=.45\textwidth]{fmd/figures/install3}
  \includegraphics[width=.45\textwidth]{fmd/figures/install4}
  \caption{Installation sequence, left to right, top to bottom
    order. Continued on \figref{fig:fmd:integration:sequence_cont}.}
  \label{fig:fmd:integration:sequence}
\end{sidewaysfigure}
\begin{sidewaysfigure}[htbp]
  \centering
  \includegraphics[width=.45\textwidth]{fmd/figures/install5}
  \includegraphics[width=.45\textwidth]{fmd/figures/install6}
  \includegraphics[width=.45\textwidth]{fmd/figures/install7}
  \includegraphics[width=.45\textwidth]{fmd/figures/install8}
  \caption{Installation sequence, left to right, top to bottom
    order. Continued from \figref{fig:fmd:integration:sequence}.}
  \label{fig:fmd:integration:sequence_cont}
\end{sidewaysfigure}

\subsection{\RB{26} Side}

First, the beam pipe is installed with a temporary support structure
and equipment for bake--out. Then \VZERO{C} and \TZERO{C} are mounted
on the absorber nose and a temporary holder of the beam pipe is
installed. This allows the lower half of the \FMD{3}{} cone, already
equipped with detectors, to be installed and attached to the temporary
beam support structure. When the upper half of the cone and \FMD{3}{}
is in place, the wire mechanism for the beam pipe support can be
attached and the beam pipe position fine adjusted. The \ITS{} is now
pushed in (first the two pixel layers and then the two drift and strip
layers) and the temporary beam support structure is removed.

These are the details of the installation sequence of the services:
\begin{enumerate}
\item Install the four sectors of the \ITS{} and \FWD{} services
with pre--cabled ducts, patch panels, PM supports and shoeboxes on
the muon absorber and its support structure.

\item Connect detector shoeboxes to the electronics outside the
\LTHREE{} magnet.

\item Install the central beam pipe on a temporary support and
bake--out. Remove the bake--out jackets.

\item Assemble the two halves of the \TZERO{C} support around the
beam pipe and fix the cylindrical structure to the front face of
the muon absorber.

\item Assemble the two halves of the \VZERO{C} detector around the
\TZERO{C} support and fix the combined structure to the front face
of the muon absorber.

\item Assemble the two halves of the \TZERO{C} detector around its
support and fix it to this support.

\item Connect the optical fibre bundles to the feed-through fixed
around the \VZERO{C}. Assemble the fibre bundles in four groups at
\unit[45]{${}^{\circ}$} with respect to the $x$ and $y$ axes. Pass
each group of fibres into the dedicated duct mounted below the
\SPD{} patch panels. Connect the fibre bundles to the
photo-multipliers mounted in the PM boxes.

\item Connect the \TZERO{C} services to their patch panel.

\item Install the two halves of \FMD{3}{} with its conical support
structure.

\item Regroup and connect the two bundles of services to the
\FMD{}{} patch panel.

\item Connect and adjust the four wires for the beam pipe support
on the conical support.
\end{enumerate}

\subsection{\RB{24} Side}

After the \ITS{} installation, the support structure for \FMD{2}{}
and the beam pipe support on the \RB{24} side can be installed.
Finally, a support cover for the \ITS{} cables is installed and
the \TPC{} can be rolled in, enclosing \VZERO{C}, \TZERO{C},
\FMD{3}{}, \ITS{} and \FMD{2}{}. \FMD{1}{} will be mounted
together with \PMD{}, \TZERO{A} and \VZERO{A} on the Mini Frame
structure.

\begin{enumerate}
\item After the installation of the \ITS{} on its provisional support,
  mount the \FMD{2}{} on the cylinder to which the second beam pipe
  support is fixed.

\item After the introduction of the pre-cabled \ITS{} service
support, the \FMD{2}{} services are connected to their patch panel
on this support.

\item After the final installation of \TPC{}, its Service Support
Wheel (SSW), and the \ITS{}, the fixation of the \ITS{} services to
the SSW, and the connection of the central beam pipe to the vacuum
valve support, the two halves of the \TZERO{A}, \VZERO{A}, and
\FMD{1}{} will be mounted on a common support structure.

\item Next, the Mini Frame is equipped with \ITS{} and \FWD{} shoeboxes,
patch panels and pre-cabled services is introduced.

\item The \FMD{2}{} services (pigtails), emerging from their duct
(near the bottom of the conical service support structure), are
now connected to their shoebox on the Mini Frame (together
with the \ITS{} services).

\item Finally, the \FMD{1}{}, \VZERO{A} and \TZERO{A} services are
connected to the respective shoeboxes on the Mini Frame.

\end{enumerate}

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  tex borge FMD TDR digitizer

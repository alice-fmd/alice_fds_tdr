%%
%% $Id: performance.tex,v 1.2 2005-07-18 09:57:09 hehi Exp $
%%
\section{Performance simulations}
\label{sec:V0:perfsim}
\CVSKeyword{$Id: performance.tex,v 1.2 2005-07-18 09:57:09 hehi Exp $}
\CVSIdShow


The evaluation of the performance of the \VZERO{} detector was made
using \ALIROOT{} based on \GEANT[3]. Minimum-bias \ppCol{} reactions
were simulated at $E_{beam} = \unit[7]{TeV}$ with the generator
\PYTHIA{}~6.214~\cite{V0:pythia}. The corresponding total reaction
cross-section is \unit[101]{mb}. It is the sum of the elastic
(\unit[22]{mb}) and inelastic (\unit[79]{mb}) cross-sections. The
elastic interactions do not produce particles that interact with the
\VZERO{} detector, which has a limited coverage at small angles.  This
component is therefore not included in the present calculations.  The
\PbPbCol{} reaction was simulated at \unit[5.5]{TeV}/nucleon with the
generator \HIJING{}~\cite{V0:hijing}.

\subsection{Background from secondaries in \ppCol{} collisions}
\label{sec:V0:secondaries}

The presence of matter (beam pipe, front absorber, \FMD{}{}, \TZERO{},
ITS services) in front of the \VZERO{} arrays results in an important
number of secondaries, which will distort physical information
expected from primary charged particles.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.60\textwidth]{v0/figures/V0vertex}

    \includegraphics[width=.40\textwidth]{v0/figures/V0beampipe}
    \caption{Origin of particles detected by the \VZERO{} arrays and projected
      onto the $xy$, $zx$ planes, and along the negative beam $z$-direction
      (top). Geometry of the beam pipe area between the vertex and the
      \VZERO{C} array (bottom).}
    \label{fig:V0:V0vertex}
  \end{center}
\end{figure}

\Figref{fig:V0:V0vertex} shows the origin of the charged particles,
projected in the $xy$ and $zx$ planes of \ALICE{}, that contribute to
the signals of the \VZERO{} arrays, which are exposed to a large
quantity of secondary particles (\VZERO{C} much more than the
\VZERO{A}). The figure shows that they come mainly from the beam pipe
and bellows (\unit[0]{cm}~$>z>-$\unit[80]{cm}) and from the flange in
front of the detector ($z<-$\unit[80]{cm}). The geometry of the setup
at the time of the simulation is shown in the same figure. An
important change in the beam pipe design would be necessary to
drastically reduce the secondary particle contribution.

\subsection{Trigger efficiency for MB \ppCol{} physics}
\label{sec:V0:triggerefficiency}

The \VZERO{} detector will provide the triggering of the largest
possible fraction of \ppCol{} reactions. In this section, we calculate
the expected efficiency of the system, taking into account the
geometrical coverage given in \tabref{tab:V0:geom}. Only the inelastic
component of the \ppCol{} cross-section will contribute.

\begin{figure}[htbp]
  \begin{center}

    \includegraphics[width=.40\textwidth]{v0/figures/effV0200432channels}
    \includegraphics[width=.40\textwidth]{v0/figures/V0effcell}
    \caption{\VZERO{} triggering efficiencies for \ppCol{} 
      physics determined by \PYTHIA{}. Transport of particles in
      vacuum (light grey) and in the \ALICE{} environment (dark grey).
      Charged-particle multiplicity distribution of events in full
      space (white) as compared to distributions of events seen by the
      \VZERO{A}, the \VZERO{C}, and both \VZERO{A} and \VZERO{C}
      (left). Distribution of cells fired in the \VZERO{A}, the
      \VZERO{C}, and both the \VZERO{A} and \VZERO{C} arrays~(right).}
    \label{fig:V0:V0effic} 
  \end{center}
\end{figure}

\Figref{fig:V0:V0effic}~(left) shows the multiplicity distribution of
the charged particles produced by the inelastic \ppCol{} interactions
over the $4\pi$ phase space. The figure also shows the corresponding
distributions of events with all \ALICE{} effects (with \VZERO{} only)
giving at least one charged particle in the \VZERO{A} array, in the
\VZERO{C} array and in both \VZERO{A} and \VZERO{C} arrays, the condition
required for a minimum-bias triggering. Trigger efficiencies are about
\unit[89]{\%} (\unit[85]{\%}), \unit[87]{\%} (\unit[84]{\%}) and \unit[83]{\%} (\unit[77]{\%}) for the detection of at least
one charged particle by \VZERO{A}, \VZERO{C} and both \VZERO{A} and
\VZERO{C} arrays. Thus, the material  improves the capability of
triggering, especially for events with low outgoing multiplicity.

Triggering with only the left or the right \VZERO{} device would have
to be interesting in terms of efficiency. About \unit[7]{\%} could be
gained (\figref{fig:V0:V0effic}~(left)). Unfortunately, owing to the
large expected background from p-gas interactions
(\secref{sec:V0:pgasrejection}), such a trigger configuration is not
advantageous. The background from p-gas interactions must be reduced
by selecting events with proper time of flight to the two detector
arrays. There is a difference of about \unit[6]{ns}  between
real \ppCol{} events and events associated to p-gas interactions. A
possible residual pollution of physics data by the background will
have to be disentangled off-line.  If these polluting events make the
acquisition rates prohibitive, a threshold on the minimum number of
cells fired in the \VZERO{A} or (and) \VZERO{C} array(s) could be
required for the filtering of this background.

\Figref{fig:V0:V0effic}~(right) shows the distributions of events as a
function of the number of cells ($N_{\text{cell}}$) fired in the
\VZERO{A}, \VZERO{C}, and both \VZERO{A} and \VZERO{C} arrays. We note
a large increase of fired cells due to secondaries generated in
material in front of the detector. Any cut on $N_{\text{cell}}$
would reduce the triggering efficiency of the \VZERO{} detector for
the smallest multiplicity events.

In \PbPbCol{} reactions the efficiency of triggering depends on the
required centrality. It is very close to \unit[100]{\%} for all but the very
peripheral part of the minimum-bias events. Moreover, the contribution of
Pb-gas background is expected to be very weak.

\subsection{Multiplicity distributions in \ppCol{} and \PbPbCol{} collisions}
\label{sec:V0:bbmultiplicity}

\subsubsection{\ppCol{} reactions}
\label{sec:V0:pp-multiplicity}

The multiplicity measurement is not the main aim of the \VZERO{}
detector.  In fact, this detector is not optimized for the
individual counting of charged particles. Moreover, the production of
many secondaries (\secref{sec:V0:secondaries}) will distort physical
information about multiplicity.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.6\textwidth]{v0/figures/V0ringmult}
    \caption{Charged-particle multiplicity distributions in \ppCol{}
      reaction through each ring of the \VZERO{A} and \VZERO{C} arrays
      as given by 7820 \PYTHIA{} inelastic events after transport of
      particles in vacuum
      (light grey) and in the \ALICE{} environment (dark grey).}
    \label{fig:V0:V0ppmult}
  \end{center}
\end{figure}

\Figref{fig:V0:V0ppmult} shows the multiplicity distributions in
\ppCol{} reaction as they are detected by each ring of the \VZERO{A} and
\VZERO{C} arrays. The comparison between results from \PYTHIA{} with
particles transported in vacuum and with all the \ALICE{}
environment shows the intensity of the background effect and its
dependence with the ring number. As expected, ring~1 of \VZERO{C}
is the most affected. The mean numbers of hits are 10 (20) for each
ring (ring 1 of \VZERO{C}). The mean number of MIPs hitting an
individual counter of \VZERO{C} is thus about 1 (2). The corresponding
numbers of hits on the \VZERO{A} individual counters are similar.

\subsubsection{\PbPbCol{} reaction}
\label{sec:V0:PbPb-multiplicity}

The pseudo-rapidity distribution of charged particles in central
\PbPbCol{} collisions ($0>b>\unit[5]{fm}$) is given in
\figref{fig:V0:dnsurdeta}.  At $\eta=0$, $dN/d\eta\approx3800$.  The
charged particles emitted inside the acceptance angles of the four
\VZERO{A} and \VZERO{C} rings vary from 1000 to 1600 and from 1400 to
1800, respectively.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.6\textwidth]{v0/figures/dNdEtaHijing}
    \caption{Pseudo-rapidity distribution of charged particles
      calculated with \HIJING{} for central \PbPbCol{}  collisions
      ($0>b>\unit[5]{fm}$).}  \label{fig:V0:dnsurdeta}
  \end{center}
\end{figure}

The \PbPbCol{} collisions were simulated using 18~\HIJING{} events with
impact parameter varying from 0 to \unit[11.2]{fm}. The results are
given in \figref{fig:V0:V0pbpbmult}~(top).  They show the correlation
between the pure (S) and the effective signals (S+N). The background
intensity is similar for all the rings of the detector except for
ring~1 of \VZERO{C} where it is two times larger, as observed in \ppCol{}
collisions. A maximum of 4000 (8000) charged particles will fire each
ring of the detector (ring~1 of \VZERO{C}). A MIP dynamics of at
least 500 for each channel is proposed to be implemented in the
electronics system (\secref{sec:V0:electronics}).

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.6\textwidth]{v0/figures/SNvsSPbPb32channels}
    \includegraphics[width=.4\textwidth]{v0/figures/SB}
    \caption{Charged particle multiplicity distributions in \PbPbCol{}
      reactions in  each ring of the \VZERO{A} (circle) and
      \VZERO{C} (triangle) arrays as given by 18~\HIJING{}
      events. Pure signal (S along x-axis) as a function of effective
      signal (S+N along y-axis). A perfect setup (N~=~0) is
      represented by the dotted line (top). Signal over Signal~+~Background
      (S/S+N) ratio as a function of the pseudo-rapidity range covered
      by the rings (bottom).}
    \label{fig:V0:V0pbpbmult}
  \end{center}
\end{figure}

Finally, the ratio of the integrated S/S+N values is plotted in
\figref{fig:V0:V0pbpbmult}~(bottom) for each ring.  Any improvement of
the setup should be explored to increase these values.


\subsection{p-gas rejection in MB \ppCol{} physics}
\label{sec:V0:pgasrejection}

A large number of particles are expected to be produced by
interactions of accelerated protons with the residual gas in the
\LHC{} vacuum chamber. They originate from the \ALICE{} experimental
area ('close p-gas', between $+$\unit[20]{m} and $-$\unit[20]{m} from
the IP) and from the \LHC{} ring (`halo'). They were simulated with
the \HIJING{} code~\cite{V0:benjamin} and with the file established for the \LHC{}b
experiment~\cite{V0:lhcb}, respectively. Their total rates were estimated at
\unit[198]{kHz} and \unit[47]{kHz}. Their contributions to the
\VZERO{} signal (\VZERO{A} and \VZERO{C} in geometrical coincidence)
can be found in~\cite{V0:benjamin}. The
\ALICE{} experiment is expected to run in \ppCol{} mode with two
different luminosities ($3\times10^{30}$ and 30 times
lower~\cite{V0:ppr}). In the high-luminosity case, the interaction
rate will be \unit[200]{kHz}, of the same order of magnitude as the
beam-gas. In the case of the low-luminosity running, the rate of
beam-gas events will be many times superior since the proton beams
will have an unchanged intensity in the accelerator.

\begin{figure}[hbt]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{v0/figures/pppgasmult}
    \hspace{1cm} \includegraphics[width=0.45\textwidth]{v0/figures/pgasdiff2}
    \caption{Absolute time difference between \VZERO{A} and \VZERO{C} arrays as
      a function of the multiplicity per event for \ppCol{} minimum-bias collision and p-gas
      interaction (left). Algebraic time difference between \VZERO{A} and \VZERO{C} arrays 
calculated with a gaussian time function (broken line) and compared to
a time function (continuous line) close to the one shown in~\figref{fig:V0:timeresolutionmex}~(right).}
    \label{fig:V0:pppgastime}
  \end{center} 
\end{figure}

The \VZERO{} detector should be able to discriminate p-gas and
\ppCol{} events by measuring the time-of-flight difference between the
\VZERO{A} and \VZERO{C} arrays~\cite{V0:cuautle}. This time difference
was estimated in both \ppCol{} and p-gas events by taking into account
the time given by the first hit in \VZERO{A} and \VZERO{C}. We
generated \ppCol{} minimum-bias collisions with a Gaussian vertex
smearing ($\sigma_z$~=~\unit[5.3]{cm}). For p-gas events we used the
generated events as described above. For each counter channel, we
applied a Gaussian time smearing of $\sigma_{time}=\unit[1]{ns}$
according to the \VZERO{} scintillator time resolution requirement. We
show in \figref{fig:V0:pppgastime}~(left) the absolute value of the
time-of-flight difference
as a function of the multiplicity per event~\cite{V0:benjamin}. Two
clusters appear. The first belongs to p-gas events and is centred at
about \unit[14]{ns}. It originates from the `halo' at low
multiplicity and from the `close' p-gas at high multiplicity. The
second one comes from \ppCol{} events at about \unit[8]{ns}.  Its
multiplicity distribution is centred at around 20.

It has to be noticed that, if distributions were plotted as a function
of the algebraic time, only half of the background events would appear at
\unit[6]{ns} above the physics events centred at \unit[8]{ns}. It concerns
the background events coming from the C-side of the apparatus. The
background events coming from the opposite direction (from
the A-side) would appear below the physics events, at about
\unit[-22]{ns}, providing no difficulty for its
identification. Moreover, the time distribution is not perfectly
gaussian shaped (see \figref{fig:V0:timeresolutionmex}~(right)). The algebraic
time distribution simulated with the inclusion of this last effect and
compared to the one with a gaussian shape is showed in
\figref{fig:V0:pppgastime}~(right) where beam `halo' contribution of
\unit[47]{kHz} is not included. A cut at \unit[11]{ns} leads to
\unit[99]{\%} of rejected background triggers in the case of a
calculation with gaussian functions. The more realistic time
distribution shape does not decrease a lot the background rejection
efficiency which is only lowered by around \unit[1]{\%}. This small
effect can be understood by the fact that the timing from the
\VZERO{A} and \VZERO{C} arrays is given by the counter which is the
first one delivering the signal. Larger is the multiplicity in the
arrays, earlier is the response of the array, weaker is the tail
effect of the time distribution provided by each channel. In other
words, the tail effect which is maximum for multiplicity~1 provides a
very weak broadening of the time distribution when folded in the
physics case. We can also check that the \ppCol{} time distribution is
broadened more than the p-gas one, indicating that the mean
multiplicity in the first process is smaller than the multiplicity in
the second one.
%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%% End:

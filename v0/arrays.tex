%%
%% $Id: arrays.tex,v 1.3 2005-08-15 11:09:35 hehi Exp $
%%
\section{The \VZERO{} arrays}
\label{sec:V0:V0arrays}
\CVSKeyword{$Id: arrays.tex,v 1.3 2005-08-15 11:09:35 hehi Exp $}
\CVSIdShow


\subsection{R\&D}
\label{sec:V0:V0retd}

Tests were carried out to determine the light yield and the time
resolution from several samples of counters made of scintillator
coupled with WLS~fibres. A test bench was used to record data from
cosmic rays and beam particles. It is described in Ref.~\cite{V0:benjamin}.
Initial measurements obtained with several assemblies of scintillating
elements are presented in Ref.~\cite{V0:V0note2}. These results allowed 
preliminary options of scintillating materials, assembling
geometry and method, surface treatment, etc.\ to be chosen so that a
maximum of light yield at the end of the fibres
for an optimized time resolution can be achieved.

Two different couplings (design~1 and design~2) of the scintillator piece
with the WLS fibres were finally tested. Each design and its resulting
performance are given in the next two sections.

\subsubsection{Design 1 and performance}
\label{sec:V0:V0design1}

For the \VZERO{A} array which will have 32~cells as shown in
\figref{fig:V0:V0segmentation} except for the fact that rings~3 and 4
will not be subdivided, the 45$^\circ$ slices will be independent
while the ring subdivision within a slice will be done by using the
`megatile' construction method developed and used in Refs.~\cite{V0:kim}
and~\cite{V0:star}.

In the `megatile' technique a large piece of scintillator is machined
with a router plane most of the way through its depth in order to separate
one sector from the rest, in this case the ring boundaries in a
slice. The grooves are filled with TiO$_2$ loaded epoxy to provide
optical insulation from the adjoining sectors, and reflection of light
into the cell, as well as to restore the mechanical strength. The
outer surfaces of the slice will be wrapped with an efficient
reflector, in our case Teflon tape.  In order to collect the light
within each detection cell, parallel grooves \unit[3]{mm} deep are
machined on each face of the cell with a pitch of \unit[10]{mm}. The
grooves will be filled with wavelength shifting optical fibres. These
fibres run radially towards the outer edge of the megatile where they
are coupled to clear fibres using optical connectors.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.5\textwidth]{v0/figures/timeresolutionmex}
    \includegraphics[width=.45\textwidth]{v0/figures/V0Ctimedistribution}
    \caption{The time resolution extracted from a single
      run. 2-dimensional plot of the number of
      photo-electrons versus the time of arrival of the pulse (left
      upper part). Extracted time resolution for individual bins
      (left lower part). The points can be very well fitted
      by $\sigma_{time}=2.55/\sqrt{N_{p.e.}}$. Time distribution
      shapes for five MIP charge bins (right upper part) and the
      integrated MIP charge distribution (right lower part). The $\sigma$ values come
      from a fit of the distributions with a gaussian function. }
  \label{fig:V0:timeresolutionmex}
  \end{center}
\end{figure}

The proposed materials to fulfil the required performance are:

\begin{itemize}
\item \unit[2]{cm} thick scintillator Bicron BC404,
\item \unit[1]{mm} diameter WLS Bicron BC9929AMC,
\item \unit[1.1]{mm} diameter clear fibre Bicron BCF98MC.
\end{itemize}

In the preliminary design we used \unit[10]{mm} BC404 and
\unit[1]{m} clear fibre that were fused to the WLS ones. However, the
fusing of fibres would not be practical in the ALICE environment. Our
tests with beams at the CERN PS and with cosmic rays indicate that the
performance i.e. the number of photo-electrons ($N_{p.e.}$) detected
and the time resolution depend crucially on the reflective layer on
the scintillator. Comparing Tyvek, aluminium foil and Teflon tape, we
found that the latter gave the best results.

\subsubsection*{Time resolution vs $N_{p.e.}$}

Minimum-ionized particles were used to establish the dependence of the
time resolution on the number of photo-electrons detected with an
XP2020 photo-multiplier. Using the fact that the charge collected by
the PMT is distributed over a large range according to a Landau
distribution, we have divided the charged spectrum in 5~bins and have
for each bin the corresponding time distribution. Using that
information, we have obtained the plot of ($N_{p.e.}$) versus the time
resolution $\sigma_{t}$ as shown in \figref{fig:V0:timeresolutionmex}~(left).
The resulting points can be very well fitted with a curve given by
$\sigma_{t}=~2.55/\sqrt{N_{p.e.}}$.

\subsubsection*{WLS fibres}

During the tests we have used mostly BC9929A single clad WLS
fibres. However, we have also tested Bicron BCF92MC (multi-clad) WLS
fibres. These latter were found to be inferior in light yield by about
\unit[20]{\%} compared to the BCF92. We are currently testing the performance
of a batch of BC9929AMC and the first measurements on a test bench
with a blue diode indicate a light yield about \unit[50]{\%} superior to the
BCF92MC one.

\subsubsection*{Clear fibres}

Since $\sigma_{t}$ depends strongly on ($N_{p.e.}$) we have measured
the attenuation length of the clear fibre. To carry out the
measurement, a WLS fibre was fused to a clear fibre. The WLS fibre was stimulated
with a blue LED and the light transmitted at the end of the clear
fibre was measured with a photodiode. We found that for a combination
BFC92/BFC98 the attenuation length is \unit[8.5]{m} while for a
combination Y11/BCF98 the attenuation length is \unit[7.5]{m}.


\subsubsection*{General performance}

Prototypes similar to the initial configuration (30$^\circ$ slices) of
each one of the rings have been tested. The results obtained can be
summarized in the following way:

\begin{itemize} 
\item clear fibres longer than \unit[5]{m} will not allow a resolution
  $\sigma_{t}$ better than \unit[1]{ns},
\item after \unit[3]{m} of clear fibre and optical connector we
  obtained a light collection of \unit[29]{p.e.} with a time resolution
  of \unit[490]{ps} (after jitter time correction).
\end{itemize}

A \VZERO{A} prototype of the tiles in ring 1-3 in the final configuration,
except for the length of the clear optical fibres which were \unit[2]{m}
instead of  the required \unit[3]{m}, were tested at the PS in September
2004. The results show that on the average
28~photo-electrons are detected in ring 1, 34 in ring 2 and 42 in ring
3. The time resolution was in all three cases better than
\unit[600]{ps}, with rings 1 and 2 being slightly better than ring
3. In every case, the HV value supplied to the PMT was above \unit[1600]{V}.

\subsubsection{Design 2 and performance}
\label{sec:V0:V0design2}

\subsubsection*{General tests}

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[height=0.3\textwidth]{v0/figures/V0Cdesign}
    \hspace{2cm}
    \includegraphics[height=0.3\textwidth]{v0/figures/V0Cringtested}
    \caption{Single counter with the fibres embedded along the two
      radial edges of the scintillator counter~(left). Dimensions of the
      scintillator block used for the tests~(right).}
    \label{fig:V0:V0Cdesign}
  \end{center} 
\end{figure}

The counter corresponding to this design was manufactured from a
single scintillator piece, part of a \VZERO{} sector, coupled with two
layers of WLS fibres glued along its two radial edges as shown in
\figref{fig:V0:V0Cdesign} where the dimensions are also given. The
components used for these measurements were as follows:

\begin{itemize}
\item \unit[2]{cm} thick scintillator BC408 or BC404 from
  Bicron~\cite{V0:bicron},
\item \unit[1.0]{mm} diameter WLS fibres Y11~(double cladding) from
  Kuraray~\cite{V0:kuraray} or BCF9929A~(single cladding or double
  cladding) from Bicron,
\item \unit[1.1]{mm} diameter optical fibres BCF98~(d.c.) from
  Bicron,
\item connector for nine passages to transmit the light from the WLS to
  the optical fibres,
\item Teflon film as envelop of the scintillator piece and the WLS
  fibres,
\item aluminium coating as reflector at the end of the WLS fibres,
  opposite to the PMT,
\item BC600 optical cement for embedding the WLS fibre layers in the
  scintillator block,
\item BC630 silicone optical grease for an optimization of the light
  transmission from WLS to optical fibres.
\end{itemize}

Minimum-ionizing particles (MIP) were used to evaluate the
performances of the counter. The time distributions for five MIP
charge bins are shown in~\figref{fig:V0:timeresolutionmex}~(right). We observe
that their shapes are not perfectly
Gaussian. In~\figref{fig:V0:V0Ctimelight}~(left) the
time resolution is plotted as a function of the light yield for two
types of components which are listed above, BC408/Y11~(d.c.) and
BC408/BCF9929A~(s.c.). These systems were measured with several lengths
of optical fibre BCF98~(d.c.), from 1 to \unit[16]{m}.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[height=0.45\textwidth]{v0/figures/V0Ctimelight}
    \includegraphics[height=0.5\textwidth]{v0/figures/V0CtimeHV}
    \caption{Time resolution as a function of light yield measured at
      the end of an optical fibre beam of several lengths (1, 5, 10 and
      \unit[16]{m}) and for a few types of scintillator/WLS fibre
      coupling (left). Time resolution given by the final selected
      scintillator/WLS fibre coupling as a function of HV value
      applied to the photo-multiplier (right). The geometry of tested
      counters was the one described in \figref{fig:V0:V0Cdesign}.}
    \label{fig:V0:V0Ctimelight}
  \end{center}
\end{figure}

The following observations can be extracted from the results:

\begin{itemize}
\item The BCF9929A~(s.c.) fibre is less efficient than the Y11~(d.c.)
  fibre as far as the light yield is concerned. Nevertheless, owing
  to its short decay time (\unit[2.5]{ns}) it provides a better
  time resolution than the Y11~(d.c.) fibre (decay time of
  \unit[7]{ns}).
\item For a given combination of materials, the time resolution varies
  like 1/$\sqrt{N_{p.e.}}$. A time resolution of 0.6, 0.8, 1.3, \unit[1.7]{ns} and 0.9,
  1.1, 1.6, \unit[2.3]{ns} can be achieved at the end of 1, 5, 10
  and \unit[16]{m} of optical fibres with the counter made of
  BC408/BCF9929A~(s.c.) and BC408/Y11~(d.c.), respectively. The time
  resolution of \unit[1]{ns} from the MIP can be achieved only if
  the transport distance of the light is shorter than \unit[5]{m} and
  \unit[1]{m} respectively. This observation shows clearly the
  necessity of installing photo-multipliers inside the \LTHREE{} magnet at
  a distance shorter than or equal to \unit[5]{m}.
\end{itemize}

These results can be improved when special care is taken in the choice
of counter components. For example, the association of BC404 with
BCF9929A~(d.c.) provides a larger light yield and a better time
resolution at the end of the optical fibre beams. This improvement is
due to a better matching between the wavelength of
the maximum emission light of the scintillator (\unit[408]{nm} for the BC404,
\unit[425]{nm} for the BC408) and the wavelength of the maximum absorption
light of the WLS fibre (\unit[410]{nm} for the BCF9929A fibre).
Moreover, the trapping efficiency of the double cladding fibre is a
factor $\approx$1.7~\cite{V0:bicron} larger than the one provided by
single cladding fibre. We verified these combined effects make the light
yield collected at the end of the optical fibre larger by a factor
1.3. All these results were used to select the best
system regarding the time resolution performance. The location of the
photo-multipliers allows optical fibre lengths shorter than \unit[3]{m} and
\unit[5]{m} for the \VZERO{A} and \VZERO{C} arrays respectively. A time
resolution better than \unit[1]{ns} in any conditions was
reached.

We have checked~\cite{V0:benjamin} that the light yield does not
depend significantly on the size of the scintillator blocks which
compose the \VZERO{} arrays. On the contrary, the smaller the counter,
the better the time resolution. The present tests were carried out
with scintillator blocks not identical (sector angle of 30$^\circ$ and
not 45$^\circ$) to the final ones but representative of the largest
counters of the arrays. Hence, the time resolutions which are given
here are representative of the ones expected with the final geometry.
We have measured that the adopted geometrical design
(\figref{fig:V0:V0Cdesign}) provides an homogeneous light yield
(within $\pm$\unit[5]{\%}) across the entire surface of the scintillator. We
measured that the efficiency for detecting the MIP was independent of
the WLS fibre type.  We have verified that no scintillation light is
emitted by the WLS fibres when crossed by charged particles.
Concerning the possible detection of Cerenkov light produced inside
the optical fibres, we can make the following remarks: this light is
emitted in the UV wavelength range inside plastic fibres, a very
absorbant material, and on a cone surface with a half opening angle of
about 40$^\circ$. Therefore, the light produced in the fibres close to
the counters (far from the PMT) will be very much attenuated when
reaching the PMT. The light produced in fibres installed along a
direction close to that of the particles (proximity of the PMTs)
cannot be guided owing to its emitted direction which make its
trapping difficult within the $\approx$25$^\circ$ acceptance angle of the
fibres. As a consequence, no significant light yield should be
collected due to direct impact of particles on the clear fibre beams.
Tests will be carried out to verify this assertion. In conclusion, the
choice of the counter elements seems to provide robust characteristics
which suit well the needs of the \VZERO{} detector. 

\subsubsection*{Final set-up}

Design 2 was tested with BC408 scintillator, Y11 / BCF9929A WLS fibres, and BCF98
optical fibres (see above). The results show that even though
the Y11 WLS fibres provide more light, they give a worse time
resolution (\figref{fig:V0:V0Ctimelight}~(left)). The better time resolution of
the BCF9929A(d.c.) fibre was the decisive factor in the choice of the
fibres. Later tests with design 2 using BC404 scintillator coupled to
BCF9929A(d.c.) fibres showed an increase in the produced light of about +\unit[30]{\%}
as compared to  previous measurements using BC408 coupled to
BCF9929A(s.c.) fibres. These results led, as for the \VZERO{A} array,
to the choice of components:

\begin{itemize}
\item scintillator BC404 of \unit[2]{cm} in thickness,
\item WLS fibres BCF9929A (double cladding) of \unit[1]{mm} in diameter,
\item optical fibres BCF98 (d.c.) of \unit[1.1]{mm} in diameter,
\item 16 dynodes mesh PMTs R5946 + divider from Hamamatsu. The choice
      of this photo-multiplier is justified in \secref{sec:V0:PMT}.
\end{itemize}

The length of the fibre bundles will be less than \unit[5]{m} for the
\VZERO{C} array. We will investigate
PMT locations giving the shortest possible fibre lengths to minimize
the loss of light. Integration tests will allow to precisely define
the fibre length necessary in each side.

The final results on the \VZERO{C} array were obtained at the PS in
September~2004 with the counter used for
tests and described in \figref{fig:V0:V0Cdesign}. The
\figref{fig:V0:V0Ctimelight}~(right) gives the time resolution as a
function of the high voltage applied to the 16 dynodes mesh Hamamatsu
R5946 tube. The anode signal was amplified by a factor 10. The data were obtained
with \unit[5]{m} of BCF98(d.c.) optical fibres (not the final length) and
\unit[25]{m} of Cu-cable (the distance up to the Front End Electronics
location in \ALICE{}). A threshold of \unit[20]{mV} applied to the
MIP signal amplitude was used to define the time. Time resolution with
constant fraction discriminator were very similar. The number of
photo-electrons given by a MIP is about 22, while 38 p.e. would have
been extrapolated using an \unit[1]{m} optical fibre (\figref{fig:V0:V0Ctimelight}~(left)).

The initial requirement on the time resolution for each V0 counter
element was $\sigma_{time}=\unit[1]{ns}$
(\secref{sec:V0:geometry}). The present
measurements are well within this specification. In  \ppCol{} collisions for
which the dynamics requirement on the signal is not large, the high
voltage applied to the PMT can be chosen to be high. \unit[1700]{V} would
provide a  $\sigma_{time}$ value better than \unit[0.7]{ns}. In \PbPbCol{} collisions, we
require a signal dynamics of 1000 (\secref{sec:V0:light}). In \secref{sec:V0:stage}, we
mention a dynamical range of \unit[3]{mV}-\unit[3]{V} provided by the
PMTs with a MIP signal amplitude at around \unit[6]{mV}. The use of threshold
discriminators allows to amplify the signal by a factor 10. That is proposed by
using the `shoebox' developed by the  \TZERO{} collaboration (\secref{sec:T0:shoebox}). The MIP will
thus be centred at around \unit[60]{mV} after amplification. If the threshold for
triggering is set to $\text{MIP}-2\sigma_{MIP}$ (or to
$\text{MIP}-3\sigma_{MIP}$ to 
prevent any loss in gain), namely to MIP/2 (or to
MIP/4 if possible), the efficiency for
detecting MIP's will be larger than \unit[98]{\%}. A HV of \unit[1300-1400]{V}
would give a  $\sigma_{time}$ value of about
\unit[0.9]{ns} for a MIP and still a lower value for larger
multiplicity signal. In any case, beam-gas
background is not expected to be a problem with heavy ion beams. 

The distances between the two layers of WLS fibres (design 2) of the counter
used for tests are larger than or similar to the ones of any counter
of the final \VZERO{C} array. The results reported in the figure above shows
that a time resolution of \unit[1]{ns} will be easily reached. The
final length of the optical fibres will probably be closer to
\unit[3]{m} than \unit[5]{m} for both arrays, which
will allow for a larger light yield favourable to a still better
time resolution.

\subsubsection*{Compensation of light yield loss}

The magnetic field effect will lower the PMT gain even if its
installation is realized with caution (a \unit[30]{${}^\circ$}
orientation of the tube axis relatively to the magnetic field would
minimize the attenuation effect). This loss of signal will have to be
compensated by increasing the HV value.

Effects of radiation were evaluated starting from several tests of
resistance found in the literature~\cite{V0:radeffects}. The radiation doses
through the \VZERO{C} array estimated during 10 years of operation are of
the order of 200, 50, 20, and \unit[10]{krad} for the rings 1 to
4, respectively~\cite{V0:ppr}. The light yield  reduction was estimated to be 
at most \unit[40]{\%} during the \unit[10]{years} of operation for the
innermost ring of \VZERO{C}, and less than about \unit[20]{\%} for the innermost ring of
\VZERO{A}. This does not take into account annealing effects which were measured in most
experiments. As a consequence, these numbers  must be considered as an
upper limit.

The loss in light can be adjusted via the PM gain too. The photo
statistics is still sufficient for full efficiency of MIP's detection. The time
resolution however may decrease by up to a factor of $\sqrt{2}$ for the
innermost counter, and less outside. As this is going to happen only
after a few years when we know the backgrounds, we can then decide if
the performance is still sufficient or not, in
which case the detectors (or parts of it) would have to be replaced.
Light yield and counter gain have to be continuously monitored and
adjusted, even without radiation effects. This will be done
continuously during data taking by monitoring the distribution of
charges.

\subsection{The \VZERO{A} and \VZERO{C} array designs}
\label{sec:V0:V0design}

As shown in the two sections above, both designs give about the same
integrated results (25 -- 30~p.e./MIP,
$\sigma_{time}\approx\unit[800-600]{ps}$) if ever a common scintillating
material type is adopted.

Design~1 (each face of the tiles is read with array of WLS fibres)
provides, relatively to the particle track segment within the
scintillator, a similar geometrical network of fibres, whatever the
position of the particle impact on the counter. This arrangement
minimises the time fluctuation of the light signal. It is therefore
the appropriate choice for large detector tiles. This design also is
very well adapted to the space available on the A-side to integrate
the transition needed from counter edges to clear fibres. As a
consequence, design~1 was adopted for the realization of the
\VZERO{A} array installed in the \RB{24} side of \ALICE{}. On the
C-side (\RB{26}), the tight space in radial and longitudinal
directions only allows design~2 (each tile is read by WLS fibres
at their radial edges) for the construction of the \VZERO{C} array. It
is the only one which permits the clear fibre bundles to be connected
within the short existing radial distance. Moreover, the thickness of
the overall \VZERO{C} device (\unit[47]{mm}) suits well the reduced
longitudinal space between the front absorber and the \FMD{}/\TZERO{}
detectors.

Finally, the Mexico group will build the \VZERO{A} array according to
design~1, the Lyon group the \VZERO{C} array according to design~2.

\subsection{\VZERO{A} assembly}
\label{sec:V0:V0Aassemb}  

The eight sectors of \VZERO{A} detector will be constructed following the
megatile technique described previously. The groove shape (key hole
type) is such that the WLS fibres will be kept in place without the
need of glue
(\figref{fig:V0:V0Akeyhole}). Each one of the WLS fibres will be cut at the
outer edge of the megatile slice. At that point they will be joined to
clear fibres by an optical connector. For each slice a single optical
connector will joint the WLS fibres and the clear fibres
(\figref{fig:V0:V0Aconnector}). The embedded end of the WLS fibres will be
polished and aluminized. The number of fibres will vary proportionally
to the size of the cell. The clear fibres ($\approx$\unit[3]{m}~long) will be
bunched and brought to the PMT which will be placed in the plane of
the scintillator disk.  The housing of the counter will provide
optical tightness and mechanical support for the optical connector.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.75\textwidth]{v0/figures/V0Akeyhole}
    \caption{The main elements of the \VZERO{A} design are shown. The
      upper left shows the way the WLS fibre is sunk into the
      scintillator to avoid small radii of curvature, while the lower
      left shows the keyhole design of the groove that keeps the
      fibre in place. The figure on the right shows the grooves in the
      scintillator on one face.}
\label{fig:V0:V0Akeyhole}
  \end{center}
\end{figure}

\begin{figure}[htbp]
  \begin{center}
      \includegraphics[height=0.40\textwidth]{v0/figures/V0Aconnector1}
      \includegraphics[height=0.40\textwidth]{v0/figures/V0Aconnector2}
    \caption{The optical connector
      between the WLS fibres and the clear fibres. The WLS fibres,
      protruding towards the upper left part of the figure are shown
      before being inserted into the scintillator. The clear fibres
      are running towards the lower right side of the figure (left).
      The three bundles, from the three rings of
      the prototype are connected to the photo-multipliers via the
      cylindrical connectors. The number of fibres in each
      connector depends on the size of the respective ring (right).}
    \label{fig:V0:V0Aconnector}
  \end{center}
\end{figure} 

\subsection{\VZERO{C} assembly}
\label{sec:V0:V0Cassemb}

All individual counters are assembled in the same way following
design~2. \figref{fig:V0:V0Cmontage}~(left) shows the different
elements in the elementary channel.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[height=0.35\textwidth]{v0/figures/V0Cmontage}
    \hspace{1.0cm}
    \includegraphics[height=0.3\textwidth]{v0/figures/V0Cconnector}
    \caption{Elements for the mounting of the individual \VZERO{C}
      counters~(left). Connector~(right).}
    \label{fig:V0:V0Cmontage}
  \end{center}
\end{figure}

The \unit[2]{cm} scintillator is machined following the dimensions
associated to the ring number (\figref{fig:V0:V0Csector}). A recess of
\unit[9]{mm} in width and \unit[1]{mm} in depth is made along the
radial sides of the block to be used to accommodate the nine WLS fibres.
The fibres are preliminarily cut with the help of an assembling gauge
and polished on one end. Their lengths are about 325, 300, 253 and
\unit[177]{mm} for elements of rings~1, 2, 3 and 4 respectively.
The polished end is coated with aluminium. The other end is embedded
in the connector (\figref{fig:V0:V0Cmontage}~(right)), and the side is
polished. The layers of nine fibres are fixed with BC600 optical glue
within the recess. Finally, scintillator and fibres are wrapped in a
Teflon strip. The light is thus trapped inside the scintillator and
the fibres are protected against mechanical damage.

Optical fibres used for the light transport to the PMT are cut
at the same length. They are gathered in bunches of nine to constitute a bundle.
Each bundle is protected by a black sheath of 5/\unit[6]{mm} in
internal/external diameter. The connector
(\figref{fig:V0:V0Cmontage}~(right)) is mounted on one end for the link
to the WLS fibres. The nine fibres are then assembled on the opposite
side and inserted into a plastic cylinder of \unit[5]{mm} in
external diameter. It is used to assure optical contact with the
photo-multiplier cathode. Finally both ends are polished.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[height=0.4\textwidth]{v0/figures/V0Csector}
    \caption{Sector of the \VZERO{C} array. Actual dimensions of the counters
      taking into account the constraints for their installation.}
    \label{fig:V0:V0Csector}
  \end{center}
\end{figure}

The sector of the array consists of six counters as shown in
\figref{fig:V0:V0Csector}. Each channel of rings~3 and~4 is made of
two identical counters in order to reduce their transverse dimensions
and thus to minimize the degradation of the time resolution. The
drawing also shows the Teflon strip thickness. The dead surface
represents about \unit[2]{\%} of the array area.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[height=0.45\textwidth]{v0/figures/V0Cbox1}
    \hspace{1.5cm}\includegraphics[height=0.4\textwidth]{v0/figures/V0Cbox2}
    \caption{Views of the \VZERO{C} array mounted in its box.}
    \label{fig:V0:V0Cbox}
  \end{center}
\end{figure}

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[height=0.45\textwidth]{v0/figures/V0Ccuts}
    \caption{Three cut views along the directions at 22.5$^\circ$, 11.25$^\circ$
      and 0$^\circ$ relative to an inter-sector radius of the
      \VZERO{C} array.}
    \label{fig:V0:V0Ccuts}
  \end{center}
\end{figure}

The eight sectors of counters are mounted inside a box made of two
identical parts (\figref{fig:V0:V0Cbox}). This box consists of
\unit[3]{mm} carbon fibre plates assembled with glue. The external
side is a 16 segment polygon. The part of the connectors attached to
the WLS fibres are fixed with glue across these faces.  Cut views
along directions at 22.5$^\circ$, 11.25$^\circ$ and 0$^\circ$
relatively to an inter-sector direction are shown in
\figref{fig:V0:V0Ccuts}. Rings~3 and 4 are set on the bottom of
the box. Wedges are glued between elements of ring~4 to prevent
any rotation of the pieces. Rings~1 and 2 are raised by 20 and
\unit[10]{mm} from the bottom. The recess for layers of fibres is
shifted by \unit[5]{mm} towards the top of scintillators. The dead
zones between the segments are thus minimized. Each sector is
maintained in vertical position by a foam of Rohacel glued on the bottom
below rings~1 and 2 and under the cover above rings~3 and 4.
Finally, two plastic pieces are in extension between the ring~4
segments and the internal face of the box to maintain the sector in the
horizontal direction.

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%% End:

%%
%% $Id: lightpulse.tex,v 1.3 2005-08-15 11:09:36 hehi Exp $
%%
\section{Light pulse treatment}
\label{sec:V0:light}
\CVSKeyword{$Id: lightpulse.tex,v 1.3 2005-08-15 11:09:36 hehi Exp $}
\CVSIdShow


The aim of the light pulse treatment is to supply the \VZERO{}
Front-End Electronics (and the TDR wake-up electronics) circuits with
signal so that:

\begin{itemize}
\item minimum-bias triggers from the MIP can be provided with a large
  efficiency in \ppCol{} and \PbPbCol{} collisions,
\item the large dynamics of the light signal encountered in the \PbPbCol{}
  collision can be transmitted with a minimum of distortion.
\end{itemize}

A MIP detection efficiency of about \unit[98]{\%} is obtained if the
electronics system can react starting from its most probable light
signal minus twice the rms ($\sigma_{MIP}$) of the distribution. The
low edge of the signal range will thus be the light corresponding to
the $\text{MIP}-2\sigma_{MIP}$.  Each cell of the \VZERO{} will have
to detect up to \unit[500]{MIPs} (\secref{sec:V0:PbPb-multiplicity})
in \PbPbCol{} collision. The high edge of the signal range will thus
correspond to \unit[500]{MIPs}. As
$\sigma_{MIP}\approx{MIP/4}$~\cite{V0:V0note1} in the conditions of
the tests, the full signal dynamics will have to approach 1000.

In this section, we present the work carried out to choose the
photo-multiplier. Then, we try to define what will be the first stage
of the electronics circuit feeding the Front-End Electronics system as
described in \secref{sec:V0:electronics}.

\subsection{Photo-multiplier}
\label{sec:V0:PMT}

Photo-multipliers will be used to amplify the signal. They will be
installed inside the magnetic field volume of the L3 magnet. Only mesh
tubes can thus be chosen. There are a few such tubes
proposed by two companies, Hamamatsu~\cite{V0:hamamatsu} from Japan
and Electron from Russia, company delivering PMTs for the \TZERO{}
detector (\secref{sec:T0:PMTtube}). The description and
characteristics of some of them are given in \tabref{tab:V0:PMT}.

\begin{table}[htbp]
  \begin{center}
    \caption{Characteristics of some mesh tubes candidates for
      equiping the \VZERO{} detector.}  
    \vspace{0.5cm}
    {\renewcommand{\arraystretch}{1.4}
      \begin{tabular} {|c|c|c|c|c|}
        \hline
        & 
        PMT1&
        PMT2&
        PMT3&
        PMT4\\
        \hline
        {\textbf{PMT}}&
        {\textbf{R7761-70}}&
        {\textbf{R5946-70SEL}}&
        
        {\textbf{R5505-70}}&
        {\textbf{FEU-187}}\\         
        {\textbf{HV divider}}&

        {\textbf{H8409-70}}&       
        {\textbf{E6113-03}}&
        {\textbf{H6152-70}}&
        {\textbf{Tveng(035)}}\\
        \hline
        Tube diam./length (mm)&
        39/50&
        38/50&
        25/40&
        32/65\\
        Number of stages&
        19&
        16&
        15&
        15\\ 
        HVmax&
        2300&
        2300&
        2300&
        2300\\
        Gain$^{HVmax}$&
        7. 10$^7$&
        2.5 10$^6$&
        10$^6$&
        \\
        Gain$^{2000 V}$ at 0 T/0.5 T&
        10$^7$/3. 10$^6$&
        10$^6$/4.3 10$^5$&
        5. 10$^5$/2.3 10$^5$&
        2. 10$^5$/1.6 10$^5$\\
        Rise/transit time (ns)&
        2.6/7.5&
        1.9/7.2&
        1.5/5.6&
        1.5/6\\ 
        Dark current (max) (nA)&
        100&
        30&
        30&
        20\\
        Pulse linearity (\unit[2]{\%}) (mA)&
        500&
        360&
        180&
        \\\hline
      \end{tabular}
    } 
    \label{tab:V0:PMT}
  \end{center}
\end{table}

In order to select a PMT well adapted to the \VZERO{} counter
performances (number of p.e. and time resolution from the MIP,
dynamics of the signal, etc.)  special tests were
carried out with green LED light, particles from the PS accelerator,
and cosmic MIPs. In the latter case, a counter made of
BC408/BCF9929A mounted according to design~2 and with dimensions
given in \figref{fig:V0:V0Cdesign} was used. Five metres of BCF98 clear fibres
were connected to the WLS fibres which allowed a light yield of
about \unit[17]{p.e.} and a time resolution close to \unit[800]{ps}
(\figref{fig:V0:V0Ctimelight}~(left)). In those measurements, the XP2020 with
a bialkali cathode was used.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.50\textwidth]{v0/figures/gain}
   \includegraphics[width=.40\textwidth]{v0/figures/MIP}
   \caption{Gain distributions as a function of HV applied to PMT1
     (square and star), PMT2 (triangle), PMT3 (cross), and PMT4
     (circle). Full symbols are from present measurements, stars from
     measurements by the constructor, empty symbols are from
     catalogues~(left). Cosmic MIP charge distributions provided by
     PMT1 (top), PMT2 (middle) and PMT4 (bottom) with an applied HV
     value of respectively 1250, 1350 and \unit[1750]{V}, providing a
     similar gain for each measurement~(right).}
    \label{fig:V0:gain}
  \end{center}
\end{figure} 

As shown in \tabref{tab:V0:PMT}, there are bialkali cathode mesh PMTs
with 19 (PMT1), 16 (PMT2) and 15 (PMT3 and PMT4) dynodes, the two last
ones being in fact very similar. A large spectrum of gain can thus be
expected. The gain distribution of some of them (PMT1, PMT2 and PMT4) was
measured with the cosmic MIPs as a function of the HV value. Results
are shown in \figref{fig:V0:gain}~(left) and compared to corresponding
distributions measured by the constructor for PMT1 and given by
the constructor's catalog for PMT1, PMT2 and PMT3. The results of
the measurements with PMT1 are similar, but a factor 2 larger than
the distribution given in the catalog. The gain curve of PMT2 is
three times larger than the one of the catalog. Concerning PMT4, which was
chosen for the \TZERO{} detector, the gain  is still lower by a factor
of about 10 as compared to the measured PMT2 gain curve, but similar
to the catalog data associated to PMT3. 

Distributions of charge provided by cosmic rays were measured with
PMT1, PMT2 and PMT4 photo-multipliers working with a similar gain
thanks to a supply voltage of respectively \unit[1250], \unit[1350]
and \unit[1750]{V}. An amplification factor of 10 was applied to the
anode signals. Results are shown in \figref{fig:V0:gain}~(right). The
MIP distribution centred at around $q=\unit[12]{pC}$ has a r.m.s.
value $\sigma_q=\sigma_{MIP}$ (defined by the distribution at smaller
$q$) of about $\unit[3.5]{pC}$ ($\approx\text{MIP}/4$) for PMT1 and
PMT2 and $\unit[5.4]{pC}$ ($\approx\text{MIP}/2$) for PMT4. As a
consequence, the discrimination of the MIP signal at the level of the
$\text{MIP}-2\sigma_{MIP}$ value is possible with PMT1 and PMT2, and
impossible with PMT4. The efficiency for detecting the MIP will thus
be larger in the first case as compared to the second one.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.45\textwidth]{v0/figures/time}
    \includegraphics[width=.45\textwidth]{v0/figures/efficiency}
    \caption{Time resolution (left) and efficiency (right) given by
      PMT1 (square), PMT2 (triangle), and PMT4 (circle) 
      as a function of the discriminator threshold value applied to
      the signal of a cosmic MIP providing about \unit[17]{p.e.}. The
      HV value was respectively 1250, 1350, and \unit[1750]{V} leading
      to a similar gain in each case.} 
    \label{fig:V0:time}
  \end{center}
\end{figure} 

Measurements of the time resolution and efficiency provided by the
cosmic MIPs were carried out with PMT1, PMT2, and PMT4 as a function of
the threshold applied to the MIP signal (\figref{fig:V0:time}) and in
the same conditions as above. The  resulting q~=~\unit[12]{pC} charge signal
corresponds to a \unit[60]{mV} amplitude signal. A threshold of
\unit[30]{mV} corresponds to about MIP/2. In \figref{fig:V0:time}
are shown results with still smaller thresholds. For a threshold of
\unit[10]{mV} PMT1 and PMT2 provide their optimum performance
regarding time resolution and efficiency. On the contrary,
PMT4 requires larger threshold value, showing reduced performances.   

In conclusion, the 15-dynodes PMT4 has a low gain and provides a broad
charge distribution. As a consequence, the time resolution and the
efficiency are not as good when compared with the 19/16 dynodes PMTs
from Hamamatsu in the same conditions of gain and triggering threshold. 
The 19/16-dynodes PMT1/PMT2 seem to be good options. We chosen the
16-dynodes PMT2 because the measured time resolution is a little bit
better than the one provided by the 19-dynodes PMT1
(\figref{fig:V0:gain}~(left)) at an HV value not too small when
compared to the constructor's specifications.


\subsection{First stage of the electronics circuit}
\label{sec:V0:stage} 

The charge of the MIP delivered by PMTs was \unit[1.2]{pC} (minimum charge to be
detected:~\unit[0.6]{pC}) with PMT2 at \unit[1350]{V}. Taking into
account the pulse width of \unit[10]{ns}
(FWHM), the MIP corresponds to a signal amplitude of \unit[6]{mV}
(minimum amplitude to be detected:~\unit[3]{mV}) at the output of
the PMT. The
dynamics of 1000 required in \PbPbCol{} collisions leads to a maximum
signal of \unit[600]{pC} or \unit[3]{V}. By adjusting the HV values,
we can probably foresee a dynamics up to \unit[5]{V} without degrading the
linearity of the signal. The use of threshold discriminators allows to
amplify the signal by a factor 10. That is proposed by using the `shoebox'
developed by the \TZERO{} collaboration.

The signal provided by each \VZERO{} channel PMT is sent to the
shoebox which delivers three signals. The first one is used to
generate the \TRD{} wake-up. This treatment is carried out close to
PMTs, at a distance of a few metres. The second one, is sent to a
threshold discriminator for the generation of the \VZERO{} triggers.
They are both amplified by a factor 10 and clamped at the level of
\unit[300]{mV}. The third one, not amplified, is used for the
measurement of the charge given by the counter. The two last
treatments are carried out far from the PMTs, at about \unit[25]{m}
(the signal attenuation of \unit[13]{\%} at the end of the cable can be
compensated by a slight increase of the PMT HV).

The signal (dynamics of \unit[0.6]{pC} to \unit[600]{pC} or
\unit[3]{mV} to \unit[3]{V} at the PMT output), after its
initial amplification at the PMT output, will have to be adjusted in a
second step according to the input characteristics of the
discriminator (low minimum threshold and full input
dynamics). Finally, parameters of the charge integration circuits can
then be defined.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%% End:

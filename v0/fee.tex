%%
%% $Id: fee.tex,v 1.4 2005-08-15 11:09:35 hehi Exp $
%%
\section{Front-End Electronics (FEE)}
\label{sec:V0:electronics}
\CVSKeyword{$Id: fee.tex,v 1.4 2005-08-15 11:09:35 hehi Exp $}
\CVSIdShow


\subsection{ALICE trigger and Data Acquisition Architecture}
\label{sec:V0:alicetrigdaq}

The \ALICE{} detector is designed to be sufficiently flexible to
acquire data during the different run periods and according to the
various types of physics and trigger investigated. The heavy-ion runs
(calcium, lead) will account for about \unit[10]{\%} of \LHC{} running
time. Proton-proton interactions will also be collected. 
\tabref{tab:V0:paramtrig} summarizes the luminosities and trigger
rates foreseen for the several collision periods.

\begin{table}[htbp]
  \begin{center}
    \caption{Trigger parameters at \ALICE{}.}
    \vspace{0.5cm} {\renewcommand{\arraystretch}{1.4}
      \begin{tabular} {|l|c|c|c|}\hline
        &
        \textbf{\PbPbCol{}}&
        \textbf{\CaCaCol{}}&
        \textbf{\ppCol{}}
        \\
        \hline Bunch crossing (ns)&
        125&
        125&
        25\\ 
        Luminosity (\unit{cm\textsuperscript{2}s\textsuperscript{-1}})&
        $10^{27}$&
        $2.7\times10^{27}$~-~ $10^{29}$&
        $10^{30}$\\
        Interaction rates (Hz)&
        8000&
        8000~-~$3.\times 10^5$&
        $10^5$\\
        \hline
        \LVL{0}-trigger latency
        ($\mu$s)&
        \multicolumn{3}{c|}{1.2}\\
        \cline{2-4} 
        Max \LVL{0}-trigger rate (kHz)&
        1.3&
        1.3~-~3.0&
        1.2\\
        \hline
        \LVL{1}-trigger latency
        ($\mu$s)&
        \multicolumn{3}{c|}{5.5}\\
        \cline{2-4} Max \LVL{1}-trigger rate (kHz)&
        1.1&
        0.7~-~1.1&
        1.1\\
        \hline 
        \LVL{2}-trigger latency ($\mu$s)&
        \multicolumn{3}{c|}{$\leq$100}\\
        \cline{2-4} 
        Max \LVL{2}-trigger rate (Hz)&
        40&
        40&
        $\leq$500\\
        \hline 
        Max \VZERO{} event size (Kb)&
        60&
        60&
        $\leq$750\\\hline
      \end{tabular}
     }
      \label{tab:V0:paramtrig}
  \end{center}
\end{table}

The trigger and the readout systems will therefore be designed with a
large bandwidth in order to cope with the very high particle
multiplicity produced in ion-ion interactions, as well as with the 
low-multiplicity events obtained at high rate during the \ppCol{} running mode.

The \ALICE{} trigger system is subdivided in three levels:

\begin{description} 
\item[The Level-Zero Trigger (\LVL{0})] strobes the Front-End
  Electronics of the \ALICE{} detectors. The trigger latency with
  respect to the time of the interaction is fixed at
  \unit[1.2]{$\mu$s}. The detectors respond to the \LVL{0} with `Detector
  BUSY' signals. The front-end is held on \LVL{0} and the logic waits for
  a first-level trigger \LVL{1} or for a timeout in the case of a missing
  \LVL{1}. The trigger is distributed with a fast fan-out to all the
  front-end cards.
\item[The Level-One Trigger (\LVL{1})] is issued at a fixed latency of
  around \unit[6]{$\mu$s} (still to be precisely specified) with
  respect to the interaction time. A positive \LVL{1} trigger causes the
  event number to be distributed to the detectors and starts the
  transfer of the data from the front-end event registers to the
  multi-event buffers.
\item[The Level-Two Trigger (\LVL{2})] causes, after data reduction
  and packing, the data transfer to the \ALICE{} data acquisition. The
  \LVL{2} reject signal (\LVL{2}r) can be issued at any time before the fixed
  latency corresponding to a level-2 accept (\LVL{2}a) trigger at around
  \unit[90]{$\mu$s} (that still needs to be precisely specified).
\end{description}   

The architecture of the \ALICE{} Data Acquisition (DAQ) system is as
follows: event fragments are collected from the Front-End Electronics
(FEE) by readout and Local Data Concentrator (LDC) operating in
parallel. The connection between the sub-event building system and the
Front-End Electronics is established by the \ALICE{} Detector Data
Link (DDL). Each sub-event building system is able to acquire data in
stand-alone mode.  The event building and distribution system
assembles the sub-events into full events and records them onto
Permanent Data Storage (PDS). Each Global Data Concentrator (GDC)
receives event-fragments from the LDCs via a switch, and sends full
events to the PDS through a second switch.

At level 0 the data are held on the front-end registers and, in the
case of positive \LVL{1} trigger without timeout, are transferred from the
Front-End Electronics to the local event memory waiting for the \LVL{2}
trigger decision. On a positive \LVL{2}, the data are then transferred to
the LDC via the Detector Data Link, a high speed optical fibre. The
protocol is common to all sub-detectors.

The aggregate bandwidth is up to \unit[2.5]{GByte/s} at the level of the
GDC. After processing and data compression, the maximum bandwidth is
reduced to \unit[1.25]{GByte/s} towards the PDS.

\subsection{Overview of the \VZERO{} FEE}
\label{sec:V0:overfee}

\subsubsection{Functional description of the FEE}
\label{sec:V0:funcfee}

The readout and data acquisition architecture
(\figref{fig:V0:dataacquisitionscheme}) is designed to be compatible
with the different running modes and to withstand the trigger
rates. The system can generate twelve triggers of \LVL{0}, six of
class AND (and six of class OR)
resulting from conditions (not necessarily) simultaneously fulfilled by
\VZERO{A} and \VZERO{C} arrays. The OR configuration can be
considered in spite of the intrinsic noise of each detection
channel. A multiplicity of at least 2 can be required in that case for
both \VZERO{A} and \VZERO{C} arrays when used as triggers for central
detectors. On the contrary, no multiplicity threshold is necessary
for \VZERO{C} array when used as validation detector for the dimuon
trigger system. Among the
six triggers of each class there will be one minimum bias trigger (MB),
two beam-gas triggers (BG) and three centrality triggers (CT). Two
triggers of the last type will be selected to be sent to the CTP which
provides only a maximum of five inputs from the \VZERO{} detector. 

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.95\textwidth]{v0/figures/dataacquisitionscheme}
    \caption{V0 Front-End Electronics scheme.}                      
    \label{fig:V0:dataacquisitionscheme}
  \end{center}
\end{figure} 

Hereafter are
listed the triggers of class AND (those of class OR are
mentioned between []) and several sets of recorded information:  

\begin{itemize}
\item One [one] minimum-bias (MB) trigger: this trigger is generated if the
  number of channels fired during a collision is at least one on \VZERO{A}
  and [or] one on \VZERO{C}. The detection of the fired channels is made with
  the help of two observation windows, one for \VZERO{A} (named BBA) and the
  other one for \VZERO{C} (named BBC). This trigger will be sent to CTP.
\item Two [two] beam-gas (BG) triggers: one [one] for the beam-gas interactions
  which occur on the \RB{24} side of the \ALICE{} detector, one [one] for the
  beam-gas interactions which occur on the \RB{26} side. The detection of these
  beam-gas events is done by the use of two specific observation windows,
  BGA and BGC, in partnership with BBC and BBA
  respectively [alone]. These two [two] triggers will be sent to CTP. 
\item Three [three] centrality (CT) triggers of the collision: these triggers are
  generated if one or the other, or both of the following conditions are
  respected:
  \begin{enumerate}
  \item the integration charge seen by \VZERO{A} and [or] \VZERO{C} during a
    collision is larger than a first or a second programmed trigger
    generation threshold. Two [two] such triggers are generated.
  \item the number of channels fired during a collision is between two
    programmed trigger generation thresholds (for \VZERO{A} and [or] for \VZERO{C}).
    One [one]  such trigger is generated.
  \end{enumerate}
\item A measure of the multiplicity of MIPs: this measurement is
  obtained in two different ways:
  \begin{enumerate} 
  \item after an anode charge digitization,
  \item after a pulse length measurement (proportional to the charge
    of the pulse) provided by the discriminator.
  \end{enumerate}
\item A measure of the time difference between the detected particles
  and the beam crossing signal or beam clock (BC).
\item A wake-up signal for the TRD: the assigned time to provide this
  wake-up signal is extremely short. Therefore, copies of all PMT anode
  outputs are sent to the TRD through the `shoebox'.
\item A set of information dedicated to the EMCal detector. In order
      to perform an optimal threshold programmation, the EMCal
      detector needs a centrality information at the reception of the
      \LVL{1} trigger signal. The \VZERO{} FEE will send an 8~bit data
      representing the charge seen by both arrays (\VZERO{A} and \VZERO{C}) on the
      reception of the \LVL{0} trigger. There will be also two triggers
      amongst the twelve generated ones (the AND and OR triggers as
      defined previously) and a parity bit in the serial
      data sent to the EMCal detector. That is a recent request which has
      to be finalized.
\end{itemize}

\subsubsection{Physical description of the FEE}
\label{sec:V0:physfee}

The readout scheme follows the modularity of the detector and the
\ALICE{} trigger requirements. The segmentation
of the \VZERO{A}/\VZERO{C} arrays is shown in
\figref{fig:V0:V0segmentation}. The trigger generation, readout, and
data acquisition chain of the
\VZERO{} consists (\figref{fig:V0:rackVME}) of the following elements:

\begin{description}
\item[CIU,] Channel Interface Unit: it performs the following
  tasks:
  \begin{enumerate}
  \item measurement of the anode signal surface (charge),
  \item digitization of the time for all the hits coming from the ring,
  \item pre-processing for the generation of the various triggers,
  \item data storage during a \LVL{0} and a \LVL{1} trigger.
  \end{enumerate}
  The two main components of this board are the discriminator and the
  HPTDC~\cite{V0:hptdc}, a Time-to-Digital Converter.
  
  The CIU board is a 9U format board connected to the rest of the
  electronics via a specific back plane. Each CIU board processes
  anode signals from the eight photo-multipliers of a \VZERO{} ring.  There
  are four CIU boards per array (four for \VZERO{A} and four for \VZERO{C}).
\item[CCIU,] Channel Concentrator Interface Unit: it performs the
  following tasks:
  \begin{enumerate}
\item clock distribution to FEE,
\item generation of calibration triggers,
  \item processing of the final trigger signal,
  \item collection and organization of the data from CIU boards,
  \item provision of an interface to the DAQ,
\item interface between TTC system and FEE through the DCS mezzanine.
  \end{enumerate}

The three main `blocks' of the CCIU board are:
  \begin{enumerate}
\item the FPGA for processing the final trigger generation (the twelve
      triggers) and the trigger configuration sent to CTP and EMCal, and the data
      handling (data collection on the back plane, data preparation
      for the SIU mezzanine),
\item the SIU mezzanine, interface between DDL and FEE,
\item the DCS mezzanine, interface between CTP system and FEE,
      ethernet interface for the \VZERO{} slow control.  
  \end{enumerate}
 
  The CCIU board is a 9U format board connected to the rest of its
  detector electronics via a VME back plane. The CCIU board broadcasts
  the collected data to the DAQ (Data Acquisition system) via one DDL
  link.  It is connected to the TTC for the five trigger signals and to the
  LTU for the generation of the `busy' signal.
\end{description}


\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/rackVME}
    \caption{V0 Data AcQuisition scheme.}                      
    \label{fig:V0:rackVME}
  \end{center}
\end{figure} 

The \VZERO{} electronics of both array systems (\VZERO{A} and
\VZERO{C}) is located in one VME crate. This 11U VME crate is used
only for its mechanical structure which can accept 9U modules, and for
its supply voltage. There is no VME controller in the rack. All boards
plugged on the VME back plane are full customs and do not use the VME
bus but a specific back plane.

\subsection{Minimum-bias trigger}
\label{sec:V0:mbtrigger}

The minimum-bias trigger of class AND provides the  identification of
beam-beam (BB) collisions. For that, it is necessary to
verify on each disk (\VZERO{A} and \VZERO{C}) the event occurence at
the expected time, namely $\approx\unit[11]{ns}$ after the collision on
\VZERO{A} and $\approx\unit[3]{ns}$ after the collision on \VZERO{C} (see
\figref{fig:V0:timealignmentcondition}).

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/timealignmentcondition}
    \caption{Time alignment condition on \VZERO{A} and \VZERO{C}.}
    \label{fig:V0:timealignmentcondition}
  \end{center}
\end{figure}

The detection of hits is done through two programmable observation windows
(adjustment of start and stop times in steps of \unit[10]{ps}). These
observation windows are named BBA for the \VZERO{A} and BBC for the
\VZERO{C}. Each hit is then precisely located inside these two windows
through delay lines programmable in steps of \unit[10]{ps}. There is
one programmable delay per channel. This MB trigger is produced 
when there is one hit at least
simultaneously detected by \VZERO{A} (BBA~=~1) and by \VZERO{C}
(BBC~=~1). The MB trigger of class OR is produced when there is one
hit at least detected by
\VZERO{A} or \VZERO{C} ($\text{BBA}=1$ or $\text{BBC}=1$) (\figref{fig:V0:MBBGtriggers}). A
threshold on the minimum number of hits required in each array (THA
and THC) can be applied if necessary.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/MBBGtriggers}
    \caption{Minimum-bias and beam-gas triggers.}                      
    \label{fig:V0:MBBGtriggers}
  \end{center}
\end{figure}


Remarks concerning minimum-bias and beam-gas
(\secref{sec:V0:bgtrigger}) trigger circuits:

\begin{itemize}
\item The part of the circuit included inside the circle in
  \figref{fig:V0:MBBGtriggers} is entirely integrated in FPGA. It is thus
  possible to modify the minimum-bias trigger generation logic until the
  end of the design.
\item The control of the selection or the inhibition of channels is
  possible through an individual ON/OFF command (common command with the
  other triggers).
\item The state of each segment (symbolized by a flag), fired (1) or
  not (0) by at least one hit in
  the corresponding observation window, will be sent to the DAQ on the
  reception of a \LVL{2} trigger for an off-line analysis.
\item The detection of hits within an observation window is performed
  in an ultra fast technology (ECLinPS) with a double synchronization to
  avoid metastability problems.
\item The generated trigger is synchronized with the \LHC{} clock.
\end{itemize}

\subsection{Beam-gas triggers}
\label{sec:V0:bgtrigger}

The beam-gas (BG) triggers allow one to ensure that a beam-gas collision
took place on the \RB{24} side or on the \RB{26} side of the detector.
The beam-gas detection is done through two observation windows,
one applied to \VZERO{A} counters (called BGA) and the other one
applied to \VZERO{C} counters (called BGC). They are also
programmable in steps of \unit[10]{ps} for the start time and for the stop
time independently (see \figref{fig:V0:MBBGtriggers}). 

To generate these triggers, it is
necessary to detect on each disk (\VZERO{A} and/or \VZERO{C}) an event
occurrence at the expected time
(\figref{fig:V0:timealignmentforBGetBB}). The generation of the BG
triggers is effective if the following
conditions are respected:

\begin{itemize}
\item For the BG from \RB{24} of class AND, if the two following requirements are
  fulfilled:
  \begin{enumerate}
  \item for the considered clock period and during the BBC 
    window ($\text{BBC}=1$), the number of hits seen by \VZERO{C} is
    higher than a   programmed threshold TH3,
  \item for the previous clock period and during the BGA
    window ($\text{BGA}=1$), namely $\approx\unit[22]{ns}$ before
  the expected hit due to a BB collision on \VZERO{A}, the number of hits seen by \VZERO{A} is higher than a
    programmed threshold TH4.
  \end{enumerate}
\item For the BG from \RB{24} of class OR, if, for the previous clock period
      and during the BGA observation window ($\text{BGA}=1$), namely $\approx\unit[22]{ns}$ before
  the expected hit due to a BB collision on \VZERO{A}, the number of hits seen
      by \VZERO{A} is larger than a programmed threshold TH4. 
\item For the BG from \RB{26} of class AND, if the two following requirements are
  fulfilled: 
  \begin{enumerate}
  \item for the considered clock period and during the BBA 
    window ($\text{BBA}=1$), the number of hits seen by \VZERO{A} is higher than a
    programmed threshold TH1,
  \item for the considered clock period and during the BGC 
    window ($\text{BGC}=1$), namely $\approx\unit[6]{ns}$ before the
  expected hit due to a BB collision on \VZERO{C}, the number of hits
  seen by \VZERO{C} is higher than a 
    programmed threshold TH2.
  \end{enumerate}
\item For the BG from \RB{26} of class OR, if, for the current clock period
      and during the BGC observation window ($\text{BGC}=1$), namely $\approx\unit[6]{ns}$ before the
  expected hit due to a BB collision on \VZERO{C}, the number of hits seen
      by \VZERO{C} is larger than a programmed threshold TH2. 

\end{itemize}

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/timealignmentforBGetBB}
    \caption{Time alignment for BB, BGA and BGC events.}                      
    \label{fig:V0:timealignmentforBGetBB}
  \end{center}
\end{figure}

\subsection{Centrality triggers and multiplicity measurement}
\label{sec:V0:centtrigger}

The purpose of these centrality (multiplicity) triggers is to give information on the
impact parameter of the collision for each period of the \LHC{} clock. Electronics must be
able to dissociate two consecutive events (e.g. BG after BB) even if
the occurrence of such a sequence remains very rare.

Two types of centrality triggers are implemented on the \VZERO{} FEE.
Here are described the centrality triggers belonging  to the class AND of
triggers as defined in \secref{sec:V0:funcfee}.
The first one is able to work for all interaction types
(\PbPbCol{}, \CaCaCol{}, \ppCol{}).  It is based on a dual high-speed
integrator. \VZERO{A} and \VZERO{C} generate a central trigger if their signals
exceed a high 
threshold and a semi-central trigger if their signals exceed a moderatly high
threshold (\secref{sec:V0:dualintegrator}). These
thresholds will be independently programmable. The second type of centrality
trigger, using the observation windows, counts the fired segments on each
disk (\VZERO{A} and \VZERO{C}) and, generates one
trigger if this number is included between two programmable thresholds
(\secref{sec:V0:multiplicity}). The latter will be useful only for
events of small multiplicity (like \ppCol{}) where the probability
of having several hits on the same segment is rare.

The CTP has only two inputs dedicated to centrality triggers.
The choice of these two ones amongst the 3 possibilities is 
selectable by programmable command. The class OR of centrality
triggers is similar to the previous one except that an OR condition
between \VZERO{A} and \VZERO{C} should replace the AND condition.


\subsubsection{Dual high-speed integrator}
\label{sec:V0:dualintegrator}

The multiplicity triggers are in this case based on a charge
integration. As mentioned above, for each \LHC{} clock, the data processing consists in a
comparison between the charge seen by the detectors and two
programmable thresholds. Two different triggers are thus generated
(\figref{fig:V0:trigmult}).  Central and semi-central collisions in
the case of ion-ion physics can be especially selected before the data
acquisition.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/centralitytrigger}
    \caption{Centrality triggers.}                      
    \label{fig:V0:trigmult}
  \end{center}
\end{figure}

Remarks concerning the centrality trigger circuit:

\begin{itemize}
\item The part of the circuit included inside the circle shown in
  \figref{fig:V0:trigmult} is entirely integrated in FPGA. It is thus
  possible to modify the centrality of the trigger generation logic
  until the end of the design.
\item The control of the selection or the inhibition of channels is
  possible through an individual ON/OFF command (common command with the
  other triggers).
\item The charge seen by each channel will be sent to the DAQ on
  reception of a \LVL{2} trigger for off-line analysis.
\item The generated triggers are synchronized with the \LHC{} clock.
\end{itemize}

In order to perform a charge integration at each clock period, the
integrator is made of two integrators working alternately with a
programmable gate width and a programmable recovery time. An example
is shown in \figref{fig:V0:chainedintegration}.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/chainedintegration}
    \caption{Dual high-speed integrator block diagram.}                      
    \label{fig:V0:chainedintegration}
  \end{center}
\end{figure}

This dual high-speed integrator allows the performance of a \unit[40]{MHz}
charge digitization on a programmable gate width from \unit[5]{ns} to
\unit[30]{ns} in steps of \unit[5]{ns}. The remaining time period
is used for the reset of the integrator. An integrator works on even
BC periods while the other one works on odd BC periods. The dead time
is used for the capacitor discharge through the switch
(\figref{fig:V0:systemtiming}).

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/systemtiming}
    \caption{Dual high-speed integrator system timing.}                      
    \label{fig:V0:systemtiming}
  \end{center}
\end{figure}

For a charge digitization independent (the least dependent
possible) of the relative position between the analog signal and the
charge digitization command (clock of the ADC), the 64 analog chains
of both disks (\VZERO{A} and \VZERO{C}) will be individually adjusted through
cable length with an accuracy of \unit[500]{ps} maximum. After this time
alignment, the timing fluctuation of the analog signal is mainly due
to the location of the hit on the scintillator and the jitter of the
used components ($\approx$\unit[3]{ns} to a first estimate). The 
clock phase will be adjusted so that \unit[3]{ns} will separate the typical
analog signal and the integration gate
(\figref{fig:V0:timingbetweenanalogandclk}).

\begin{figure}[htbp]
  \begin{center}                                    
    \includegraphics[width=.9\textwidth]{v0/figures/timebetweenanalogandclk}
    \caption{Timing between analog input signal and command clock.}
    \label{fig:V0:timingbetweenanalogandclk}
  \end{center}
\end{figure} 

The input buffer allows the minimization of  the charge fluctuations seen by
the adaptation resistor (\unit[50]{$\Omega$}), whereas the output buffer is
used to adapt the voltage level and characteristics (differential) to
the input stage of the ADC.

For a given `n' BC period, the required data of an eventual trigger
are:
\begin{itemize}
\item charge digitization corresponding to the `n' BC period,
\item charge digitization corresponding to the `n+1' BC period.
\end{itemize}

In the case of BB interactions, the system will give the charge
collected by each \VZERO{A} and \VZERO{C} segment in correlation with
$\text{BBA}=\text{BBC}=1$. In the case of BG interactions, the system
will give the accurate charge collected by the \VZERO{C} and \VZERO{A}
segments 
fired from \RB{24} and \RB{26} sides respectively. In contrast, it
will give erroneous charge values for the \VZERO{A} and \VZERO{C}
counters due to the slight shift of the hits, $+\unit[3]{ns}$ for
\VZERO{A} and $-\unit[6]{ns}$ for \VZERO{C} counters relative to the
integration gate (\figref{fig:V0:timingbetweenanalogandclk}). In this
case, a correction factor can be applied to the measured charge
values. In summary:

\begin{itemize}
\item When BBA~=~BBC~=~1 (BB), the charge of \VZERO{A} and \VZERO{C}
  segments will be obtained directly.
\item When BBC~=~BGA~=~1 (BG from \RB{24}), the charge of \VZERO{C}
  elements will be obtained directly, the charge of \VZERO{A} elements
  will be obtained after correction.
\item When BBA~=~BGC~=~1 (BG from \RB{26}), the charge of \VZERO{A}
  elements will be obtained directly, the charge of \VZERO{C} elements
  will be obtained after correction.
\end{itemize}

Before any registration in FIFO, a pedestal value (determined in the
calibration phase) is subtracted from the two data corresponding to the
output of the ADC. The rate of the registration in FIFO is
\unit[40]{MHz}. These data are collected by the CCIU board on a \LVL{1}
trigger request. A 10-bit ADC will be used to digitize the integrated
charge.

In addition to the charge information provided by physical events, the
CCIU board generates some internal triggers in order to
collect data corresponding to a pedestal value of the charge
integrators as well as the charge corresponding to a minimum bias
trigger. The CIU board stores these data in FIFO and send them to the
DAQ with the data of interest on a \LVL{2} trigger. The size of both
FIFO (one for pedestal, the other for MB charge) is progammable and
initially fixed to 10. A continuous measurement of the charge
provided by the counters will be thus possible in the (almost) same trigger
condition, whatever the CTP trigger type can be. These recorded data
will be used to have a good knowledge of the \VZERO{} counter gain evolution
during the time. In particular, radiation effects along the time can
be quantified.

\subsubsection{Multiplicity trigger based on observation windows}
\label{sec:V0:multiplicity}

This multiplicity (centrality) trigger of each class (AND and OR) is
not based on the charge seen by both disks,
but only on the number of fired segments. It will thus be useful only
for interactions with low-multiplicity events such as \ppCol{}
interactions where the probability of having several hits in the same
segment is small.

The detection of the number of fired segments is done by the same
observation window system as the one used for the minimum-bias trigger
for the hit detection on \VZERO{A} (BBA) and for the hit detection on
the \VZERO{C} (BBC). The generation of the centrality triggers
is effective as soon as the number of events seen by each disk is
included between two programmable thresholds. The use of a
programmable interval rather than a simple threshold allows 
events with small and high multiplicity due to BG
interactions (\figref{fig:V0:ppmultiplicity}) to be eventually rejected.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/ppmultiplicity}
    \caption{ Multiplicity based on observation window.}                      
    \label{fig:V0:ppmultiplicity}
  \end{center}
\end{figure} 

Remarks concerning the multiplicity trigger circuit:

\begin{itemize}
\item The part of the circuit included inside the circle shown in
  \figref{fig:V0:ppmultiplicity} is entirely integrated in FPGA. It is
  thus possible to modify the multiplicity trigger generation logic
  until the end of the design. The rest of the function is common with
  the minimum-bias function.
\item The control of the selection or the inhibition of channels is
  possible through an individual ON/OFF command (common command with
  the other triggers).
\item The generated triggers are synchronized with the \LHC{} clock.
\end{itemize}

\subsection{Time measurement}
\label{sec:V0:timemeas}

The main component of this function is the HPTDC~\cite{V0:hptdc}, a
High Performance Time to Digital Converter developed in the
microelectronics group at CERN. It allows the measurement of the time
intervals between the raising/trailing edges of a PMT signal and the
raising edge of the reference clock. Assuming that the pulse width is
proportional to the charge of the pulse, the trailing edge is used
off-line to estimate the charge of each hit. The hit signal comes from
the shoebox and then from the low threshold discriminator output.  The
data delivered by the HPTDC are registered in FIFO until the arrival
of a \LVL{2} accept trigger.  The possible offset in time of each
\VZERO{A} and \VZERO{C} channel will be compensated through the FPGA
which has a register making possible this alignment. A
calibration procedure will allow these values to be measured in a simple
way (\secref{sec:V0:commissioning}).

There is one HPTDC per CIU board. It performs the time digitization of
the eight channels of a \VZERO{} ring. The complete scheme of the time
digitization is shown in \figref{fig:V0:timemeasurement}.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/timemeasurement}
    \caption{Time digitization.}                      
    \label{fig:V0:timemeasurement}
  \end{center}
\end{figure} 

In order to have an access to data which, for some reason, do not
generate a \LVL{2} trigger, an histogram of 10 measurements of each
channel (corresponding to -5 to +5 bunch crossings) will be
sent to the DAQ at each \LVL{2} trigger instead of the simple `useful
data'. These data will make it possible to determine off-line the BG
event rate versus the BB event rate.

The data-driven architecture of the HPTDC imposes limitations which must
be taken into account for our application. Several levels of data
merging are performed before data are finally read out. Each level of
data merging has a related set of de-randomize buffers to minimize the
effect of such potential bottlenecks. In addition, sufficient buffering
must be available in the `HPTDC \LVL{1} buffer' to store data during the
trigger latency:

\begin{description}
\item[Channel merging] The absolute maximum bandwidth which can be
  used by one channel of the HPTDC is \unit[10]{MHz} (@\unit[40]{MHz}
  clocking). When several channels are used the total available
  bandwidth (\unit[40]{MHz}) is distributed such that each channel
  gets its fair share (max. \unit[10]{MHz} or 1/8 of total bandwidth),
  so \unit[5]{MHz} min. For the \VZERO{}, the maximum interaction rate
  expected in \ppCol{} is \unit[100]{kHz}, allowing some margin for the HPTDC
  one-channel bandwidth.
\item[`HPTDC \LVL{1} buffer'] The `HPTDC \LVL{1} buffer' must store hit
  measurements during the latency of the trigger (\unit[1.2]{$\mu$s}
  for the \LVL{0} trigger), when the trigger matching function is used. The
  required buffering is proportional to the product of the average hit
  rate on eight channels, feeding a \LVL{1} buffer, and the trigger latency
  ($\unit[1.2]{\mbox{$\mu$s}} \times 8 \times
  \unit[100]{kHz}~\approx1$). Knowing that it can in general be
  considered safe to work under conditions where the average buffer
  occupancy is less than half the available buffering capability
  (256/2~=~128), we considered the required buffering as
  sufficient.
\end{description}

The HPTDC has a large number of programmable features. They allow a
very flexible operation under different conditions. A listing of the
main programming data is presented in the \tabref{tab:v0:HPTDCprog}.


\begin{table}[htbp]
  \begin{center}
    \caption{Main programming HPTDC data.}
    \vspace{0.5cm} {\renewcommand{\arraystretch}{1.4}
      \begin{tabular}{|l|l|l|}\hline
        Enable leading&
        0&
        \\\hline Enable trailing&
        0&
        \\\hline Enable
        pair&
        1&
        Pairing of leading and trailing edges\\\hline Dead
        time&
        00&
        \unit[5]{ns} dead time to remove potential ringing
        from analog front-end\\\hline Dll clock
        source&
        001&
        \unit[40]{MHz} from PLL\\\hline Dll
        mode&
        00&
        \unit[40]{MHz} DLL mode\\\hline Leading
        resolution&
        001&
        \unit[200]{ps}\\\hline Enable
        matching&
        1&
        Trigger matching enabled\\\hline Enable
        serial&
        0&
        Parallel readout\\\hline Enable bytewise&
        0&
        32 bits
        readout\\\hline Keep token&
        1&
        TDC allowed to keep token until
        no more data\\\hline Master &
        0&
        All TDC configured as
        slaves\\\hline
      \end{tabular}
    } \label{tab:v0:HPTDCprog}
  \end{center}
\end{table}

\subsection{Readout and Data Acquisition System (DAQ)}
\label{sec:V0:daq}

The interface between the DAQ system and the \VZERO{} FEE is performed
through the SIU card. All the data collected by the FEE are sent to
the DAQ (through one DDL which is a standard \ALICE{} component) on a \LVL{2}
trigger. The DDL will be handled by the master processor through a SIU
daughter board which will form a part of the CCIU board.

The readout is of course used to collect the physics data. It is also
necessary to monitor several other data used for
the monitoring. The list of the data transfered through the DDL is
given below:
\begin{itemize}
\item for the events of interest and the (+5, -5) events around:
\begin{enumerate}
\item charge seen by all segments (integrated charge),
\item time of all segments (leading and tailing edges),
\item statement of all flags for all segments.
\end{enumerate}
\item an histogram of the pedestals (10 data per segment),
\item an histogram of the charge corresponding to the MB events (10
      data per segment).
\end{itemize}

\subsection{Calibration and monitoring}
\label{sec:V0:calibbeam}

The knowledge of the \VZERO{} timing and gain response is very
important for the generation of different triggers. An initial calibration
of the \VZERO{} will be done with cosmic rays in laboratories and/or
with beam particles (\secref{sec:V0:commissioning}).  The response of
the detector to the outgoing particles from \ppCol{} collisions is a
good tool for the complete channel gain characterization and for a
correct differential timing adjustment between channels.

These operations include:

\begin{itemize}
\item alignment of the \LHC{} clock on the hits,
\item setting of the observation windows, start and stop times, and
  the various programmable thresholds,
\item gain and pedestal calibration for each channel (also both
  branches of the dual high-speed integrator) in correlation with the
  high voltage (PMT supply),
\item time alignment of each channel (each one compared to the others):
  \begin{enumerate}
  \item initially with the lengths of the cables in order to have a time
    fluctuation of about $\approx$\unit[500]{ps},
  \item finally using the programmable delays in order to have a time
    fluctuation of about $\approx$\unit[20]{ps}.
  \end{enumerate}
\item pulse injection in the FEE for monitoring the proper working of
      each channel.
\end{itemize}

\subsection{Slow Control}
\label{sec:V0:slowcont}

The \ALICE{} Detector Control System (DCS) is characterized
in Ref.~\cite{V0:dcs}.  Two major modes of operation can be distinguished for
the DCS:

\begin{itemize}
\item In normal operation during physics data taking, a controlled
  start, operation and shutdown of the different sub-detectors is
  measured. For this purpose, standard operator commands will be
  available. Malfunctioning will be signalized through centralized
  alarms. The DCS will be accessed through the global experiment
  control system and data exchange with the DAQ will be provided.
\item During all other periods, the detectors will be operated in a
  less coherent manner. It will be necessary to run a detector or
  parts of it separately. Access to the equipment must nevertheless be
  guaranteed from remote locations. However, interference between
  detectors or, between detectors and external services must be
  screened.
\end{itemize}

The DCS board is used by the \VZERO{} Slow Control  System to communicate
with the detector electronic chain through an ethernet link.

To satisfy the preceding requirements, the architecture will be based
on distributed intelligence. The \VZERO{} Slow Control System will
integrate most of the functions required to monitor and control the
sub-system components such as high voltage and readout electronics.
Each of these elements is described in \figref{fig:V0:DCS}.

The VME crate used for the readout electronics allows
several parameters (voltage, current, cooling) to be monitored by
means of a Field Bus standard (i.e. CAN).

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{v0/figures/DCS}
    \caption{\VZERO{} Detector Control System.}                      
    \label{fig:V0:DCS}
  \end{center}
\end{figure}  

\subsection{High- and low-voltage power supplies, cabling and racks}
\label{sec:V0:powersupply}

The HV and LV power supplies are systems common to \VZERO{} and
\TZERO{} detectors which are made of 64~+~24 detection channels. The
power supplies will be commercial units located in a rack (Z04) far
from the detector (CR4). The setting of the HV and LV voltages and the
monitoring of the voltage and the current will be performed remotely
by means of a Field Bus standard (i.e. CAN). The chosen HV power
supply system is the CAEN SY1527 \cite{V0:caen}, the chosen power
supply boards are A1733P modules (12 channels per module) for V0
(A1733N modules for T0). Three cables with 37 conductors will be used
to deliver the high voltage to both \VZERO{} and \TZERO{} arrays. The
chosen LV power supply system is the A3009 unit able to furnish up to
\unit[12]{A} under $\pm\unit[6]{V}$. Ten cables with 36 conductors
will be used to deliver the low voltage to both \VZERO{} and \TZERO{}
arrays.

The Front-End Electronics will be housed in a rack (B18) of the B
group located on the top of the \LTHREE{} magnet. Less than
\unit[25]{m} of cable are needed to connect the PMTs to the
electronics. The chosen \unit[50]{$\Omega$} cable has an attenuation
of \unit[1.7]{dB} at \unit[100]{MHz}, namely a power attenuation of
about \unit[32]{\%} and a voltage attenuation of about \unit[18]{\%}.
Measurement of the latter quantity gave a value of
$\approx\unit[13]{\%}$. The transit delay of this cable is about
\unit[4]{ns} per metre. BNC is the standard connector.

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%% End:

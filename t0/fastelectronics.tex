%%
%% $Id: fastelectronics.tex,v 1.13 2005-06-14 15:43:42 hehi Exp $
%%
%% Tomasz Malkiewicz 10.08.2004
%%

\section{Fast Electronics}
\label{cha:t0:fastel}

The overall diagram of \TZERO{} electronics is shown in
\figref{fig:t0:fastelectronics:diagram}. Signals from each PMT are
first sent to the so-called shoeboxes, located about \unit[6]{m}
from the detectors. The main role of the shoeboxes is to split and
amplify the signals to generate a wake--up call for the \TRD{}
detector. Otherwise no electronics would be required between the
PMTs and the main electronics racks of \TZERO{} outside the
\LTHREE{} magnet. There the \TZERO{} pulses are processed and used
to produce the required trigger signals. The time and amplitude
information from each PMT will be read out and stored by \ALICE{}
DAQ. The \TZERO{} readout will be nearly identical to that of the
\TOF{} detector. This solution was adopted to cut costs and to
guarantee the performance of the \TZERO{}. Currently \TOF{} is the
only \ALICE{} sub-detector that needs non-trigger information from
\TZERO{}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.9\textwidth]{t0/figures/diagram}
  \caption{\TZERO{} fast electronics.}
  \label{fig:t0:fastelectronics:diagram}
\end{figure}

\subsection{Shoebox with front--end electronics}
\label{sec:T0:shoebox}

There will be two separate shoeboxes, one for each arm of the
\TZERO{} detector. The \TZERO{A} shoebox will be a stand-alone unit
containing only the electronics for splitting and amplifying the
signals. The \TZERO{C} shoebox will be in direct proximity to the
\TRD{} wake--up shoebox, where the pulses from all \TZERO{} and
\VZERO{} detector will merge and be processed in a similar fashion.

As discussed in \secref{sec:t0:detector:voltages} the expected
signal amplitude of the input to the shoebox will be from
\unit[15]{mV} up to \unit[5]{V}. To secure \unit[50]{ps} time
resolution throughout the entire dynamic range it is necessary to
preserve as much of the signal's original shape as possible. To
prevent the zero level from floating with changing count rate, the
ultra--wideband amplifier in the shoebox should be of the direct
current type.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.95\textwidth]{t0/figures/shoeboxes}
  \caption{Block diagrams of the amplification stage of the shoebox as
    tested at \CERN{}, 2004 (left), and the improved version
    (right).}
  \label{fig:t0:fastelectronics:shoeboxes}
\end{figure}

\Figref{fig:t0:fastelectronics:shoeboxes} shows the ``shoebox'' used
during the June 2004 run and the modified design that will be tried
out next. Each channel of the shoebox has one input for PMT pulses
and 3 outputs: 1 direct and 2 with gain of about 25. The direct
output will go to the wide range CFD (see the next section). In
principle, if the CFD works as expected in the full dynamic range,
the amplified signal will only be used to improve the accuracy of
amplitude digitization. In case of unforeseen problems, the
amplified output could also feed to the second CFD working in
parallel with the one with the direct signal, allowing for precise
off-line corrections.

The \TRD{} wake--up electronics does not need the same time
resolution as \TOF{}. Since small time shifts such as those due to
saturated pulses are not a problem for the wake--up, and the absence
of low amplitude (not amplified) pulses makes the design easier and
more tolerant of electronic noise and interference, only the
amplified signals will be delivered to the \TRD{} wake--up
electronics.


The main construction elements of the shoebox are the OPA695
current-feedback operational amplifier and THS4503 - a wideband,
low-distortion fully differential amplifier. Since even these modern
operational amplifiers provide the proper bandwidth only for gain
below 8, we have had to use a two-stage system, each with gain of
about 5. The Printed Circuit Board (PCB) of the prototype was
$\unit[80]{mm} \times \unit[75]{mm}$ and included two low-dropout
voltage regulators providing the amplifier with a clean and
stabilised power supply. In the quiescent mode the unit consumes
\unit[75]{mA} from \unit[+6]{V} and from \unit[-6]{V}.  At high
counting rate the current will increase to \unit[100]{mA}.

For the next version of the prototype the size of the PCB will be
reduced to \unit[$50\times60$]{mm\textsuperscript{2}} and a
non-inverting stage will be used to eliminate the passive fan out at
the input, which currently gives a slight (50/70) attenuation.

\subsection{Constant Fraction Discriminator with Wide Dynamic Range}

Constant Fraction Discriminators (CFD) are used to determine the
arrival time of analogue pulses from fast detectors. As long as the
amplitude of the pulse stays within the dynamic range of the CFD, no
slewing corrections are needed. The time does not depend on the
amplitude of the pulse. A very good CFD, for instance the Phillips
Scientific 715 \cite{t0:phillips} exhibits typical time walk plus
slewing of $ \pm \unit[75]{ps} $ for amplitudes between threshold
and 100 times threshold. As discussed in
\secref{sec:t0:detector:voltages}, the dynamic range required in
\ALICE{} experiments will be 5 times larger. In principle, off-line
slewing corrections are a standard procedure that enables good time
resolution even with simpler Leading Edge discriminators (LED).
However, considering the need for good on-line performance (trigger)
and to stay below the \unit[50]{ps} range in time resolution, we
opted to develop a CFD that will work in a dynamic range of 1:500
(see \figref{fig:t0:fastelectronics:cfd}).

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/cfd}
  \caption{Schematic diagram of the prototype of the wide range CFD.}
  \label{fig:t0:fastelectronics:cfd}
\end{figure}

The timing channel of the CFD is implemented according to the
traditional scheme, with the detection of zero crossing using an
inverted, delayed and attenuated signal. The main difference is in
the use of the two signals (amplified and without amplification)
instead of just one. In this way our CFD is in fact two CFDs working
in parallel and governed by a comparator of output signals. The
delay and attenuation values are set by input signal parameters. A
lot of attention was paid to the design of the wide-range amplifier.
It provides relatively high amplification ($K_{amp} = 22$) while
retaining the characteristics of the original input signal. The use
of the amplifier is crucial for extracting time signals with low
amplitude. On the tested prototype the lowest discrimination level
was \unit[4]{mV} and was restricted only by the noise level of the
amplifier stage. The threshold level is set using 8-bit code. The
utilization of two channels makes it possible to obtain a timing
accuracy of $T = \pm\unit[25]{ps}$ while the input signal ranges
from \unit[4]{mV} to \unit[3]{V}. The amplitude discriminator
commutates the output signals from the first or second timing
channels depending on the amplitude of the input signal. The output
shapers of the unit generate the signals in NIM and LVDS standards,
matching the requirements of the receiving units.

The buffer amplifier incorporated into the tested CFD prototype has
gain $K=1$ and serves as an input signal splitter.

\subsection{\TZERO{} Vertex Unit}

Determination of the IP of each collision and comparing it to preset
minimum and maximum values is one of the main trigger functions of
the \TZERO{} detector. The unit intended to perform these operations
is the \TZERO{} Vertex Unit or TVDC. The main parameters of the TVDC
are determined by the expected size of the interaction area
(\unit[0.7]{m}), nominal resolution of the measurements ($ \pm
\unit[1.5]{cm} $), and the working frequency of the \LHC{}
(\unit[40]{MHz}). Accordingly, the TVDC should meet the following
requirements:

\begin{itemize}
\item time range of  $ \pm \unit[2.5]{ns} (\unit[5]{ns}) $;
\item nominal time resolution of \unit[20]{ps} (for 8-bit conversion);
\item total dead time below  \unit[25]{ns}.
\end{itemize}

The block diagram of the tested prototype is shown in
\figref{fig:t0:fastelectronics:tvdc}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/tvdc}
  \caption[TVDC unit]{TVDC unit.
  \newline
    \begin{tabular}{clcl}
      C   &=  Comparator              & BR  &=  Buffer Register\\
      CC  &=  Coincidence Circuit     & ADC &= Amplitude-Digital Converter\\
      UV  &=  Univibrator             & BA  &=  Buffer Amplifier\\
      AC  &=  Anticoincidence Circuit & \&  &=  AND circuit\\
      CS  &=  Charging current Switch & OR  &=  OR  circuit.\\
      DS  &=  Discharge Switch\\
    \end{tabular}}
  \label{fig:t0:fastelectronics:tvdc}
\end{figure}

The main components of TVDC are the Time to Amplitude Converter
(TAC) followed by a flash ADC with digital discriminators for
\TZERO{} Vertex signal generation. The TAC is designed to generate
an output signal only when both input signals come within the
allowed time interval (\unit[4]{ns}) and in the presence of the
\LHC{} clock signal (or Bunch Crossing signal). The 8--bit flash ADC
AD9002 used in the prototype has the encoding frequency
\unit[150]{MSPS}, and \unit[20]{ps} granularity. The digital
comparator K1500CP166 has a delay equal to \unit[3]{ns}. It
generates the \TZERO{} Vertex output signal when the code of the
flash ADC coincides with one of the preset (allowed) codes of the
vertex position. The total dead time of the TVDC unit is, as
expected, below \unit[25]{ns}.

The performance of the first prototype of the \TZERO{} Vertex Unit
obtained during the July 2003 test run at CERN is shown in
\figref{fig:t0:fastelectronics:vertex}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/vertex}
  \caption{Performance of the first prototype \TZERO{} Vertex Unit
    during the July 2003 test experiment using PS beams at CERN. The four curves illustrate trigger
efficiency as a function of vertex position recalculated for 4
different settings of the upper and lower threshold.}
  \label{fig:t0:fastelectronics:vertex}
\end{figure}

\subsection{\TZERO{} Multiplicity Discriminator}
The Multiplicity Discriminator (MPD) generates three logical signals
corresponding to the three pre-set levels of desired particle
multiplicity. The MPD output goes to Trigger Signal Module (TSM)
where all the other \TZERO{} trigger signals are converted to the
form acceptable by the Central Trigger Processor (CTP). In addition
to the trigger function MPD generates analogue sum that will be
digitized and stored by \ALICE{} DAQ. The block diagram of the first
prototype to be successfully tested at CERN (in June 2004) is shown
in \figref{fig:t0:fastelectronics:multiplicity}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/multiplicity}
  \caption{Multiplicity Discriminator. S indicates summation unit.}
  \label{fig:t0:fastelectronics:multiplicity}
\end{figure}

The 12 analogue signals originating either from \TZERO{A} or from
\TZERO{C} arrive at the input of an analogue summator ($\Sigma$).
The summed--up signals than go to the inputs of the three
comparators (D1, D2, D3) and one analogue output. The threshold
voltages, set with 8-bit resolution, correspond to the low, middle,
and high level of multiplicity. These voltages are shaped by the
multi-channel Digital to Analogue Converter (DAC) using the digital
octal codes written to the RG1- RG3 data registers, allowing for
remote control. After additional stretching, the output signals from
the comparators come to the outputs of low (Output1), middle
(Output2), and high (Output3) levels of multiplicity.

\subsection{Mean Timer}

Just as the time difference between \TZERO{A} and \TZERO{C} gives
vertex position along the $z$-axis, the average of \TZERO{A} and
\TZERO{C} arrival times cancels this dependence and yields
position-independent collision time (plus some fixed delay along the
cables, fast electronics, etc.). On-line calculation of the
collision time is accomplished by a time-coordinate compensator
(Mean Timer) whose schematic diagram is shown in
\figref{fig:t0:fastelectronics:timemeaner}. The prototype of the
Mean Timer has been tested in in-beam conditions at \CERN{} during
the July 2003 run and yielded consistency of about \unit[10]{ps} of
compensation error, as shown in
\figref{fig:t0:fastelectronics:perftimemean}. Since the Mean Timer
signal (\TZERO{}) is extracted from two independent pulses, \TZERO{}
time resolution is better from that of a single detector by about
$\sqrt{2}$. The actual results from the 2003 experiment yielded
$\sigma_{\text{T0}} = \unit [28] {ps} $.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/timemeaner}
  \caption{Mean Timer. UV1, UV2 are Univibrators (monostable
    multivibrators); Sw1 and Sw2 indicate switches; GI is a current
    generator; FC = Fast Comparator; and F is a shaper for forming the output pulses. }
  \label{fig:t0:fastelectronics:timemeaner}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/perftimemean}
  \caption{Performance of the Mean Timer determined during the July 2003
    test run at CERN.}
  \label{fig:t0:fastelectronics:perftimemean}
\end{figure}

\subsection{Variable Delay Unit}

Strict matching of the characteristics of all 24 PMT units forming
the \TZERO{} detector is simply not possible. As a result each
\TZERO{} detector tube will operate at a different and individually
selected voltage. This alone will cause differences in the arrival
times of the signals of up to few ns. To equalize these and
comparable differences we have designed the Variable Delay Unit
(VDU). Each VDU channel consists of an NIM to ECL converter, an
MC100EP195 chip with programmable delay, and an output ECL to NIM
converter. A dedicated register connected through an interface with
VME is used to record the value of delays. The block diagram of the
current prototype VDU is shown in
\figref{fig:t0:fastelectronics:delay}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/delay}
  \caption{Block diagram of prototype VDU.}
  \label{fig:t0:fastelectronics:delay}
\end{figure}

\subsection{Charge to Time Converter}
\label{sec:t0:fastelectronics:qtc}

\TZERO{} will not develop its own readout system but use the one
developed for \TOF{}, as explained in the next chapter. The normal
procedure to digitize and store the amplitudes of PMT signals would
be to use a Charge to Digital Converter (QDC). Although \TOF{} does
have provision for a QDC it does not have sufficient resolution to
cope with the dynamic range expected from the \TZERO{} signals. As
the resolution of the \TOF{} TDC is much better it was decided to
develop a Charge to Time Converter (QTC) and connect its output to
the \TOF{} TDC for digitization, readout and storage. The same
approach is used to digitize the summed amplitude used for
multiplicity determination. For unification of the measurements and
data transmission to the DAQ, HPTDC converters (designed for other
\ALICE{} detectors) are used for final charge-to-code conversion.
Thus, the QTC converts the amplitude (charge) to the time delay
adequate for coding using an HPTDC converter. The logarithmic
characteristic of the converter is necessary due to the wide range
in the PMT signal amplitude. From the output of the comparator, the
signal goes to the differential circuit (Shaper 1 and 2 in
\figref{fig:t0:fastelectronics:qtc}), generating pulses bound to the
rising and falling edges of the comparator pulse. These pulses go to
the ECL-LVDS level converter for shaping into the LVDS format.

The first prototype of the QTC unit was successfully tested during
the June 2004 experiment at \CERN{}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/qtc}
  \caption{Schematic layout of the QTC unit.}
  \label{fig:t0:fastelectronics:qtc}
\end{figure}

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

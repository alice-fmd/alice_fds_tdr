%%
%% $Id: finalbeamtest.tex,v 1.16 2005-06-14 15:43:42 hehi Exp $
%%
%% Tomasz Malkiewicz 11.08.2004
%%

\section{Beam Test of complete \TZERO{} system}

The beam tests described in \charef{cha:t0:inibeamtest} were
essential to verify the validity of the concept of the detector and
to determine the baseline parameters of the main \TZERO{}
components. Since then progress has been made both in the detector
R\&D and in the development of the electronics. There have also been
important advance in \ALICE{} integration, down to the defining of
cables, connectors, etc. With prototypes of all major electronics
components, the actual cables, PMTs, and quartz radiators, we have
made additional in-beam tests recreating closely the actual \ALICE{}
environment. The goal of these tests was to check the detectors and
electronics with real signals as opposed to laser and generator
pulses in low-noise laboratory conditions and to find out if the
baseline parameters (such as the size of the quartz radiators) are
indeed the best possible. Two measurements with test beam were held
in July 2003 and in June 2004. The first concentrated on the
electronics prototypes. During the 2004 session the second
generation of prototypes was tested but the main emphasis was on
light collection and the pulse shape obtained by various quartz
radiators. Otherwise both experiments were very similar so, for the
sake of brevity, only the 2004 setup is described.

\subsection{Experimental Setup}

The beam (a mixture of \unit[6]{GeV/c} negative pions and
kaons) that for the purpose of our measurements can be considered as
Minimum Ionizing Particles (MIP) as they could easily penetrate all
(usually up to four) detectors placed in their path without any
nuclear interactions nor appreciable loss of energy. A typical
configuration with four \TZERO{} detector modules is shown in
\figref{fig:t0:finalbeamtest:detconf}. Usually the first and the
last detector worked as triggers while the two middle detectors were
investigated. The modules were placed one after the other and were
well aligned with the beam axis. The width of the beam was
considerably larger than \unit[3]{cm} - the diameter of the largest
tested detector.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/detconf}
  \caption{Photograph of a typical detector configuration during the
    2004 test run at CERN PS. There are four PMT + quartz detector
    units. Each unit has its own aluminum casting with plastic end cups.}
  \label{fig:t0:finalbeamtest:detconf}
\end{figure}

Depending on the need, the individual units could be shifted in
respect to each other (to change the relative timing), tilted (to
reduce the working area and to check the beam profile) or inverted
\unit [180]{${}^{\circ}$} to study the pulse from the particles
traversing the radiator in the ``wrong direction''.

Each PMT divider ended with a short pigtail of cables going to a
small patch panel on the supporting rail. To reproduce exactly the
expected configuration in \ALICE{} the PMT signals were sent from
the patch panel to the shoebox prototype (see
\figref{fig:t0:finalbeamtest:shoeprot}) over a \unit[6]{m} long
cable. From the shoebox the signals were delivered over \unit[25]{m}
long cables to the main electronics rack, just as it will be in the
final setup.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/shoeprot}
  \caption{Photograph of the shoebox prototype (for two input
    channels) tested at CERN PS in June 2004. The input cables are
    seen entering from below and the output cables exit from the top.
    The ribbon cables deliver $ \pm \unit[6]{V} $ of power.}
  \label{fig:t0:finalbeamtest:shoeprot}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/rack}
  \caption{Test version of the main \TZERO{} electronics rack
    photographed during the 2004 run at CERN.}
  \label{fig:t0:finalbeamtest:rack}
\end{figure}

\subsection{Tested Quartz Radiators}
\label{sec:t0:finalbeamtest:quartz}

The baseline size of the \TZERO{} quartz radiator was \unit[30]{mm}
long (see calculations in \secref{sec:t0:detector:cherenkov}) and
\unit[30]{mm} in diameter. This diameter coincides with the outer
diameter of the PM tube (see \figref{fig:t0:detector:feu187}) but is
substantially larger than the diameter of the photocathode
(\unit[20]{mm}). Larger cross section of the radiator gives a larger
solid angle, increasing the overall detection efficiency. In broad
beam conditions and assuming perfect electronics the detection
probability can be taken as simply proportional to the cross section
of the radiator. The price to pay when the diameter of the radiator
exceeds that of the photocathode is the deterioration of the shape
of the pulse, leading to the loss of time resolution, and decreased
efficiency. The best way to verify the results of simulations is to
take real measurements. We tested radiators of 3 diameters (the
length of all radiators was \unit[30]{mm}):

\begin{itemize}
\item \unit[30]{mm} (the current baseline; same as the PMT)
\item \unit[20]{mm} (matching that of the photocathode)
\item \unit[26]{mm} (an arbitrary intermediate value)
\end{itemize}

From January 2005 the new baseline is \unit[20]{mm} in diameter and
\unit[20]{mm} long.

\subsection{Time Resolution}

The most direct way to determine the time resolution of a detector
($\sigma_{\text{det}}$) is to use two identical detectors as start
and stop and to analyse the collected \TOF{} distribution of
mono-energetic particles.  Ideally, this would be a Gaussian
distribution, therefore:

\begin{equation}
 \sigma_{TOF} \approx \frac {\text{FWHM}_{TOF}} { 2.35}
\end{equation}
and
\begin{equation}
  \sigma_{det} = \frac {\sigma_{TOF}} {\sqrt{2}}~,
\end{equation}
where \TOF{} stands for Time--Of--Flight, FWHM is full width at half
maximum and $\sigma_{det}$ is the time resolution of the detector (in
our case one quartz + PMT \Cherenkov{} module). The \ALICE{} requirement
for \TZERO{} is

\begin{equation}
  \sigma_{det} \leq \unit[50]{ps}
\end{equation}

A typical \TOF{} spectrum obtained during the June 2004 experimental
session is shown in \figref{fig:t0:finalbeamtest:tofspec} and the
result for all 3 radiators is summarized in \tabref{tab:t0:diam}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/tofspec}
  \caption{Typical \TOF{} spectrum obtained during the 2004 test run
    at CERN PS. FWHM is \unit[94]{ps}, which corresponds to $\sigma =
    \unit[28]{ps} $.}
  \label{fig:t0:finalbeamtest:tofspec}
\end{figure}


\begin{table}[htbp]
  \centering
  \caption{Dependence of FWHM and time resolution ($\sigma$) on the
    diameter of the \Cherenkov{} radiator. The values in brackets were
    obtained when the signals were amplified in the shoebox instead of
    going directly to CFD. Current improvements to the shoebox design
    should reduce these values, so they are preceded with a less than or
    equal to sign.}
  \label{tab:t0:diam}
  \vglue0.2cm
  \begin{tabular}{|l|c|c|}%\\
    \hline
    \textbf{Radiator diameter} &
    \textbf{FWHM (TOF)} &
    \textbf{$\sigma$ (\TZERO{}detector)} \\
    \hline
    \unit[30]{mm}  &
    \unit[122]{ps}   &
    \unit[37]{ps} \\
    &
    ($\leq$ \unit[140]{ps})  &
    ($\leq$ \unit[42]{ps})  \\
    \hline
    \unit[26]{mm}  &
    \unit[112]{ps} &
    \unit[34]{ps} \\
    &
    ($\leq$ \unit[128]{ps})  &
    ($\leq$ \unit[39]{ps}) \\
    \hline
    \unit[20]{mm}  &
    \unit[94]{ps}  &
    \unit[28]{ps} \\
    &
    ($\leq$ \unit[115]{ps}) &
    ($\leq$ \unit[35]{ps}) \\
    \hline
  \end{tabular}
\end{table}


As expected, the best resolution is achieved with the smallest
radiator, but even with the current version of the shoebox
electronics all results are well within the \ALICE{} specification.

\subsection{Pulse Shape and Efficiency}

\Figref{fig:t0:finalbeamtest:outpmt} shows measured amplitudes of
the PMT output obtained for 3 different radiator sizes.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/outpmt}
  \caption{Dependence of the light output of a PMT on the diameter of
    the radiator produced with minimum ionizing particles.
    \unit[1]{MIP} results in about 180 photoelectrons ejected from the
    cathode of the PMT.  For easier comparison the spectra were
    smoothed and their areas normalized.}
  \label{fig:t0:finalbeamtest:outpmt}
\end{figure}

The \unit[30]{mm} diameter radiator produces a broad amplitude
spectrum with a characteristic two-hump structure that we have
observed before (\figref{fig:t0:testbeam:f9}) and is well reproduced
by simulations (\figref{fig:t0:montecarlo:f12}). The lower bump
comes from light loss through the area around the photocathode. This
gap is smaller for a \unit[26]{mm} radiator but qualitatively the
spectrum is not much different from that of a \unit[30]{mm}
radiator. A great improvement occurs for the smallest radiator i.e.,
when the diameter does not exceed that of the photocathode. In this
case all the light produced inside the volume of the radiator is
directed to the photocathode. The spectrum shifts up and becomes
Gaussian, reflecting the statistical nature of photo conversion.
There is a broad and clear area separating the peak from the noise
(pedestal) level.

As discussed before, the consequence of the radiator's reduced
diameter is the reduction in the covered solid angle (detection
efficiency) that is roughly proportional to the second power of the
diameter (area). This, however, is true only if the discriminator
threshold can be placed at sufficiently low value. A rise in the
threshold will lead to loss of efficiency.
\Figref{fig:t0:finalbeamtest:thresh} illustrates the dependence of
efficiency on the threshold level, calculated using the amplitudes
spectra of \figref{fig:t0:finalbeamtest:outpmt}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/thresh}
  \caption{Efficiency as a function of the threshold calculated using
    measured amplitude spectra. 100\% was assigned to the
    \unit[30]{mm} radiator and the maximum values for the 26 and
    \unit[20]{mm} diameter radiators were scaled accordingly.}
  \label{fig:t0:finalbeamtest:thresh}
\end{figure}

The steep drop of efficiency for 30 and \unit[26]{mm} diameter
radiators with the increase of the threshold is somewhat disturbing.
It means that even if no noise problems prevent the setting of the
threshold value sufficiently low, any instability will cause
noticeable variations in efficiency. In contrast the characteristics
of the \unit[20]{mm} quartz are excellent with a prominent and broad
plateau.

\subsection{Sensitivity to the Particle Backsplash}

\Cherenkov{} radiation is strictly directional but since the
polished walls of the quartz radiator work as a mirror, particles
travelling in the ``wrong'' direction will also produce detectable
light pulses. This undesirable effect can be partially reduced by
covering the front surface of the radiator with a light-absorbing
layer, for instance by glueing (to get optical contact) a black
paper on top of it. It works well for particles travelling exactly
in the opposite direction but those at intermediate angles will
inevitably produce some signals. This may not be a problem for
\TZERO{C} despite being just next to the muon absorber, because the
absorber was designed to minimize particle backsplash. This,
however, will not be the case for \TZERO{A}, which is placed in the
proximity of a vacuum pump, valve and support structure. It is
therefore important to know what kind of spectra are to be expected
from the ``wrong'' particles.  The largest amplitudes from strain
particles (the worst-case scenario) arise when they travel in
exactly the opposite direction and the front of the radiator is free
from optical contact with a light-absorbing material. The results
are shown in \figref{fig:t0:finalbeamtest:resp26} (\unit[26]{mm}
diameter radiator) and \figref{fig:t0:finalbeamtest:resp20}
(\unit[20]{mm} radiator).

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/resp26}
  \caption{Response of the \TZERO{} module with \unit[26]{mm} diameter
    radiator to MIPs entering directly from the front (solid line)
    and from the opposite direction (dashed line).}
  \label{fig:t0:finalbeamtest:resp26}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/resp20}
  \caption{Response of the \TZERO{} module with \unit[20]{mm} diameter
    radiator to MIPs entering directly from the front (solid line)
    and from the opposite direction (dashed line).}
  \label{fig:t0:finalbeamtest:resp20}
\end{figure}

It is reassuring to see that with the \unit[20]{mm} radiator even
the largest signals from strain particle can be effectively (without
the loss of pulses from good events) discriminated against by
raising the threshold. For 30 and \unit[26]{mm} radiators this would
not be the case.

\subsection{Light Transmission Measurements}

The radiators used in our experiments were made at a different time
and presumably also from different batches of the quartz material.
To check the consistency and quality of production we have made
light transmission measurements for each of the tested radiators.
The coved wavelength (\unit[200--600]{nm}) matches that of PMT
sensitivity. The results are presented in
\figref{fig:t0:finalbeamtest:wave}. They show that while there are
indeed small differences between the samples, the overall quality of
quartz radiators is good. The sample with the highest transmission
(sample 1) was from the production run of \unit[30]{mm} diameter
radiators.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/wave}
  \caption{Light transmission through \unit[3]{cm} thick samples of quartz
    radiators as a function of the wavelength. }
  \label{fig:t0:finalbeamtest:wave}
\end{figure}


\subsection{Conclusions}

The most important conclusion of the beam tests of the complete
\TZERO{} system is that with the actual cables, connectors and
prototypes of the electronics and for all tested radiator types the
required time resolution of $\sigma \leq \unit[50]{ps}$ has been
achieved.

The tests demonstrated the excellent amplitude spectrum of the
\unit[20]{mm} diameter radiators. It is so much better than the
\unit[30]{mm} radiator that it justifies considering a change in the
current baseline. The only drawback would be reduced efficiency in
\ppCol{} collisions. This reduction can however be compensated, if
necessary by the doubling the number of \TZERO{} modules in the
\TZERO{A} array, as demonstrated in
\tabref{tab:t0:finalbeamtest:detmod}.

\begin{table}[htbp]
  \centering
  \caption{Simulated efficiency in \ppCol{} collisions (\PYTHIA{6.125}; 1000
    events) for the baseline configuration (12--12), and for the
    configuration with 24 detectors on \RB{24} side (24--12).}
  \label{tab:t0:finalbeamtest:detmod}
  \vglue0.2cm
    \begin{tabular}{|p{.2\textwidth}|p{.2\textwidth}|p{.2\textwidth}|p{.2\textwidth}|}%\\
  \hline
  \textbf{Number of detector} \par
  \textbf{modules in \TZERO{A}} &
  \textbf{Number of detector} \par
  \textbf{modules in \TZERO{C}} &
  \textbf{Efficiency in \ppCol{}} \par
  \textbf{with \unit[30]{mm} diameter radiators} &
  \textbf{Efficiency in \ppCol{}} \par
  \textbf{with \unit[20]{mm} diameter radiators}\\
  \hline
  12 & 12 & 53\% & 36\%  \\
  \hline
  24 & 12 & -- & 49\%  \\
  \hline
\end{tabular}
\end{table}

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  MIPs

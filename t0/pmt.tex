%%
%% $Id: pmt.tex,v 1.12 2004-09-08 11:34:50 cholm Exp $
%%
\section{Gain and Time Properties of Fine--Mesh PMTs}

The initial studies of the timing properties of fine--mesh
phototubes as well as their behaviour in the magnetic field were
undertaken in collaboration with Rice University group (USA) and
PNPI group (St.~Petersburg) for the STAR \TOF{} system
\cite{t0:AHM97a,
  t0:AHM97b}. The PMTs under study were Hamamatsu models R5505, and
R3432--01, and the Russian FEU--527 produced by the Moscow \textit
{MELZ} enterprise in cooperation with the St.~Petersburg \textit
{Electron} enterprise. Later \textit {Electron} started to produce
the same PMTs under the name FEU--187. In 2002 the measurements were
repeated at PNPI by the MEPHI group for the latest samples:
Hamamatsu R5506 (2 pieces), R3432--01(1 piece) and FEU--187 (similar
to FEU--527; 8 pieces). In these measurements light emitting diode
was used to imitate a scintillation pulse. We studied the gain
behavior at different HV values resulting in a wide dynamic range of
the output signals. Typical results of these measurements for
FEU--187 are given in \figref{fig:t0:pmt:f3}.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f3}
  \caption{Typical characteristics of FEU--187: gain versus high
    voltage (at \unit[0.5]{T} magnetic field) and relative gain versus
    magnetic field at different high voltages.}
  \label{fig:t0:pmt:f3}
\end{figure}


The behavior of the gain in a magnetic field at nominal values of HV
does not differ from those obtained for R5505 and FEU--527. At lower
voltages the gain falls somewhat faster with the increase of
magnetic field compared with nominal high voltage. For all samples
we measured the gain in the magnetic field at voltages corresponding
to 100, 10 and \unit[1]{\%} of the nominal gain. Our conclusions are
as follows:
\begin{enumerate}
\item The differences between the samples are not large.
\item The gain difference for different samples can be easily
  compensated by changing HV bias.
\item All studied PMTs could operate in magnetic field in a wide
  dynamic range (1:100).
\end{enumerate}


In order to measure the time characteristics of the tested PMTs at
conditions close to those in the \Cherenkov{} detector we have used
a pulsed laser (\unit[70]{ps} FWHM). Two types of measurement were
made with the laser. In the first run we changed the intensity of
the laser pulse and monitored the time resolution and the amplitude.
The results are shown in \figref{fig:t0:pmt:f4}.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f4}
  \caption{Gain and time resolution versus high voltage for typical
    FEU--187. Arbitrary light flux equal to unity corresponds to
    approximately 100 photoelectrons.}
  \label{fig:t0:pmt:f4}
\end{figure}

As expected, the time resolution initially improves with the
increase of the light flux and then levels off. At the fluxes
expected from the \Cherenkov{} radiator the resolution should stay
below \unit[50]{ps}. Also the flux--amplitude characteristic is
good.

In the second run the number of photoelectrons was fixed at about
120--140 and the time resolution and the amplitude were measured as a
function of the applied HV. The results of this run are given in
\figref{fig:t0:pmt:f5}.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f5}
  \caption{Time resolution and amplitudes of the output signal of PMT
    FEU--187 as functions of high voltage at a fixed light flux equal
    approximately to 120--140 photoelectrons (\unit[1]{MIP}).}
  \label{fig:t0:pmt:f5}
\end{figure}

One can see that the time resolution of the fine--mesh PMT FEU--187
remains below \unit[40]{ps} in a broad dynamic range (at least
1:120) of the output amplitudes. A small decrease in the time
resolution at low amplitudes is caused by the increase in electronic
jitter \cite{t0:GRI00a}.

\subsection{Routine PMT Tests in Magnetic Field}

Good test results with selected PMT units cannot guarantee the
proper behaviour of the entire production batch. To be sure of that
each PMT has to be thoroughly tested in magnetic field. Since each
PMT is slightly different, the full set of performance curves at
various field and HV settings will be taken and stored in the
detector database. To do that a small air-cooled magnet was designed
and constructed (\figref{fig:t0:pmt:magnet}). It provides uniform
magnetic field of up to \unit[0.5]{T} inside a volume large enough
to accommodate one \TZERO{} detector unit. PMT tests with this
magnet will be periodically repeated to monitor the performance
stability of the \TZERO{} modules.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/magnet}
  \caption{A small, air-cooled magnet for testing PM tubes in up to \unit[0.5]{T} magnetic
    field.}
  \label{fig:t0:pmt:magnet}
\end{figure}


%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  Hamamatsu

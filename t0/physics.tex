%%
%% $Id: physics.tex,v 1.15 2004-09-09 16:28:48 cholm Exp $
%%
\section{Physics Objectives}

The \TZERO{} detector is required to fulfil the following functions:

\begin{enumerate}
\item To supply main signals to the \ALICE{} \LVL{0} trigger.
\item To deliver an early (prior to the \LVL{0} trigger) ``wake--up''
  trigger to the \TRD{}.
\item To give a start signal with good time resolution for the
  Time--of--Flight (\TOF{}) particle identification system in
  \ALICE{}.
\end{enumerate}

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.8\textwidth]{t0/figures/detT0}
  \caption{Photography of the prototype of \TZERO{C}.}
  \label{fig:t0:physics:detT0}
\end{figure}

The trigger functions requested from \TZERO{} are as follows:

\begin{itemize}
\item to measure the approximate vertex position;
\item to give a rough estimate of event multiplicity;
\item to inform that at least one of the arms of the \TZERO{} detector has
  registered a valid pulse.
\end{itemize}

The first trigger function is crucial for discriminating against
beam--gas interactions. With \unit[50]{ps} time resolution one
should obtain $\pm \unit[1.5]{cm}$ accuracy in vertex determination.
If the vertex position falls within the pre--defined values, an
\LVL{0} trigger signal called \emph{\TZERO{}--vertex} will be
produced.

The second feature (multiplicity determination) will be an important
back--up option for \VZERO{} that covers a considerably larger
pseudorapidity range. For \TZERO{}, the covered pseudorapidity range
is $2.9< | \eta | <3.3$ on the \RB{26} and $4.5 <| \eta |< 5$ on the
\RB{24}. The measured multiplicity will be compared to 2 pre--set
values to generate one of the three possible trigger signals:
\emph{\TZERO{}(minimum bias)}, \emph{\TZERO{}(semi--central)}, or
\emph{\TZERO{}(central)}, corresponding to low, intermediate, and
high multiplicities. There will be only two threshold values because
the minimum bias signal is identical to \emph{\TZERO{}--vertex}
(sufficient multiplicity to have triggered both halves of the
\TZERO{} detector). Since the \TZERO{} detector generates the
earliest \LVL{0} trigger signals, all these signals should be
strictly generated on--line without the possibility of any off--line
corrections.

The early wake--up signal to the Transition Radiation Detector also
must be strictly produced on--line. The full list of trigger signals
delivered by \TZERO{} is listed in \tabref{tab:t0:triggers}.

The \TZERO{} detector is the only \ALICE{} sub--detector capable of
delivering a high--precision start signal for the \TOF{} detector.
This \TZERO{} signal must correspond to the real time of the collision
(plus a fixed time delay) and be independent of the position of the
vertex. The required precision of the \TZERO{} signal must be better
or at least equal to that of the \TOF{} detector ($\sigma =
\unit[50]{ps}$). Generating the \TZERO{} start will not be done by any
other detector in \ALICE{} so the quality of the \TZERO{} time
resolution will directly influence the quality of \TOF{}
identification. In favourable cases, mostly for HI collisions, one may
expect some further improvement of \TZERO{} time resolution in
off--line analysis. For that purpose it is important to read out and
store the time and amplitude of each PMT of the \TZERO{} array (see
the prototype of \TZERO{C} in \figref{fig:t0:physics:detT0}).

\begin{table}[htbp]
  \centering
  \caption{List of trigger signals delivered by the \TZERO{} detector to the
    Central Trigger Processor (CTP) and the Transition Radiation
    Detector (\TRD{}).}
  \label{tab:t0:triggers}
  \vglue0.2cm
  \begin{tabular}{|p{.3\textwidth}|p{.3\textwidth}|p{.3\textwidth}|}%\\
    \hline

    \textbf{Trigger signal}& \textbf{Purpose}& \textbf{Adjustable
      parameters}\footnote{Other than HV, individual thresholds, delays,
      etc. which will be set using laser calibration during the tuning phase
      of the detector before each run.}\\ \hline

    \textbf{\TZERO{}--vertex}&
    \emph{Beam gas suppression};\par
    Will also serve as \emph{minimum bias};\par
    \emph{Can not} be used to study \emph{beam gas interactions}
    because \Cherenkov{} detectors are  direction sensitive.&
    \emph{Two programmable thresholds}\par
    defining the upper and lower limit for the acceptable time
    difference in arrival time between \TZERO{C} and
    \TZERO{A}. The maximum acceptable range is about
    $\pm\unit[80]{cm}$. \\ \hline

    \textbf{\TZERO{C}(\TZERO{Right})}& Useless as stand--alone.  To be combined
    with \VZERO{} &
    None \\ \hline

    \textbf{\TZERO{A}(\TZERO{Left})}& Useless as stand--alone.  To be combined
    with \VZERO{} &
    None\\
    \hline

    \textbf{\TZERO{} semi--central}& \emph{Semi--central collisions}
    (back--up for \VZERO{})&
    \emph{One:} threshold level\\ \hline

    \textbf{\TZERO{}--central}& \emph{Central collisions} (back--up for
    \VZERO{})& \emph{One:} threshold level\\ \hline

    \textbf{\TRD{}{} wake--up}& \emph{Wake--up call }&
     \emph{None} (There
    will be 24 delays and 24 thresholds to be adjusted jointly by the \TZERO{} and
    \TRD{} group using laser calibration before each run.)\\ \hline

  \end{tabular}
\end{table}

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  pseudorapidity

%%
%% $Id: detector.tex,v 1.17 2005-06-21 08:13:02 hehi Exp $
%%
\section{\TZERO{} Detector Overview}

All in all, three different techniques were considered and tested
for the \TZERO{} detector design: microchannel plate detectors
(MCP), resistive plate chambers (RPC) and \Cherenkov{} radiators
optically coupled to photomultiplier tubes (PMT). The most ambitious
and challenging of the tested alternatives was based on MCP
\cite{t0:ANT99, t0:BON02}. If such a detector operates properly and
if it covers sufficient pseudorapidity range it will work not only
as \TZERO{} but also as \VZERO{} and \FMD{}{}. In other words, it
will combine the functions of all 3 forward detectors in one.
Unfortunately MCP technology is also the most expensive, requires
operation in good vacuum and has not been used before in similar
applications. Therefore, considering the available time, manpower
and resources as well as proven performance, the \ALICE{}
collaboration approved in spring 2001 the PMT solution as the
baseline for the \TZERO{} detector.

\subsection{Detector Module Design}

Once the PMT technology was chosen, remaining issues included the
right tubes, selection of \Cherenkov{} radiators and design the
electronics. In this we could learn from the experience of the
PHENIX experiment at RHIC, where a very similar detector, based on
quartz radiators and Hamamatsu fine--mesh phototubes, has been
built and is operated by the Hiroshima group \cite{t0:IKE98}. This
group, using \unit[1.6]{GeV/c} negative pions, has demonstrated a
time resolution of \unit[50]{ps} after off--line pulse shape
correction. A \unit[100]{ps} resolution was obtained without any
off--line correction with a simple leading--edge discriminator.

%% \subsection{PMT Choice FEU--187}
\subsection{Photomultiplier Tube (PMT)}
\label{sec:T0:PMTtube}

Currently there are only two manufacturers in the world producing
PMTs capable of operation in a magnetic field and fulfilling
\TZERO{} specifications. The tubes are \emph{R5506} from the
Japanese company Hamamatsu (PHENIX's choice) and \emph{FEU--187}
from the Russian firm `Electron'. Both products are fine--mesh
phototubes with good timing properties, UV entrance windows, which
can operate in the axial magnetic field of \unit[0.5]{T}.

In a series of extensive tests we have verified that the differences
in performance between the R5506 and the FEU--187 are negligible,
while the spread in key characteristic parameters of units delivered
by the manufacturer in the same batch are very big.  For instance,
Hamamatsu's catalogue quotes anode sensitivity of
\unit[40]{$\mu$A/Lm}, but the actual value can be anywhere from 5 to
300. To produce a detector array giving \unit[50]{ps} time
resolution for any combination of the firing PMTs, they must all
match closely. Ordering closely matched tubes sharply increases the
price and requires good -- preferably direct -- contact with the
producer, so that user can verify the units selected. Since we have
no access to the Hamamatsu plant, but we do have good contacts with
`Electron', we decided to use FEU--187 (presented in
\figref{fig:t0:detector:feu187}).

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.8\textwidth]{t0/figures/feu187}
  \caption{FEU--187. The outside diameter of the photomultiplier tube is \unit[30]{mm}.}
  \label{fig:t0:detector:feu187}
\end{figure}

The additional factor in support of our choice was the price (even
an unselected R5506 is twice as expensive as FEU--187). As a
precaution the tubes in our design will be interchangeable. This
means that if the need arises the change from FEU--187 to R5506 will
be possible with only minimal modifications.

\subsection{\Cherenkov{} Radiator}
\label{sec:t0:detector:cherenkov}

It is uncertain whether a fast scintillator will survive 10 years of
operation under \LHC{} conditions, since most organic materials
quickly lose transparency, especially at short wavelengths when they
are exposed to high radiation doses. For these reasons we have opted
for \Cherenkov{} detectors with fused quartz radiators. Quartz is
known to be radiation hard and is transparent to UV. The other
advantage of the \Cherenkov{} option is a very fast light emission
in comparison with other fast scintillators.

The length of the quartz radiator was estimated based on the
assumption that the PMT will respond to the \unit[200-550]{nm} band
of the \Cherenkov{} light emission spectrum --- see
\figref{fig:t0:detector:wvlenght}.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.5\textwidth]{t0/figures/wvlenght}
  \caption{Photocathode sensitivity measured for two different
    production batches of FEU--187 photomultiplier tube.}
  \label{fig:t0:detector:wvlenght}
\end{figure}

The average number of emitted photons per \unit[1]{cm} of radiator
is given by the expression:

\begin{equation}
  N_{ph} = 2\pi\alpha \left(1/\lambda_2 -
  1/\lambda_1\right)\sin^2\Theta~.
\end{equation}

For fused quartz the refraction index is $n = 1.458$, yielding
$\cos\Theta = 1/n = 0.686$, $\sin^2\Theta = 0.53$. Hence the average
number of photons per \unit[1]{cm} length will be about 770, 440 and
250 for the wavelength bands \unit[200-550]{nm}, \unit[300-550]{nm},
and \unit[350-550]{nm}, respectively. With these values, assuming an
average quantum efficiency of the photocathode equal to 15\%, we
estimate 112, 66, and 38 emitted photoelectrons for the
corresponding wavelength bands for a \unit[1]{cm} long radiator. To
triple the number of photoelectrons one needs a \unit[3]{cm} long
quartz radiator. According to the actual experimental data given in
\cite{t0:IKE98}, Hamamatsu R5505 with a conventional borosilicate
glass entrance window (spectral sensitivity \unit[300-550]{nm}) or
R5506 with a UV glass entrance window (spectral sensitivity
\unit[200-550]{nm}) should give enough photoelectrons to achieve a
\unit[50]{ps} time resolution for the very short and well--focused
\Cherenkov{} light emission. We have verified this with our own
measurements of the time resolution and dynamic range of the three
types of PMTs: Hamamatsu R3432--01 (which is quite similar to
R5505), Hamamatsu R5506 and FEU--187.

All tests and calculations indicate that a \unit[3]{cm} long quartz
is a good choice for the \Cherenkov{} radiators of the \ALICE{}
\TZERO{} detector.

\subsection{Location and Size of the Two \TZERO{} Arrays}
\label{sec:t0:detector:arrays}

To measure the exact time of an event and the vertex position along
the $z$--axis, the \TZERO{} detector should consist of two arrays of
\Cherenkov{} counters with the IP in--between. On the muon absorber
side (\RB{26}) the distance of the array from the IP is limited by
the position of the absorber nose. The distance from the IP to
\TZERO{C} is \unit[70]{cm} -- as close as possible to the absorber.
On the opposite side (\RB{24}) the distance from \TZERO{A} to the IP
is about \unit[3.6]{m}. \TZERO{C} covers the pseudorapidity range
$2.9< |\eta| <3.3$, and \TZERO{A} of $4.5 < |\eta| < 5$. The chosen
location of the two \TZERO{} arrays inside \ALICE{} is shown
schematically in \figref{fig:t0:detector:f1WHT}.

\begin{figure}[htbp]
  \centering \includegraphics[width=.95\textwidth]{t0/figures/f1WHT}
  \caption{Position of the \TZERO{} detector inside \ALICE{}.}
  \label{fig:t0:detector:f1WHT}
\end{figure}

We have made Monte Carlo simulations of the efficiency and
background conditions for this geometry using the latest version of
\PYTHIA[6.125]. We have included \Cherenkov{} light emission and
light collection processes in the simulation procedure.
\Figref{fig:t0:detector:f2ab} shows the resulting track multiplicity
for charged particles and gammas for simulated \ppCol{} collisions
(light grey). The $y$--axis shows the number of generated \PYTHIA{}
events for the given multiplicity in the range $|\eta| < 10$. If in
the simulated event at least one track produced at least one photon
reaching the photocathode of a PMT in the studied array, we
considered it as registered (hit) and the event was plotted in
black. \TZERO{C} hits are shown in \figref{fig:t0:detector:f2ab}
(top), \TZERO{A} in \figref{fig:t0:detector:f2ab} (bottom), and
coincidence hits from the same event in both arrays are shown in
\figref{fig:t0:detector:f2c}.

\begin{figure}[htbp]
  \centering \includegraphics[width=.6\textwidth]{t0/figures/f2ab}
  \caption{Total track multiplicity distribution of charged and
    neutral particles given by \PYTHIA{6.125}. Light grey: all events,
    black: those registered by \TZERO{C} (top), by the \TZERO{A}
    (bottom).}
  \label{fig:t0:detector:f2ab}
\end{figure}


\begin{figure}[ht]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f2c}
  \caption{Total track multiplicity distribution of charged and
    neutral particles given by \PYTHIA[6.125]. Light gray: all events,
    black: those registered by both arrays in coincidence.}
  \label{fig:t0:detector:f2c}
\end{figure}


One can see that efficiency increases rapidly with the multiplicity
of events. It becomes quite satisfactory already at $M>100$. The
combined geometrical efficiency for all processes included in
\PYTHIA{} and estimated from these distributions is about 64\% for
\TZERO{C}, 59 \% for \TZERO{A}, and 45\% for the coincidence of both
arrays. The actual efficiencies should, in fact, be slightly higher
(67\%, 60\% and 48\%) due to photon conversion into electrons in the
beam pipe and other material between the IP and the \Cherenkov{}
radiators. In heavy--ion collisions, with the exception of extremely
peripheral collisions, the efficiency of \TZERO{} is always 100\%.


The simulations were made assuming that the diameter of the
\Cherenkov{} radiators matches that of the outside diameter of the
PMT (\unit[3]{cm} for FEU--187). By reducing the diameter one
reduces the efficiency correspondingly, but gains slightly in the
time resolution. This is because the active area of the photocathode
is only \unit[20]{mm} in diameter. At the moment \unit[20]{mm} is
the baseline for the radiator diameter (see
\secref{sec:t0:finalbeamtest:quartz}).

Initially two design options were considered for the \TZERO{} array:
a smaller version, with just 12 detectors in each array forming a
single layer of PM tubes wrapped around the beam pipe, and a version
with 24 detectors arranged in 2 layers. Due to the space constraints
on the muon absorber side the smaller version was chosen. In fact
simulations indicate that, unlike on the \RB{24} side, the second
layer on the \RB{26} would not considerably improve efficiency as it
would already be too far away ($|\eta| < 2.9 $). The described
calculations were made for the adopted version ($12+12$).

\subsection{Laser Calibration System (LCS)}

The goal of the LCS is to tune the \TZERO{} electronics and monitor
the performance of the detector before and during the \ALICE{}
experiments. For that reason it is necessary to provide simultaneous
light pulses with adjustable amplitudes for all 24 PMTs of the
\TZERO{} detector. It is highly desirable that the amplitudes of the
laser pulses at the input of each PMT cover the full dynamic range
of the detector and that the wavelength is well within the
sensitivity range of the photocathode. Regrettably, the latter
requirement excludes red lasers, which are widely used for instance
in telecommunication, and therefore have a very broad range of
relatively inexpensive accessories. We have only been able to find
one laser that matched our specifications and price range. It is
Picosecond Injection Laser PIL040G \unit[408]{nm}
(\figref{fig:t0:detector:LaserCal1}). The maximum power of this
laser is close to the limit of that required by our application,
therefore it is essential to minimize signal loss along the optical
wire and couplings. It is also important that the timing
characteristics of the laser pulse should be preserved on the way to
the PMT. Otherwise it would be impossible to tune the arrays to
better than \unit[50]{ps} accuracy.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/LaserCal1}
  \caption{PIL040G 408 nm laser.}
  \label{fig:t0:detector:LaserCal1}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.95\textwidth]{t0/figures/LaserCal2}
  \caption{Current conceptual drawing of LCS.}
  \label{fig:t0:detector:LaserCal2}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.5\textwidth]{t0/figures/LaserCal3}
  \caption{Time resolution measured with laser amplitudes
    corresponding to 1, 10 and \unit[100]{MIP}. The laser pulse was
    delivered over \unit[20]{m} of multi--mode optical fibre. The resolution
    coming from the electronic noise is also plotted.}
  \label{fig:t0:detector:LaserCal3}
\end{figure}

Tests made with the laser and \unit[20]{m} long multi--mode optical
fibre indicate that the LCS concept sketched in
\figref{fig:t0:detector:LaserCal2} is sound and can be used for
\TZERO{} calibration. The achieved time resolution for different
light outputs is plotted in \figref{fig:t0:detector:LaserCal3}. For
the test a manual attenuator was used. In the actual set-up it will
be replaced by a computer controlled attenuator (for instance
Digital Variable Attenuator \texttt{DA--100--3S--830--9/125--M--3})
\cite{t0:ozoptics}. The attenuated signal will then be split into 24
identical pulses and delivered to 24 PMT arrays, about \unit[25]{m}
from the laser. The PMTs will be divided into 2 groups of 12 PMTs
and placed in different locations. Each PMT will be equipped with a
short (\unit[1]{m}) optical fibre. One end of this fibre will have a
standard connector to couple to the \unit[25]{m} fibre coming from
the laser. The other end will be permanently attached to the PMT
assembly in such a way as to illuminate the photocathode of the PMT.
The design of this part is not yet fixed but most probably it will
be just a cut and polished end of the fibre shining directly on the
quartz surface in front of the PMT.

\subsection{PMT Operation Voltages}
\label{sec:t0:detector:voltages}

All the tests have confirmed that FEU--187 can provide very good
time resolution in a wide range of bias voltages and magnetic fields
(\figsref{fig:t0:detector:PMToper1} and
\ref{fig:t0:detector:PMToper2}). Naturally, increasing HV bias
rapidly increases pulse amplitude at the output (by up to 3 orders
of magnitude) and changes the relative pulse delay by several
nanoseconds. Therefore selection of the optimum HV has a big impact
on detector performance and must be made with care.  Even a slight
change of HV bias necessitates retuning of all delays and
thresholds, affecting the efficiency and often also the time
resolution.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/PMToper1}
  \caption{Dependency of time resolution on HV bias (V) as a function
    of external magnetic field strength ($B = 0.3$ and
    $\unit[0.5]{T}$). To demonstrate the consistency of the
    results two measurements at $B = \unit[0]{T}$ are shown.}
  \label{fig:t0:detector:PMToper1}
\end{figure}

Since most of the events in \ppCol{} and \PbPbCol{} collisions will
be minimum bias events, it makes sense to operate in both types of
runs at the same PMT HV. Running at the same voltage is also
beneficial for normalization of the results.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/PMToper2}
  \caption{Relative PMT gain loss in the magnetic field.
    HV bias values were set in such a way that the amplitudes of the
output signals remained constant for 1, 10, and \unit[100]{MIP}.}
  \label{fig:t0:detector:PMToper2}
\end{figure}

While \unit[1]{MIP} performance is very important, the most
interesting heavy--ion events are expected to produce up to
\unit[100]{MIP} signals. This is the main reason why we need signal
processing with a sufficiently wide dynamic range to handle all the
cases between 1 and \unit[100]{MIP}. A PMT can produce a maximum
signal amplitude of about \unit[5]{V}. Assuming the linear
characteristic of a PMT and taking \unit[5]{V} for a
\unit[100]{MIPs} signal one obtains the average amplitude of
\unit[50]{mV} for a \unit[1]{MIP} signal. Due to statistics the
amplitude distribution for a \unit[1]{MIP} particle is very broad,
so to get a reasonable efficiency the electronics threshold has to
be set at about 1/3 of the average amplitude, i.e. at about
\unit[15]{mV}. These values (\unit[15]{mV} and \unit[5]{V})
translate to 1:333 dynamic range. Adding a small safety margin the
required dynamic range for pulse processing is therefore 1:500. We
have shown that it is possible to cope with such dynamic range with
a single CFD unit but as a further precaution we shall also amplify
the PMT pulses with two different amplification coefficients (1 and
20).

The next important consequence of the 1--\unit[100]{MIP} range is
the need to use relatively low HV bias values (about \unit[1000]{V})
to avoid the distortion of large pulses. We have tested to ensure
that even at such low voltages the time resolution remains quite
good at \unit[1]{MIP} and improves with the increase of the light
emitted. Keeping the HV bias low we did not encounter pulse
saturation up to the \unit[100]{MIP} level.

To guarantee longevity of the PMT the average anode current (not to
be confused with HV divider, which current is larger by nearly 3
orders of magnitude) should be kept below\unit[1]{$\mu$A} even if
short bursts of up to \unit[10]{$\mu$A} are acceptable. According to
our estimates this condition will be fulfilled with the proposed
operation voltage giving \unit[50]{mV} pulses for \unit[1]{MIP}.  In
the calculations we have used nominal luminosities with occupancies
and multiplicities generated by \PYTHIA{} and \HIJING{}. If ageing
nevertheless takes place it is known from the manufacturer that it
will lead to a slow decrease of the gain.  This effect is relatively
easy to compensate for by increasing the operating voltage. It is
also known that after turning HV off for a longer period the PMT
partially recovers; its gain factor slowly comes back.

\subsection{Mechanical Support}

A 1:1 mechanical model of \TZERO{C} has been build, tested and fully
integrated into the central and forward detectors mock--up. The
entire support structure (see \figref{fig:t0:detector:Muon_abs}) is
made of aluminium. It will be fixed to the muon absorber prior to
the installation of \VZERO{} and \FMD{}{}. All the services will be
supplied via the dedicated duct
(\figref{fig:t0:detector:Patch_panels}) placed on the 12--hour
position of the absorber (\figref{fig:t0:detector:T0_integration}).
This location was chosen to minimize the cable length to the \TRD{}
``wake-up'' box.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/Muon_abs}
  \caption{Layout of the \TZERO{} support on the muon absorber side.}
  \label{fig:t0:detector:Muon_abs}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.45\textwidth]{t0/figures/T0_integration}
  \caption{Integration test. The plywood structure mocks the
    muon absorber and the tube, the beam pipe.  All cables are fed
    to patchpanels located in the 12 o'clock duct.}
  \label{fig:t0:detector:T0_integration}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.45\textwidth]{t0/figures/Patch_panels}
  \caption{\TZERO{} patch panels inside the duct on the top of the
    muon absorber.}
  \label{fig:t0:detector:Patch_panels}
\end{figure}

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  photocathode MIPs

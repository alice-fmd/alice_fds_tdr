%%
%% $Id: montecarlo.tex,v 1.15 2004-09-09 16:28:48 cholm Exp $
%%
\section{Monte Carlo Simulations}

Adequate description of the response function of a \Cherenkov{}
counter is needed to simulate experimental data and to estimate the
influence of the secondaries scattered by surrounding detectors on
the performance of the \TZERO{} detector. In our first approach we
did not use \ALIROOT{} because our goal was to compare the results
of the simulations with the experimental data obtained in our beam
tests. The simulations were based on \GEANT{4.2} and confirmed with
\GEANT{3}. We have added own program modules (in C++) describing the
conditions of our experiments.

\subsection{Detector Response Function}

We have made simulations of the following beam profiles: narrow,
limited (\unit[$0.8\times0.8$]{cm\textsuperscript{2}}) beam
geometry) and broad beam geometry. In all cases we have assumed
fully random, uniform flux of $\beta = 1$ particles flying parallel
to the symmetry axis of the \Cherenkov{} counter. The simulations
were made for PMT R3432--01 with a quartz radiator \unit[30]{mm} in
diameter, and \unit[30]{mm} long.

In the narrow beam geometry all particles followed exactly the same
path so the light collection efficiency was constant and the PMT
output pulse distribution follows the Poisson distribution of
photoelectrons emitted from the photocathode. The result of this
simulation for particle trajectories on the axis of the counter is
shown in \figref{fig:t0:montecarlo:f10}.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f10}
  \caption{Monte Carlo simulations of the number of photoelectrons emitted
    by particles with $\beta = 1$ traversing the cylindrical quartz
    \Cherenkov{} radiator along its central axis.}
  \label{fig:t0:montecarlo:f10}
\end{figure}

$\langle N\rangle \approx 180$ is the mean number of photoelectrons
emitted from the photocathode in response to the \Cherenkov{} light
produced in \unit[3]{cm} of quartz by a relativistic particle. This
result is in good agreement with the estimate presented in
\secref{sec:t0:detector:cherenkov}. The value $\eta = 0.15$ was
taken as the average of the quantum efficiency of the photocathode
in the \unit[300--550]{nm} wavelength range. The extracted width
parameter $\sigma = 0.076$ is consistent with 0.075 calculated
placing $\langle N\rangle = 180$ to the formula $\sigma = 1 /
\sqrt{\langle N\rangle}$.

Since the photocathode of a fine--mesh PMT covers only about 45\% of
the surface of the entrance window, light collection drops sharply
at the edges, as illustrated in \figref{fig:t0:montecarlo:f11}. This
explains the double peak character of the amplitude distribution
measured in the broad beam geometry (shown in
\figref{fig:t0:testbeam:f9}). \Figref{fig:t0:montecarlo:f12}
demonstrates the results of Monte Carlo simulations based on the
response function of a \Cherenkov{} detector with a cylindrical
quartz radiator to the broad beam profile (approximated by realistic
two--dimensional Gaussian distribution of the density of the
particles in the beam, with $\sigma= \unit[0.85]{cm}$).

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f11}
  \caption{Mean number of photoelectrons $\langle N\rangle$ emitted by
    particles with $\beta$ = 1 traversing the cylindrical quartz radiator
    at different distances from the centre.}
  \label{fig:t0:montecarlo:f11}
\end{figure}

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f12}
  \caption{Monte Carlo simulations of the number of photoelectrons emitted
    by particles with $\beta$ = 1 in the cylindrical quartz radiator and
    ``broad'' beam geometry with ``realistic'' beam density distribution.}
  \label{fig:t0:montecarlo:f12}
\end{figure}

In actual \ALICE{} conditions the amplitude distribution of the
\Cherenkov{} counters' outputs will be somewhat different from those
in \figref{fig:t0:montecarlo:f12} because of the changing position
of the IP. Gamma rays, originating from the IP and subsequently
converting into electron--positron pairs, should also be taken into
account. We have done that by implementing into \ALIROOT{}
subroutines the response function of the \Cherenkov{} counters and
generating events with \PYTHIA[6.125]. The resulting amplitude
distributions for \TZERO{C} (\unit[0.7]{m} from the IP, on the muon
absorber side) and for \TZERO{A} (\unit[3.6]{m} from the vertex on
the \RB{24} side) are given in \figref{fig:t0:montecarlo:f13}
(without and with the background induced by the beam pipe
background).

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.45\textwidth]{t0/figures/f13}
  \includegraphics[width=.45\textwidth]{t0/figures/f14}
  \caption{Amplitude distributions for the \Cherenkov{} counter in
  the \TZERO{C} (top), and the \TZERO{A} (bottom) given by \PYTHIA{}
    version 6.125. On the left hand side background from the
    beam pipe is not taken into account, the right hand side does include
    this background.}
  \label{fig:t0:montecarlo:f13}
  \label{fig:t0:montecarlo:f14}
\end{figure}

\subsection{\TZERO{} Efficiency in \ppCol{} Collisions}

The triggering efficiency of the \TZERO{} detector in heavy--ion
collisions (due to high multiplicities of produced particles) is
nearly 100\% and therefore is of no concern at this point. But in
\ppCol{} collisions the involved multiplicities are much smaller and
the expected efficiency must be carefully simulated, taking into
account not only the response function of the \Cherenkov{} detector
but also all the details of geometry, location and thickness of the
beam pipe, support structures, etc. In carrying out these
simulations we have considered a particle to be registered if the
signal from the PMT was larger than 40 photoelectrons. This
threshold value was based on our actual experimental data. The
background from the interaction of primary particles coming from the
IP with the beam pipe was also taken into account. The results of
these simulations were shown in \secref{sec:t0:detector:arrays}
(\figsref{fig:t0:detector:f2ab} and \ref{fig:t0:detector:f2c}).
\tabref{tab:t0:ppeff} summarizes the calculated efficiencies
extracted from the data for all events generated by \PYTHIA{}.

\begin{table}[htbp]
  \centering
  \caption{Calculated efficiencies of the \TZERO{} detector for \ppCol{}
    collisions}
  \label{tab:t0:ppeff}
  %% table begins
  %%
  \vglue0.2cm
  \begin{tabular}{|p{.30\textwidth}|c|c|c|}%\\
    \hline
    &\textbf{Right array}&\textbf{Left array}&\textbf{Both arrays in
      coincidence} \\
    \hline
    Physical efficiency\par without beam pipe& 62\%& 58\%& 43\%\\ \hline
    Geometrical efficiency\par without beam pipe& 64\%& 59\%& 45\%\\ \hline
    Physical efficiency\par with beam pipe& 67\%& 60\%& 48\%\\ \hline
\end{tabular}
\end{table}
The increase of the physical efficiency in the presence of the beam
pipe is caused mainly by the conversion of gammas into electrons in
the material of the pipe. The efficiencies given in
\tabref{tab:t0:ppeff} are averaged over all multiplicities.
Efficiency at multiplicities larger than 20 is given in
\figref{fig:t0:montecarlo:f16}.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f16}
  \caption{Efficiency of registration of \ppCol{} collisions as function
    of total multiplicity of events.}
  \label{fig:t0:montecarlo:f16}
\end{figure}

At multiplicities $M>150$ the efficiency of the \TZERO{} detector
(coincidence of \TZERO{A} and \TZERO{C}) is already about 90\%. It
is therefore obvious that for ion--ion collisions the efficiency
will be 100\%, excepting the ultra--peripheral collisions.

\subsection{Simulations of Multiplicity Resolution }

The \TZERO{} detector should generate 3 trigger signals
corresponding to the 3 multiplicity levels: minimum--bias,
semi--central, and central ion--ion collisions. Such signals could
be produced, for instance, by analysing the sum of all PMT pulses
with discriminators. Obviously, this procedure will work only in the
case of high multiplicities (i.e. for ion--ion collisions). In
\ppCol{} collisions the average occupancy per \Cherenkov{} counter
is only about 0.3, making multiplicity determination impossible.

To estimate \TZERO{} multiplicity resolution from the sum of PMT
signals we have used a simple step--by--step approach. First we
estimated multi--particle resolution of a single \Cherenkov{}
counter in broad--beam geometry. This can be done in two ways. As
the distribution of the particles is random and uniform across the
surface of the radiator, one can simply sum the number of
photoelectrons for 2 particles, 3 particles, etc. Typical results
for this procedure are given in \figref{fig:t0:montecarlo:f17}. When
the number of particles exceeds 3 the photoelectron distribution
becomes Gaussian.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f17}
  \caption{Monte Carlo simulations of response functions of a \Cherenkov{}
    counter for $n$ particles randomly distributed across the surface of
    the radiator.}
  \label{fig:t0:montecarlo:f17}
\end{figure}

The other way is to calculate directly the dispersion $D = \langle
n^2\rangle - \langle n\rangle^2$. For the distribution given in
\figref{fig:t0:montecarlo:f12} $D = 1936, \sigma_0 = D^{1/2} = 44$
(r.m.s.) and the relative error is $\delta_0 = \sigma_0 / \langle N
\rangle = 0.39$ ($\langle N\rangle = 112$). For $n$ particles
$\delta_n = \delta_0/\sqrt{n}$. The results of these calculations are
given in \figref{fig:t0:montecarlo:f18}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f18}
  \caption{Relative statistical errors for the registration of
    multi--particle events for parallel random charged particles'
    flux.}
  \label{fig:t0:montecarlo:f18}
\end{figure}

The dashed curve is the function $\delta_n$. The rhombs give the
values of deltas obtained from the Gaussian fits of the
distributions similar to \figref{fig:t0:montecarlo:f17}. The solid
curve represents the Poisson fluctuations of the number of
particles' relative statistical error $\delta = 1/\sqrt{n}$. It is
clear that the resolution of a \Cherenkov{} counter for
multi--particle events is approximately two times better than the
statistical error.

\Figref{fig:t0:montecarlo:f13} characterises the \Cherenkov{}
counters' response to the random flux of relativistic particles
parallel to the counter axis. Under actual \ALICE{} conditions the
angular distribution of particles, gamma ray conversion in the
radiator, and the background induced by the beam pipe should also be
taken into account. Since full treatment requires a lot of computer
time we have settled for simplified approach. To simulate
multi--particle resolution of the detector we have used a
parametrized \HIJING{} event generator. The multiplicity of events
was fixed at a certain value, corresponding to the average number of
primary charged particles per \Cherenkov{} counter equal to unity.
In the course of simulations (10 000 events) we have fixed each
amplitude in each \Cherenkov{} counter for each event. The primary
gamma--ray conversion in the radiator, as well as the secondary
particles (electron--positron pairs and $\delta$-electrons), were
also taken into account. The resulting amplitude distribution,
averaged over all counters and events, in a single \Cherenkov{}
counter is shown in \figref{fig:t0:montecarlo:f19}. The relative
error $\delta_0$ of this distribution is equal to 1.04 (compared to
the value 0.39 obtained for the narrow beam geometry hitting the
center of the radiator).

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f19}
  \caption{Amplitude distribution in one PMT from the (\TZERO{C}), averaged over 10 000
    \HIJING{} events. Zero amplitudes are excluded from the
    distribution for reasons of scaling.}
  \label{fig:t0:montecarlo:f19}
\end{figure}

The sum of the signals coming from the 12 PMTs of the \TZERO{C} and
averaged over all events is shown in \figref{fig:t0:montecarlo:f20}.
The shape of this amplitude distribution approaches Gaussian
distribution. The relative error $\delta_0$ of the distribution is
equal to 0.295, practically equal to that of the distribution for a
single PMT divided by $\sqrt{12}$.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f20}
  \caption{Averaged over 10 000 \HIJING{} events, the amplitude distribution of
    the sum of 12 PMTs' amplitudes in the \TZERO{C}.}
  \label{fig:t0:montecarlo:f20}
\end{figure}

\paragraph{Note}
The distributions given in \figsref{fig:t0:montecarlo:f19} and
\ref{fig:t0:montecarlo:f20} include the statistical Poisson
distribution of the primaries coming from IP. This Poisson
distribution for primary charged particles registered in the
\TZERO{C} is shown in \figref{fig:t0:montecarlo:f21}.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f21}
  \caption{Poisson distribution of the number of primary charged particles
    registered by the \TZERO{C} in one \HIJING{} event at fixed multiplicity,
    corresponding to \unit[1] primary charged particle per
    \Cherenkov{} counter.}
  \label{fig:t0:montecarlo:f21}
\end{figure}

The multi--particle resolution of the array when the average number
of primary charged particles per \Cherenkov{} detector is equal to
2, 3, and 4 was obtained by summing the amplitudes for 2, 3 and 4
events.

It is interesting to compare these results with the case when only
statistical Poisson fluctuations of the number of primary charged
particles are taken into account. \Figref{fig:t0:montecarlo:f22} shows
the statistical Poisson fluctuations as a function of the mean number
of particles (solid line).

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f22}
  \caption{Relative statistical errors for the registration of the number of
    primary charged particles by the \TZERO{A}. Solid line -- Poisson
    statistics, rhombs -- detector resolution obtained by Monte Carlo
    simulations.}
  \label{fig:t0:montecarlo:f22}
\end{figure}

The rhombs show the statistical errors of charged particle
multiplicities defined as above. This coincidence may be explained
by taking into account that every \Cherenkov{} counter registers not
only primary charged particles but also some primary gammas (which
contribute about \unit[20]{\%} of the signals) and charged particles
from the beam pipe, the number of which is correlated with the
primary statistics. The results of the simulations show, that the
actual number of charged and neutral particles registered by each
counter is two times larger than the number of primary charged
particles. The same simulations were carried out for the \TZERO{A}
at a fixed multiplicity corresponding to the mean number of primary
charged particles per single \Cherenkov{} counter in the array and
being equal to one. The results of the simulations practically
coincide with those given in \figref{fig:t0:montecarlo:f21} for
\TZERO{C}.

In conclusion one can say that the multi--particle resolution for
primary charged particles of both arrays is determined almost solely
by Poisson statistics (statistical error).



%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

%%
%% $Id: datareadout.tex,v 1.9 2005-06-14 15:43:41 hehi Exp $
%%
%% Tomasz Malkiewicz 12.08.2004
%%

\section{Data Readout}

As mentioned in \charef{cha:t0:fastel}, the only \ALICE{}
sub-detector requiring non-trigger data from \TZERO{} is \TOF{}.
\TOF{} needs time and amplitude information from each PMT to make
off-line corrections that should further improve the precision and
stability in definitions the interaction time. Otherwise, the only
reason for storing raw \TZERO{} parameters would be for monitoring.
Therefore, to cut costs and to guarantee the performance of
\TZERO{}, our readout (see \figref{fig:t0:datareadout:blread}) will
be nearly identical to that of the \TOF{} detector. For conciseness,
only the modifications and changes in the \TOF{} readout will be
presented here. Together with arrival times and amplitudes from each
PMT a handful of other parameters (vertex, summary amplitudes, etc.)
will also be read out and stored by \ALICE{} DAQ in exactly the same
fashion. From the point of view of the readout architecture, the
\TZERO{} detector will be just one more, fully independent, sector
of the \TOF{} detector.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/blread}
  \caption{Block diagram of \TZERO{} readout.}
  \label{fig:t0:datareadout:blread}
\end{figure}

The main difference between \TZERO{} and \TOF{} pulses (relevant to
the readout system) is their dynamic range. To accommodate the
larger amplitude range from \TZERO{} a QTC + TDC will be used
instead of QDC (see \secref{sec:t0:fastelectronics:qtc}). A more
serious problem is the range of the TDC.  The High Performance TDC
(HPTDC) developed by \TOF{} has a range of about \unit[200]{ns},
nearly one order of magnitude more than what is needed by \TZERO{}.
The resolution will be naturally sufficient but there is a problem
with the dead time.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/demul}
  \caption{Block diagram of the demultiplexer.}
  \label{fig:t0:datareadout:demul}
\end{figure}

There is a small (below a few percent) probability that the same
\TZERO{} module will produce a pulse in two consecutive bunch
crossings. In \ppCol{} collisions they are separated by just
\unit[25]{ns}.  The first pulse will start the HPTDC and block it
for the next \unit[200]{ns} preventing the conversion, readout and
storage of the second pulse. Let's further assume that the first
signal was just a noise, a strain particle or a cosmic ray while the
second come from proper interaction that should be triggered and
stored. In this possible but unlikely case the \TZERO{} will produce
all the correct trigger signals but the data (time and amplitude)
from that particular PMT will not be digitised.

There are several ways to handle this problem. The easiest is to
simply ignore such events and require the absence of \TZERO{} data
for at least \unit[200]{ns} prior to the valid interaction. The only
drawback would be a slight (below a few percent) reduction in the
data-taking rate. This is the solution that we prefer and consider
as the baseline, since it has no consequence for \PbPbCol{} running
and very small effect for \ppCol{}.

The second solution is to use the the \TZERO{} Vertex signal as a
strobe for the \TZERO{} readout. In this way no reduction in the
data-taking rate will take place but the information on the \TZERO{}
operation in non--trigger conditions will be absent. Such a solution
would also require additional delays to the time and amplitude
signals in waiting for the production of \TZERO{} Vertex. Such
delays would slightly worsen signal quality.

Finally, it is possible to solve the problem completely by
demultiplexing the signals from each PMT into \unit[16]{TDC} inputs
instead of just one. The prototype of such a demultiplexer has been
built and successfully tested (see
\figref{fig:t0:datareadout:demul}). It may be used as the base for
production modules. The biggest disadvantage of the demultiplexer
solution is a 16 fold increase in readout electronics. For that
reason we consider it only as an upgrade option.

\subsection{Data Readout Module (DRM)}

Each crate will be equipped with a Data Readout Module (DRM) card
that will act as the main interface between the Central \ALICE{}
DAQ, the CTP and \TZERO{} electronics (see
\figref{fig:t0:datareadout:dare}). The DRM will receive and
distribute the \unit[40]{MHz} clock and the trigger signals (\LVL{1}
and \LVL{2}) to the \TZERO{}--TRMs. The clock will be received
through an optical fibre, while the other signals will be derived
from a TTCrx. The clock will be distributed to \TZERO{}--TRM and
fast electronics in LVDS standard. The control signals (\LVL{1},
\LVL{2} accept and \LVL{2} reject) will be distributed with an LVDS
bus to the \TZERO{}--TRMs through an external flat cable.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/dare}
  \caption{\TZERO{} Data Readout Module.}
  \label{fig:t0:datareadout:dare}
\end{figure}

The DRM will read the data from the \TZERO{}--TRM modules. If an
\LVL{2} reject is received the corresponding event buffer will be
cleared on the \TZERO{}--TRMs, otherwise, on \LVL{2} accept, data
will be transferred from all the \TZERO{}--TRMs to the DRM via the
VME64 backplane. This data transfer is performed by the FPGA.

The data will be further processed and encoded by a DSP on board and
sent through a standard \ALICE{} DDL interface to the central DAQ. A
power PC will allow monitoring of the data and will host the slow
controls of the \TZERO{} system (threshold setting, delay setting
etc.). All these I/O devices (TTC and DDL interfaces and power PC
cards) will be developed as a piggy-back card in standard PMC format
applied to a VME card.

\subsection{TDC Readout Module - TRM unit}

An FPGA will perform the readout of the HPTDCs. To ensure high
bandwidth the FPGA will act as an external readout controller of two
separate chains consisting of \unit[15]{HPTDC} slaves (in token -
base parallel-readout configuration). The use of an Actel Pro Asic
Plus FPGA is foreseen. A Digital Signal Processor (DSP) will control
various setup operations (including R-C delay chain calibration) and
data packaging. The use of an Analog Devices Shark family DSP is
foreseen. Memory (RAM and SRAM) is provided for event buffering and
program hosting.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{t0/figures/trm}
  \caption{TRM unit.}
  \label{fig:t0:datareadout:trm}
\end{figure}

Program loading and general control of the TRM will be managed
through a VME interface (see \figref{fig:t0:datareadout:trm}).
Initialization and setup of the HPTDC chips will be normally
performed through the DSP. At reception of an \LVL{1} signal from
the \ALICE{} Central Trigger Processor (CTP) the HPTDCs will look
for hits with a time offset of \unit[6.2]{$\mu$s}, moving then to
the internal readout FIFO. This operation does not cause dead time
to the acquisition of data by the HPTDC.

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

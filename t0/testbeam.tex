%%
%% $Id: testbeam.tex,v 1.13 2004-09-09 16:28:48 cholm Exp $
%%
\section{Initial Beam Tests of Detector Prototypes}
\label{cha:t0:inibeamtest}

In our R\&D studies we have tested different \Cherenkov{} counter
option, varying the types of PMTs and radiators. Scintillation
counters based on the BC--408 scintillator have also been tested
\cite{t0:GRI00a,t0:GRI00b,t0:GRI01}.

\subsection{Experimental Setup}

For the first tests we used the mixed ITEP pion/proton beam and the
experimental setup of the \ALICE{} ITEP group. This setup was also
used in the ITEP studies of the timing properties of RPCs for the
\ALICE{} \TOF{} detector. The schematic diagram of the test beam
setup is shown in \figref{fig:t0:testbeam:f6}.

\begin{figure}[htbp]
 \centering
  \includegraphics[width=.8\textwidth]{t0/figures/f6}
  \caption{Test beam layout.}
  \label{fig:t0:testbeam:f6}
\end{figure}

The test beam facility includes several detectors. S1 and S2 are two
identical scintillation counters working as START. Each consists of
a PMT XP7229 and BC 408 scintillator \unit[$2\times2\times2.5$]{cm}.
The S3 scintillator, located at a flight distance of \unit[10]{m}
from the START, allows the separation of pions from protons ($p =
\unit[1.28]{GeV/c}$) with nearly 100\% efficiency. Scintillators F1
and F2 form a cross, defining the beam profile to
\unit[$0.8\times0.8$]{cm}. There were also additional scintillation
counters S4 and \LVL{1} intended for coincidence operation with
other counters if needed.  The time resolution of each counter was
typically about \unit[50]{ps}. It was continuously monitored during
the run.

The investigated \Cherenkov{} detector (CHD) was about \unit[2]{m}
downstream from S1. The signals from S1, and S2 were fed to a
constant fraction discriminator inputs, whereas the signals from CHD
were fed to a fast leading edge discriminator with a \unit[60]{mV}
threshold. All TDC channels had an identical \unit[50]{ps/channel}
resolution. A 1024 channel QDC was used to measure the amplitude
distributions of the CHD signals.  All measurements were made at
\unit[1.28]{GeV/c} for both pions and protons.

\subsection{Experimental Results}

We have compared the time resolution of scintillation and
\Cherenkov{} detectors using different radiator types and shapes.
The scintillation detector used the same PMT but used BC--408 in
place of a \Cherenkov{} radiator. The studied radiators included:
\begin{enumerate}
\item A cylindrical quartz radiator \unit[26]{mm} in diameter and
  \unit[30]{mm} long.
\item A similar quartz radiator but with a thin Al cover to provide
  mirror reflection with \unit[98]{\%} efficiency.
\item A Lucite (Plexiglas) radiator of rectangular shape
  \unit[$18\times18\times30$]{mm\textsuperscript{3}}.
\end{enumerate}

Among the studied PMTs were Hamamatsu R3432--01 (26 mm diameter)
and a Russian fine--mesh PMT FEU--187 (\unit[30]{mm} in diameter).
We also used a \unit[20]{mm} thick BC--408 scintillator with a
diameter matching that of the PMT (\unit[26]{mm} and \unit[30]{mm}
correspondingly). A fast leading edge discriminator was used in all
runs, and we applied an off--line correction in order to obtain the
final time resolution values of each different type of detector.

All our measurements were made at two geometries of the beam: a
limited rectangular cross--section beam
\unit[$0.8\times0.8$]{cm\textsuperscript{2}} defined by the F1 and
F2 counters, and a broad beam illuminating the whole detector. The
amplitude distributions of the PMT output signals differed
significantly for these two geometries. In the first case all
radiator types produced a single--peak Gaussian distribution. In the
second case the amplitude distribution consisted of two peaks
(\figref{fig:t0:testbeam:f9}).

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{t0/figures/f9}
  \caption{Amplitude distribution of PMT output signals for ``broad''
    beam geometry for pions \unit[1.28]{GeV/c} (cylindrical quartz
    \Cherenkov{} radiator).}
  \label{fig:t0:testbeam:f9}
\end{figure}

The right--hand part (high--amplitude) of the double peak coincides
roughly with the position of the single peak registered with the
limited beam. The left--hand bump is at half the amplitude value. A
similar amplitude distribution for the broad beam geometry was
obtained by the Hiroshima group in the PHENIX experiment
\cite{t0:IKE98}.  Monte Carlo simulations explain this doubling
effect quite well, as will be shown in the next section.

A summary of our results is collected in \tabref{tab:t0:timeres} and
\tabref{tab:t0:timeres2}. It is clear that \Cherenkov{} counters
give better performance than the BC--408 scintillator. As far as
time resolution is concerned both Lucite and quartz radiators are
acceptable. The advantage of a Lucite radiator is a smaller
radiation length. The radiation length of Lucite is $X0 =
\unit[34.4]{cm}$, whereas for quartz $X0 = \unit[11.7]{cm}$. A
\unit[3]{cm} quartz radiator makes about $0.25 X0$ as compared to
$0.1 X0$ for \unit[3]{cm} of Lucite. However, the radiation
stability of Lucite is only \unit[100]{krad}, which is substantially
less than the expected cumulative dose for the \TZERO{} detector.
This was the main reason for choosing quartz radiators.

\begin{table}[htbp]
  \centering
  \caption{Typical time resolution values obtained with
    different PMTs, radiators and scintillators.}
  \label{tab:t0:timeres}
  %% table begins
  %%
  \vglue0.2cm
  \begin{tabular}{|ll|l|r|}%\\
    \hline
    \multicolumn{2}{|l|}{\textbf{Beam geometry}}&
    \textbf{Radiator/scintillator}&
    \textbf{Time resolution}\\
    & PMT &&\\
    \hline
    &R3432--01& Quartz diam \unit[$26\times30$]{mm}& \unit[40]{ps}\\
    \hline
    & FEU--187& Quartz diam \unit[$26\times30$]{mm}& \unit[42]{ps}\\ \hline

    & R3432--01& BC--408, diam \unit[$26\times20$]{mm} (with a diffuse
    reflection)& \unit[57]{ps}\\
    \hline

    & FEU--187& BC--408, diam \unit[$30\times20$]{mm} (with a diffuse
    reflection)& \unit[55]{ps}\\
    \hline

    & R3432--01& Aluminized quartz diam \unit[$26\times30$]{mm}&
    \unit[48]{ps}\\ \hline

    \multicolumn{2}{|l|}{Broad--beam geometry}&&\\
    \hline
    & R3432--01& Quartz diam \unit[$26\times30$]{mm}& \unit[55]{ps}\\ \hline &
    FEU--187& Quartz diam \unit[$26\times30$]{mm}& \unit[57]{ps}\\ \hline

    & R3432--01& BC--408, diam \unit[$26\times20$]{mm} (with a diffuse reflection)& \unit[80]{ps}\\
    \hline

    & FEU--187& BC--408, diam \unit[$30\times20$]{mm} (with a diffuse
    reflection)& \unit[89]{ps}\\
    \hline
    &

    R3432--01&

    Aluminized quartz

    diam. \unit[$26\times30$]{mm}&

    \unit[54]{ps}\\ \hline
    &
    R3432--01&
    Plexiglas radiator \unit[$18\times18\times30$]{mm}&
    \unit[45]{ps}\\
    \hline
  \end{tabular}
\end{table}

\begin{table}[htbp]
  \centering
  \caption{Results from ITEP accelerator, pions 1.28 GeV/c, March 2,
    2002. Broad--beam geometry. Both Lucite and quartz radiators were
    \unit[30]{mm} long and their diameter was matched to that of the PMT (26
    mm for Hamamatsu and \unit[30]{mm} for FEU--187)}
  \label{tab:t0:timeres2}
  \vglue0.2cm
  \begin{tabular}{|c|c|c|c|}
    \hline
    \textbf{Run No.}&
    \textbf{Type of PMT} &
    \textbf{Type of radiator}&
    \textbf{Time resolution, ps}\\
    \hline
    3&  Hamamatsu R3432--01 & Quartz &  53\\ \hline
    3&  Hamamatsu R3432--01 & Lucite &  50\\ \hline
    7&  Hamamatsu R3432--01 & Lucite &  54\\ \hline
    8&  Hamamatsu R3432--01 & Lucite &  56\\ \hline
    9&  Hamamatsu R5506     & Quartz &  59\\ \hline
    10& Hamamatsu R5506     & Lucite &  75\\ \hline
    11& FEU--187            & Quartz &  55\\ \hline
    12& FEU--187            & Quartz &  58\\ \hline
    13& FEU--187            & Quartz &  52\\ \hline
    14& FEU--187            & Quartz &  42\\ \hline
  \end{tabular}
\end{table}

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

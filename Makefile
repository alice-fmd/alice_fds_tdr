#
# $Id: Makefile,v 1.114 2006-08-27 16:31:28 hehi Exp $
#
NAME		:= alice_tdr_011
VERSION		:= 1.6
REMOTEHOST	:= lxplus.cern.ch
REMOTEDIR	:= /afs/cern.ch/alice/tdr/fmdv0t0/web/
# REMOTEDIR	:= /afs/cern.ch/user/c/cholm/public_html/alice/tdr

REDIRECT        = > /dev/null 2>&1
CHAPDEF		:= \def\chapconfig{$(CHAP)}

TEXS		:= $(NAME).tex				\
		   alicetdr.cls				\
		   author.tex				\
		   bibnames.sty				\
		   bibunits.sty				\
		   fds-title.tex			\
		   fds.sty				\
		   tdr-defs.tex				\
		   colour/colour.tex			\
		   fmd/bib.tex				\
		   fmd/commissioning.tex		\
		   fmd/designconsiderations.tex		\
		   fmd/electronics.tex			\
		   fmd/fmd.tex				\
		   fmd/integration.tex			\
		   fmd/organisation.tex			\
		   fmd/physics.tex			\
		   fmd/safety.tex			\
		   fmd/sirings.tex			\
		   fmd/survey.tex			\
		   fmd/time.tex				\
		   integration/integration.tex		\
		   introduction/bib.tex			\
		   introduction/introduction.tex	\
		   organization/organization.tex	\
		   t0/bib.tex				\
		   t0/datareadout.tex			\
		   t0/designconsiderations.tex		\
		   t0/detconsys.tex			\
		   t0/detector.tex			\
		   t0/fastelectronics.tex		\
		   t0/finalbeamtest.tex			\
		   t0/laser.tex				\
		   t0/montecarlo.tex			\
		   t0/naming.tex			\
		   t0/organization.tex			\
		   t0/physics.tex			\
		   t0/pmt.tex				\
		   t0/pmtoper.tex			\
		   t0/t0.tex				\
		   t0/testbeam.tex			\
		   v0/bib.tex				\
		   v0/arrays.tex	   		\
		   v0/bib.tex		   		\
		   v0/commissioning.tex		   	\
		   v0/fee.tex		   		\
		   v0/generaldesign.tex		   	\
		   v0/lightpulse.tex		   	\
		   v0/objectives.tex		   	\
		   v0/organization.tex		   	\
		   v0/performance.tex		   	\
		   v0/timetable.tex		   	\
		   v0/v0.tex		   		\
		   version.tex

SCRIPTS		:= fmd/figures/accc1.C \
		   fmd/figures/accc2.C \
		   fmd/figures/ampl.C \
		   fmd/figures/coeff.C \
		   fmd/figures/contrib.C \
		   fmd/figures/dependence_on_v2.C \
		   fmd/figures/deprim.C \
		   fmd/figures/eta.C \
		   fmd/figures/ev1.C \
		   fmd/figures/flat.C \
		   fmd/figures/genrec.C \
		   fmd/figures/genrel.C \
		   fmd/figures/meanMultSigma.C \
		   fmd/figures/npart.C

FIGS		:= fmd/figures/RapidityAcceptance.fig \
		   fmd/figures/digitizer.fig \
		   fmd/figures/electronics.fig \
		   fmd/figures/numbering.fig \
		   fmd/figures/rcu.fig \
		   fmd/figures/si_det_3d.fig \
		   fmd/figures/va1_prime2-arch.fig \
		   fmd/figures/va1_prime2-time.fig \
		   introduction/figures/naming.fig

EPSS		:= cover.eps \
		   colour/figures/alice_fwd.eps \
		   fmd/figures/AliceFMD1.eps \
		   fmd/figures/AliceFMD2.eps \
		   fmd/figures/AliceFMD3new.eps \
		   fmd/figures/Cone_eng.eps \
		   fmd/figures/ITS_MockUp_3.eps \
		   fmd/figures/InnerOuterRings.eps \
		   fmd/figures/ModelCone.eps \
		   fmd/figures/RCUphoto.eps \
		   fmd/figures/ReadOutTest.eps \
		   fmd/figures/RingAssembly.eps \
		   fmd/figures/RingAssemblyColour.eps \
		   fmd/figures/Si-geometry-134inner.eps \
		   fmd/figures/Si-geometry-134outer.eps \
		   fmd/figures/Sistriplayout.eps \
		   fmd/figures/Sistriplayout2.eps \
		   fmd/figures/VA_chip.eps \
		   fmd/figures/altro_test.eps \
		   fmd/figures/bonding_3d.eps \
		   fmd/figures/bonding_section.eps \
		   fmd/figures/dcs.eps \
		   fmd/figures/displ_cyl_0.eps \
		   fmd/figures/displ_tot_back.eps \
		   fmd/figures/fmd3d.eps \
		   fmd/figures/hybrids.eps \
		   fmd/figures/install1.eps \
		   fmd/figures/install2.eps \
		   fmd/figures/install3.eps \
		   fmd/figures/install4.eps \
		   fmd/figures/install5.eps \
		   fmd/figures/install6.eps \
		   fmd/figures/install7.eps \
		   fmd/figures/install8.eps \
		   fmd/figures/racks.eps \
		   fmd/figures/va_noise.eps \
		   integration/figures/integration_1.eps \
		   integration/figures/integration_2.eps \
		   integration/figures/integration_3.eps \
		   integration/figures/integration_4.eps \
		   integration/figures/integration_5.eps \
		   introduction/figures/FWD-3d.eps \
		   introduction/figures/FWD-3d-bw.eps \
		   introduction/figures/fwd-inner.eps \
		   introduction/figures/fwd-outer.eps \
		   introduction/figures/fwd-outernewz.eps \
		   introduction/figures/fwd-overview.eps \
		   t0/figures/LaserCal1.eps \
		   t0/figures/LaserCal2.eps \
		   t0/figures/LaserCal3.eps \
		   t0/figures/Muon_abs.eps \
		   t0/figures/PMToper1.eps \
		   t0/figures/PMToper2.eps \
		   t0/figures/PMToper3.eps \
		   t0/figures/Patch_panels.eps \
		   t0/figures/T0_integration.eps \
		   t0/figures/T0photo2.eps \
		   t0/figures/blread.eps \
		   t0/figures/cfd.eps \
		   t0/figures/dare.eps \
		   t0/figures/dcs.eps \
		   t0/figures/delay.eps \
		   t0/figures/demul.eps \
		   t0/figures/detT0.eps \
		   t0/figures/detconf.eps \
		   t0/figures/diagram.eps \
		   t0/figures/f10.eps \
		   t0/figures/f11.eps \
		   t0/figures/f12.eps \
		   t0/figures/f13.eps \
		   t0/figures/f14.eps \
		   t0/figures/f16.eps \
		   t0/figures/f17.eps \
		   t0/figures/f18.eps \
		   t0/figures/f19.eps \
		   t0/figures/f1WHT.eps \
		   t0/figures/f20.eps \
		   t0/figures/f21.eps \
		   t0/figures/f22.eps \
		   t0/figures/f2ab.eps \
		   t0/figures/f2c.eps \
		   t0/figures/f3.eps \
		   t0/figures/f4.eps \
		   t0/figures/f5.eps \
		   t0/figures/f6.eps \
		   t0/figures/f9.eps \
		   t0/figures/feu187.eps \
		   t0/figures/magnet.eps \
		   t0/figures/multiplicity.eps \
		   t0/figures/outpmt.eps \
		   t0/figures/perftimemean.eps \
		   t0/figures/qtc.eps \
		   t0/figures/rack.eps \
		   t0/figures/resp20.eps \
		   t0/figures/resp26.eps \
		   t0/figures/shoebox00.eps \
		   t0/figures/shoeboxes.eps \
		   t0/figures/shoeprot.eps \
		   t0/figures/thresh.eps \
		   t0/figures/timemeaner.eps \
		   t0/figures/tofspec.eps \
		   t0/figures/trm.eps \
		   t0/figures/tvdc.eps \
		   t0/figures/vertex.eps \
		   t0/figures/wave.eps \
		   t0/figures/wvlenght.eps \
		   v0/figures/DCS.eps \
		   v0/figures/MBBGtriggers.eps \
		   v0/figures/MIP.eps \
		   v0/figures/SB.eps \
		   v0/figures/SNvsSPbPb32channels.eps \
		   v0/figures/V0Aconnector1.eps \
		   v0/figures/V0Aconnector2.eps \
		   v0/figures/V0Akeyhole.eps \
		   v0/figures/V0Cbox1.eps \
		   v0/figures/V0Cbox2.eps \
		   v0/figures/V0Cconnector.eps \
		   v0/figures/V0Ccuts.eps \
		   v0/figures/V0Cdesign.eps \
		   v0/figures/V0Cmontage.eps \
		   v0/figures/V0Cphoto1.eps \
		   v0/figures/V0Cphoto2.eps \
		   v0/figures/V0Cphoto3.eps \
		   v0/figures/V0Cringtested.eps \
		   v0/figures/V0Csector.eps \
		   v0/figures/V0CtimeHV.eps \
		   v0/figures/V0Ctimedistribution.eps \
		   v0/figures/V0Ctimelight.eps \
		   v0/figures/V0beampipe.eps \
		   v0/figures/V0design.eps \
		   v0/figures/V0effcell.eps \
		   v0/figures/V0ringmult.eps \
		   v0/figures/V0segmentation.eps \
		   v0/figures/V0vertex.eps \
		   v0/figures/centralitytrigger.eps \
		   v0/figures/chainedintegration.eps \
		   v0/figures/dNdEtaHijing.eps \
		   v0/figures/dataacquisitionscheme.eps \
		   v0/figures/effV0200432channels.eps \
		   v0/figures/efficiency.eps \
		   v0/figures/gain.eps \
		   v0/figures/pgasdiff2.eps \
		   v0/figures/ppmultiplicity.eps \
		   v0/figures/pppgasmult.eps \
		   v0/figures/rackVME.eps \
		   v0/figures/systemtiming.eps \
		   v0/figures/time.eps \
		   v0/figures/timealignmentcondition.eps \
		   v0/figures/timealignmentforBGetBB.eps \
		   v0/figures/timebetweenanalogandclk.eps \
		   v0/figures/timemeasurement.eps \
		   v0/figures/timeresolutionmex.eps

PDFS		:= 

EXTRA_DIST	:= fmd/figures/various.C Makefile update_figures README

GEN_TEXS	:= version.tex
ALL_TEXS	:= $(TEXS) $(GEN_TEXS)

GEN_EPSS	:= $(SCRIPTS:%.C=%.eps) $(FIGS:%.fig=%.eps)
ALL_EPSS	:= $(EPSS) $(GEN_EPSS)

GEN_PDFS	:= $(EPSS:%.eps=%.pdf) $(SCRIPTS:%.C=%.pdf) $(FIGS:%.fig=%.pdf)
ALL_PDFS	:= $(PDFS) $(GEN_PDFS)

DSOURCES	:= $(TEXS) $(SCRIPTS) $(FIGS) $(EPSS) $(PDFS) \
		   $(EXTRA_DIST)
SOURCES		:= $(DSOURCES) $(GEN_TEXS)



LATEX		= latex
DVIPS		= dvips
PDFLATEX	= pdflatex
EPSTOPDF	= epstopdf
ROOT		= root
ROOTFLAGS	= -l -b -q 
FIG2DEV		= fig2dev
REFWARN         = "LaTeX Warning: There were undefined references."
LBLWARN         = "LaTeX Warning: Label(s) may have changed."
MULWARN		= "LaTeX Warning: There were multiply-defined labels."
LATEXFLAGS      = -interaction=nonstopmode
PDFLATEXFLAGS   = -interaction=nonstopmode

%.eps:%.C
	$(ROOT) $(ROOTFLAGS) $< $(REDIRECT)
	if test -f `basename $@` ; then mv -f `basename $@` $@ ; fi

%.eps:%.fig
	$(FIG2DEV) -L eps $< > $@

%.pdf:%.fig
	$(FIG2DEV) -L pdf $< > $@

%.dvi %.aux:%.tex 
	@for i in 1 2 3 4 ; do \
	  echo -n "Running $(LATEX) on $< -> $@, pass $$i ... " ; \
	  $(LATEX) $(LATEXFLAGS) '$(CHAPDEF)\relax\input $*' $(REDIRECT) ; \
	  ret=$$? ; \
	  if test $$ret -gt 0 ; then rm $@ ; echo "There were errors" ; \
	      else echo "OK" ; fi ; \
	  if `grep -q $(LBLWARN) $*.log` || \
	     `grep -q $(MULWARN) $*.log` || \
	     `grep -q $(REFWARN) $*.log` ; then \
	    grep "Warning" $*.log 2>&1 ;  else break ; fi ; done ; \
	if test $$ret -gt 0 ; then rm $@ ; echo "There were errors" ; \
	   false ; fi 


%.ps :%.dvi  %.aux 
	$(DVIPS) $< -o $(REDIRECT)

%-2.ps:%.ps
	psnup -2 -pa4 $< $@ 

%.pdf:%.tex  $(filter-out $(NAME).tex, $(SOURCES)) $(ALL_PDFS) 
	@if test -f $*.aux && `grep -q "hyper@anchor" $*.aux` ; then : ; \
	  else rm -f $*.aux ; fi 
	@for i in 1 2 3 4 ; do \
	  echo -n "Running $(PDFLATEX) on $< -> $@, pass $$i ... " ; \
	  $(PDFLATEX) $(PDFLATEXFLAGS) '$(CHAPDEF)\relax\input $*'$(REDIRECT);\
	  ret=$$? ; \
	  if test $$ret -gt 0 ; then rm $@ ; echo "There were errors" ; \
	      else echo "OK" ; fi ; \
	  if `grep -q $(LBLWARN) $*.log` || \
	     `grep -q $(MULWARN) $*.log` || \
	     `grep -q $(REFWARN) $*.log` ; then \
	    grep "Warning" $*.log 2>&1 ;  else break ; fi ; done ; \
	if test $$ret -gt 0 ; then rm $@ ; echo "There were errors" ; \
	   false ; fi 

%.pdf:%.eps
	$(EPSTOPDF) $< 

%-$(VERSION).ps:%.ps
	cp $< $@ 
%-$(VERSION).pdf:%.pdf
	cp $< $@ 

%.ps.gz:%.ps 
	gzip $< 

%.pdf.gz:%.pdf 
	gzip $< 

%-$(VERSION)-$(CHAP).ps:
	rm -f $*.dvi
	$(MAKE) $*.ps 
	mv $*.ps $*-$(VERSION)-$(CHAP).ps 


%-$(VERSION)-$(CHAP).pdf:
	rm -f $*.pdf
	$(MAKE) $*.pdf
	mv $*.pdf $*-$(VERSION)-$(CHAP).pdf 

all: 			check ps

dvi:			check $(NAME).dvi
pdf:			check $(NAME).pdf
ps:			check $(NAME).ps 
epss eps_figures:	check $(ALL_EPSS)
pdfs pdf_figures:	check $(ALL_PDFS)
versioned:		check $(NAME)-$(VERSION).ps.gz $(NAME)-$(VERSION).pdf.gz

psparts:	$(NAME)-$(VERSION).ps
	$(MAKE) CHAP=front $(NAME)-$(VERSION)-front.ps 
	$(MAKE) CHAP=chap1 $(NAME)-$(VERSION)-chap1.ps 
	$(MAKE) CHAP=chap2 $(NAME)-$(VERSION)-chap2.ps 
	$(MAKE) CHAP=chap3 $(NAME)-$(VERSION)-chap3.ps 
	$(MAKE) CHAP=chap4 $(NAME)-$(VERSION)-chap4.ps 
	$(MAKE) CHAP=chap5 $(NAME)-$(VERSION)-chap5.ps 
	$(MAKE) CHAP=chap6 $(NAME)-$(VERSION)-chap6.ps 

pdfparts:	$(NAME)-$(VERSION).pdf
	$(MAKE) CHAP=front $(NAME)-$(VERSION)-front.pdf 
	$(MAKE) CHAP=chap1 $(NAME)-$(VERSION)-chap1.pdf 
	$(MAKE) CHAP=chap2 $(NAME)-$(VERSION)-chap2.pdf 
	$(MAKE) CHAP=chap3 $(NAME)-$(VERSION)-chap3.pdf 
	$(MAKE) CHAP=chap4 $(NAME)-$(VERSION)-chap4.pdf 
	$(MAKE) CHAP=chap5 $(NAME)-$(VERSION)-chap5.pdf 
	$(MAKE) CHAP=chap6 $(NAME)-$(VERSION)-chap6.pdf 

pdfs.tar.gz:	$(ALL_PDFS)
	tar -czvf $@ $^

epss.tar.gz:	$(ALL_EPSS)
	tar -czvf $@ $^



clean:
	rm -f *~ */*~ *.log *.toc *.lof *.lot *.fxm *.brf *.out semantic.cache
	find . -name "*~" | xargs rm -f 
	rm -f $(NAME)-$(VERSION)*.ps{,.gz} $(NAME)-$(VERSION)*.pdf{,.gz}

distclean realclean:clean
	rm -f *.aux */*.aux *.dvi *.ps *.pdf $(GEN_TEXS) \
		TAGS $(GEN_PDFS) $(GEN_EPSS) *.gz

$(NAME).dvi $(NAME).aux:$(SOURCES) $(ALL_EPSS)

show:
	@echo "NAME:		$(NAME)"
	@echo "VERSION:		$(VERSION)"
	@echo "LATEX:		$(LATEX)"
	@echo "PDFLATEX:	$(PDFLATEX)"
	@echo "DVIPS:		$(DVIPS)"
	@echo "SOURCES:		$(SOURCES)"
	@echo "FIGURES:		$(FIGURES)"
	@echo "EPSS:		$(EPSS)"
	@echo "PDFS:		$(PDFS)"
	@echo "SCRIPTS:		$(SCRIPTS)"
	@echo "ALL_EPSS:	$(ALL_EPSS)"
	@echo "ALL_PDFS:	$(ALL_PDFS)"

dist:	$(NAME)-$(VERSION).tar.gz
$(NAME)-$(VERSION).tar.gz:$(DSOURCES)
	rm -rf $(NAME)-$(VERSION)
	mkdir -p $(NAME)-$(VERSION)
	for i in $(DSOURCES) ; do d=`dirname $$i` ; \
		if test ! -d  $(NAME)-$(VERSION)/$$d ; then \
			mkdir -vp $(NAME)-$(VERSION)/$$d  ; fi ; \
		cp -va $$i $(NAME)-$(VERSION)/$$d/ ; done 
	tar -chof - $(NAME)-$(VERSION)/ | gzip -c > $(NAME)-$(VERSION).tar.gz
	rm -rf $(NAME)-$(VERSION)

distcheck: dist
	gzip -dc $(NAME)-$(VERSION).tar.gz | tar -xf - 
	$(MAKE) -C $(NAME)-$(VERSION)
	rm -rf $(NAME)-$(VERSION)
	@echo "====================================================="
	@echo "   $(NAME)-$(VERSION) is ready for distribution"
	@echo "====================================================="

figure_sizes: 
	@du -hsc $(EPSS) $(SCRIPTS) $(PDFS) $(FIGS)

version.tex:Makefile
	@echo -n "Creating $@ ... "
	@echo "\\def\\TdrVersion{$(VERSION)}" > $@
	@echo "done"

config.tex:
	@echo -n "Creating $@ ... "
	@rm -rf $@ 
ifneq ($(CHAP),)
	@echo "\\input config-$(CHAP).tex" > $@
endif
	@echo "\\relax" >> $@
	@echo "done"
	@cat config.tex


ChangeLog:
	rcs2log > ChangeLog 

web/internal.html:web/internal.html.in 
	sed -e 's/@NAME@/$(NAME)/g'		\
	    -e 's/@VERSION@/$(VERSION)/g'	\
	    < $< > $@ 


upload:	$(NAME)-$(VERSION).ps.gz 	\
	$(NAME)-$(VERSION).pdf.gz 	\
	$(NAME)-$(VERSION).tar.gz	\
	web/internal.html		\
	web/lhcc.html			\
	web/index.html
	scp $^ $(REMOTEHOST):$(REMOTEDIR) 

upload_parts: 
	rm -f $(NAME).ps 
	$(MAKE) psparts 
	rm -f $(NAME).pdf 
	$(MAKE) pdfparts 
	scp $(NAME)-$(VERSION)-front*.pdf.gz $(NAME)-$(VERSION)-front*.ps.gz \
	  $(NAME)-$(VERSION)-chap*.pdf.gz $(NAME)-$(VERSION)-chap*.ps.gz \
	  $(REMOTEHOST):$(REMOTEDIR) 

check:
	@which $(ROOT)      $(REDIRECT) && : || echo "$(ROOT) missing"
	@which $(EPSTOPDF)  $(REDIRECT) && : || echo "$(EPSTOPDF) missing"
	@which $(FIG2DEV)   $(REDIRECT) && : || echo "$(FIG2DEV) missing"


.PRECIOUS:	$(GEN_EPSS) $(GEN_PDFS)
# 
# EOF
#

void npart() { 
  //
  gROOT->LoadMacro("fmd/figures/various.C");   
  setpawstyle(); 
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);//ROOT version3.05/02
  TCanvas *c2 = new TCanvas("c2", "FMD reconstructed particles",
			    469,13,598,596);
  gStyle->SetOptStat(0);
  c2->SetRightMargin(0.05);
  c2->SetTopMargin(0.05);  
  c2->SetLeftMargin(0.15);
  c2->SetBottomMargin(0.15);  
  c2->Range(-3.75,-3.75,33.75,33.75);
  c2->SetBorderSize(2);
  c2->SetFrameFillColor(0);
  
  TH1 *hr = new TH2F("hr","FMD reconstructed particles",31,0,30,31,0,30);
  hr->SetStats(0);
  hr->GetXaxis()->SetTitle("Number of simulated particles");
  hr->GetXaxis()->SetTitleSize(.06);
  hr->GetYaxis()->SetTitleSize(.06);
  hr->GetYaxis()->SetLabelSize(.06);
  hr->GetXaxis()->SetLabelSize(.06);
  hr->GetXaxis()->SetTitleColor(1);
  hr->GetYaxis()->SetTitleFont(132);
  hr->GetYaxis()->SetTitle("Number of reconstructed particles");
  hr->Draw("");
  
  TGraphErrors *gre = new TGraphErrors(25);
  gre->SetName("Graph");
  gre->SetTitle("Graph");
  gre->SetFillColor(1);
  // gre->SetMarkerColor(4);
  gre->SetMarkerStyle(25);
  gre->SetPoint(0,1,0.908562);
  gre->SetPointError(0,0,1.3665);
  gre->SetPoint(1,2,1.69512);
  gre->SetPointError(1,0,2.13565);
  gre->SetPoint(2,3,2.6115);
  gre->SetPointError(2,0,2.65586);
  gre->SetPoint(3,4,3.78073);
  gre->SetPointError(3,0,2.91033);
  gre->SetPoint(4,5,4.84329);
  gre->SetPointError(4,0,3.43269);
  gre->SetPoint(5,6,5.72321);
  gre->SetPointError(5,0,3.68937);
  gre->SetPoint(6,7,6.83715);
  gre->SetPointError(6,0,4.07048);
  gre->SetPoint(7,8,7.85192);
  gre->SetPointError(7,0,4.27369);
  gre->SetPoint(8,9,8.73358);
  gre->SetPointError(8,0,4.38984);
  gre->SetPoint(9,10,9.76146);
  gre->SetPointError(9,0,4.53708);
  gre->SetPoint(10,11,10.8774);
  gre->SetPointError(10,0,4.80462);
  gre->SetPoint(11,12,11.8581);
  gre->SetPointError(11,0,4.92501);
  gre->SetPoint(12,13,12.7628);
  gre->SetPointError(12,0,4.96742);
  gre->SetPoint(13,14,14.2605);
  gre->SetPointError(13,0,4.97197);
  gre->SetPoint(14,15,15.7719);
  gre->SetPointError(14,0,4.88577);
  gre->SetPoint(15,16,16.546);
  gre->SetPointError(15,0,4.89556);
  gre->SetPoint(16,17,17.5228);
  gre->SetPointError(16,0,4.85551);
  gre->SetPoint(17,18,18.5568);
  gre->SetPointError(17,0,4.73663);
  gre->SetPoint(18,19,19.5791);
  gre->SetPointError(18,0,4.67762);
  gre->SetPoint(19,20,20.4072);
  gre->SetPointError(19,0,4.5505);
  gre->SetPoint(20,21,21.3443);
  gre->SetPointError(20,0,4.41045);
  gre->SetPoint(21,22,22.258);
  gre->SetPointError(21,0,4.22139);
  gre->SetPoint(22,23,22.9394);
  gre->SetPointError(22,0,4.06512);
  gre->SetPoint(23,24,23.8536);
  gre->SetPointError(23,0,3.82025);
  gre->SetPoint(24,25,24.4634);
  gre->SetPointError(24,0,3.54145);
   
  TH1 *Graph12 = new TH1F("Graph12","Graph",100,0,27.4);
  Graph12->SetMinimum(-3.10422);
  Graph12->SetMaximum(30);
  Graph12->SetDirectory(0);
  Graph12->SetStats(0);
  gre->SetHistogram(Graph12);
  gre->Draw("p");

  TF1* line = new TF1("pol1", "x", 0, 26);
  line->SetLineWidth(2);
  line->Draw("same");
  
  c2->Modified();
  c2->cd();
  c2->Print("npart.eps");
}

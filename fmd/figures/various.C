//
// $Id: various.C,v 1.1 2004-03-29 15:03:23 cholm Exp $
// 
// Copyright (c) 2003 Christian Holm Christensen.
//
// Permission is granted to copy, distribute and/or modify this
// document under the terms of the GNU Free Documentation License,
// Version 1.1 or any later version published by the Free Software
// Foundation; with the all section being Invariant Sections, with the
// Front-Cover Texts being "Preface", and with no Back-Cover Texts. A
// copy of the license is included in the section entitled "GNU Free
// Documentation License". 
//
//____________________________________________________________________
void setmystyle() 
{
  // 
  TStyle* myStyle = new TStyle("myStyle", "My Style");
  myStyle->SetPadColor(0);
  myStyle->SetCanvasColor(0);
  myStyle->SetCanvasBorderMode(0);
  // myStyle->SetTextFont(52);
  // myStyle->SetTextSize(.5);
  // myStyle->SetOptStat(0);
  myStyle->SetOptTitle(0);
  myStyle->SetFrameFillColor(0);
  myStyle->SetFrameBorderMode(0);

  gStyle = myStyle;
}

//____________________________________________________________________
void setpawstyle() 
{
  if (gROOT->GetStyle("PAW")) {
    gROOT->SetStyle("PAW");
    return;
  }
    
  TStyle* pawStyle = new TStyle("PAW", "PAW like style");

  pawStyle->SetTextFont(132);
  
  pawStyle->SetPadColor(0);
  pawStyle->SetPadBorderSize(1);
  pawStyle->SetPadTopMargin(.05);
  // pawStyle->SetPadRightMargin(.05);

  pawStyle->SetCanvasColor(0);
  pawStyle->SetCanvasBorderMode(0);
  pawStyle->SetCanvasBorderSize(1);

  pawStyle->SetFrameFillColor(0);
  pawStyle->SetFrameBorderMode(0);
  pawStyle->SetFrameBorderSize(1);

  pawStyle->SetOptStat(1001100);
  pawStyle->SetStatBorderSize(1);
  pawStyle->SetStatX(.90);
  pawStyle->SetStatY(.95);
  pawStyle->SetTitleColor(0);

  pawStyle->SetOptTitle(0);
  pawStyle->SetTitleBorderSize(0);
  pawStyle->SetTitleX(.1);
  pawStyle->SetTitleY(1);
  pawStyle->SetStatColor(0);

  pawStyle->SetOptFit(111);

  pawStyle->SetLabelFont(132, "x");
  pawStyle->SetLabelFont(132, "y");
  pawStyle->SetLabelFont(132, "z");
  pawStyle->SetStatFont(132);
  pawStyle->SetTitleFont(132);
  // pawStyle->SetTitleOffset(1.3, "x");
  // pawStyle->SetTitleOffset(1.5, "y");
  // pawStyle->SetTitleOffset(1.3, "z");
  // pawStyle->SetTitleOffset(1.3, "z");

  pawStyle->SetPalette(1);
  pawStyle->SetNdivisions(108, "x");
  pawStyle->SetNdivisions(108, "y");
  pawStyle->SetNdivisions(108, "z");
  
  gROOT->SetStyle("PAW");
}

//____________________________________________________________________
void setgraypalette() 
{
  Int_t palette[12];
  for (Int_t i = 0; i < 12; i++) {
    Float_t shade = (12 - i) * 1. / 12.;
    if (gROOT->GetColor(i + 250)) {
      TColor* colour = gROOT->GetColor(i + 250);
      colour->SetRGB(shade, shade, shade);
    }
    else 
      TColor* colour = new TColor(250 + i, shade, shade, shade, 
				  Form("gray%02d", i * 5));
    palette[i] = 250 + i;
  }
  gStyle->SetPalette(12, palette);
}


//____________________________________________________________________
int mod_script(const TString& script) 
{
  // Function to put in a function name and print into a ROOT
  // TCanvas generated script 

  // First we get the basename sans extension 
  TString base(gSystem->BaseName(script.Data()));
  base.Remove(base.Length() - 2, 2);

  // Just a friendly message 
  cout << "Script: " << script << " (" << base << ")" << endl;

  // Open the file in read/write mode 
  fstream  file(script.Data(), ios::in | ios::out);
  // Go to the beginning 
  file.seekp(0, ios::beg);
  
  // put in the function name and some style options 
#if 1
  file << "void " << base << "() { //" << endl
       << "  gROOT->LoadMacro(\"various.C\"); "
       << "  setpawstyle(); " << endl
       << "gStyle->SetOptStat(0);gStyle->SetOptTitle(0);//" << flush;
#endif    
  // Try to find the canvas identifier, so we can use it later on 
  TString canvas;
  while (true) {
    TString token;
    token.ReadToken((istream&)file); 
    // IF we found the token `TCanvas' , the name is next 
    if (token == "TCanvas") {
      // Read the canvas identifier 
      canvas.ReadToken((istream&)file);
      // Remove the '*' in front if any 
      canvas.ReplaceAll("*", "");
      // And pring a friendly message 
      cout << "Canvas name: " << canvas << endl;
      break;
    }
    
    // End of of file and failures are the same here 
    if (file.fail()) {
      cerr << "Faiure while read" << endl;
      return 1;
    }
  }

  // Go back two characters (a '}' and '\n') from the end
  file.seekp(-2, ios::end);
  // and insert the print 
  file << "  " << canvas.Data() << "->Print(\"" << base 
       << ".eps\");\n}" << endl;
  // close the file and quit
  file.close();
  return 0;
}

//____________________________________________________________________
double knoscale(double mch) 
{
  return 0.35 * TMath::Power(mch - 1, 2) / mch;
}

double ppscale(double snn) 
{
  return -4.2 + 4.69 * TMath::Power(snn, 0.31);
}

double invknoscale(double omega) 
{
  double m = omega / 0.35 + 1;
  do {
  } while (knoscale(TMath::Sqrt(m++) * omega / 0.35 + 1) < omega);
  return m--;
}

double invppscale(double mch) 
{
  return TMath::Power((mch + 4.2) / 4.69, 1 / .31);
}

double snn2omega(double snn) 
{
  return knoscale(ppscale(snn));
}

double omega2snn(double omega) 
{
  return invppscale(invknoscale(omega));
}
//____________________________________________________________________
//
// EOF
//

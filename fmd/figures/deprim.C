void deprim() { 
  //
  gROOT->LoadMacro("fmd/figures/various.C");   
  setpawstyle(); 
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  TCanvas* c1 = new TCanvas("c1", "c1",10,10,699,699);
  gStyle->SetOptStat(0);
  c1->Range(-0.0681592,-94.1978,1.11973,817.566);
  c1->SetFillColor(10);
  c1->SetBorderSize(2);
  c1->SetFrameFillColor(0);
  c1->SetRightMargin(0.05);
  c1->SetTopMargin(0.05);
  
  TH1 *hdE1 = new TH1F("hdE1","Energy deposition per strip",100,0.05,1);
  hdE1->SetEntries(10240);
  hdE1->SetBinContent(0,2980);
  hdE1->SetBinContent(1,145);
  hdE1->SetBinContent(2,140);
  hdE1->SetBinContent(3,217);
  hdE1->SetBinContent(4,458);
  hdE1->SetBinContent(5,694);
  hdE1->SetBinContent(6,593);
  hdE1->SetBinContent(7,347);
  hdE1->SetBinContent(8,225);
  hdE1->SetBinContent(9,132);
  hdE1->SetBinContent(10,118);
  hdE1->SetBinContent(11,164);
  hdE1->SetBinContent(12,148);
  hdE1->SetBinContent(13,184);
  hdE1->SetBinContent(14,222);
  hdE1->SetBinContent(15,250);
  hdE1->SetBinContent(16,230);
  hdE1->SetBinContent(17,241);
  hdE1->SetBinContent(18,166);
  hdE1->SetBinContent(19,118);
  hdE1->SetBinContent(20,109);
  hdE1->SetBinContent(21,115);
  hdE1->SetBinContent(22,105);
  hdE1->SetBinContent(23,105);
  hdE1->SetBinContent(24,88);
  hdE1->SetBinContent(25,114);
  hdE1->SetBinContent(26,106);
  hdE1->SetBinContent(27,107);
  hdE1->SetBinContent(28,85);
  hdE1->SetBinContent(29,73);
  hdE1->SetBinContent(30,76);
  hdE1->SetBinContent(31,56);
  hdE1->SetBinContent(32,55);
  hdE1->SetBinContent(33,54);
  hdE1->SetBinContent(34,75);
  hdE1->SetBinContent(35,58);
  hdE1->SetBinContent(36,45);
  hdE1->SetBinContent(37,39);
  hdE1->SetBinContent(38,42);
  hdE1->SetBinContent(39,29);
  hdE1->SetBinContent(40,32);
  hdE1->SetBinContent(41,40);
  hdE1->SetBinContent(42,34);
  hdE1->SetBinContent(43,31);
  hdE1->SetBinContent(44,32);
  hdE1->SetBinContent(45,31);
  hdE1->SetBinContent(46,29);
  hdE1->SetBinContent(47,18);
  hdE1->SetBinContent(48,28);
  hdE1->SetBinContent(49,25);
  hdE1->SetBinContent(50,21);
  hdE1->SetBinContent(51,15);
  hdE1->SetBinContent(52,8);
  hdE1->SetBinContent(53,12);
  hdE1->SetBinContent(54,11);
  hdE1->SetBinContent(55,5);
  hdE1->SetBinContent(56,13);
  hdE1->SetBinContent(57,11);
  hdE1->SetBinContent(58,15);
  hdE1->SetBinContent(59,13);
  hdE1->SetBinContent(60,11);
  hdE1->SetBinContent(61,14);
  hdE1->SetBinContent(62,9);
  hdE1->SetBinContent(63,12);
  hdE1->SetBinContent(64,15);
  hdE1->SetBinContent(65,14);
  hdE1->SetBinContent(66,16);
  hdE1->SetBinContent(67,10);
  hdE1->SetBinContent(68,11);
  hdE1->SetBinContent(69,10);
  hdE1->SetBinContent(70,6);
  hdE1->SetBinContent(71,5);
  hdE1->SetBinContent(72,5);
  hdE1->SetBinContent(73,7);
  hdE1->SetBinContent(74,8);
  hdE1->SetBinContent(75,10);
  hdE1->SetBinContent(76,5);
  hdE1->SetBinContent(77,5);
  hdE1->SetBinContent(78,11);
  hdE1->SetBinContent(79,3);
  hdE1->SetBinContent(80,10);
  hdE1->SetBinContent(81,2);
  hdE1->SetBinContent(82,2);
  hdE1->SetBinContent(83,6);
  hdE1->SetBinContent(84,4);
  hdE1->SetBinContent(85,10);
  hdE1->SetBinContent(86,8);
  hdE1->SetBinContent(87,7);
  hdE1->SetBinContent(88,6);
  hdE1->SetBinContent(89,5);
  hdE1->SetBinContent(91,4);
  hdE1->SetBinContent(92,7);
  hdE1->SetBinContent(93,4);
  hdE1->SetBinContent(94,2);
  hdE1->SetBinContent(95,7);
  hdE1->SetBinContent(96,3);
  hdE1->SetBinContent(97,4);
  hdE1->SetBinContent(98,3);
  hdE1->SetBinContent(99,3);
  hdE1->SetBinContent(100,2);
  hdE1->SetBinContent(101,222);
  hdE1->SetFillColor(18);
  hdE1->GetXaxis()->SetTitle("energy deposited / pad [MeV]");
  hdE1->GetXaxis()->SetTitleOffset(.7);
  hdE1->GetXaxis()->SetTitleSize(.06);
  hdE1->GetXaxis()->SetTitleColor(1);
  hdE1->Draw("");
   
  TH1 *hdEprim1 = new TH1F("hdEprim1","nergy deposition  per strip",100,0.05,1);
  hdEprim1->SetEntries(10240);
  hdEprim1->SetBinContent(0,6078);
  hdEprim1->SetBinContent(1,110);
  hdEprim1->SetBinContent(2,103);
  hdEprim1->SetBinContent(3,220);
  hdEprim1->SetBinContent(4,554);
  hdEprim1->SetBinContent(5,718);
  hdEprim1->SetBinContent(6,526);
  hdEprim1->SetBinContent(7,246);
  hdEprim1->SetBinContent(8,147);
  hdEprim1->SetBinContent(9,90);
  hdEprim1->SetBinContent(10,73);
  hdEprim1->SetBinContent(11,72);
  hdEprim1->SetBinContent(12,74);
  hdEprim1->SetBinContent(13,96);
  hdEprim1->SetBinContent(14,123);
  hdEprim1->SetBinContent(15,143);
  hdEprim1->SetBinContent(16,135);
  hdEprim1->SetBinContent(17,104);
  hdEprim1->SetBinContent(18,61);
  hdEprim1->SetBinContent(19,49);
  hdEprim1->SetBinContent(20,35);
  hdEprim1->SetBinContent(21,30);
  hdEprim1->SetBinContent(22,27);
  hdEprim1->SetBinContent(23,39);
  hdEprim1->SetBinContent(24,32);
  hdEprim1->SetBinContent(25,41);
  hdEprim1->SetBinContent(26,31);
  hdEprim1->SetBinContent(27,33);
  hdEprim1->SetBinContent(28,21);
  hdEprim1->SetBinContent(29,13);
  hdEprim1->SetBinContent(30,13);
  hdEprim1->SetBinContent(31,8);
  hdEprim1->SetBinContent(32,12);
  hdEprim1->SetBinContent(33,14);
  hdEprim1->SetBinContent(34,14);
  hdEprim1->SetBinContent(35,6);
  hdEprim1->SetBinContent(36,5);
  hdEprim1->SetBinContent(37,9);
  hdEprim1->SetBinContent(38,11);
  hdEprim1->SetBinContent(39,3);
  hdEprim1->SetBinContent(40,1);
  hdEprim1->SetBinContent(41,3);
  hdEprim1->SetBinContent(42,4);
  hdEprim1->SetBinContent(43,7);
  hdEprim1->SetBinContent(44,7);
  hdEprim1->SetBinContent(45,5);
  hdEprim1->SetBinContent(46,5);
  hdEprim1->SetBinContent(48,6);
  hdEprim1->SetBinContent(49,7);
  hdEprim1->SetBinContent(50,2);
  hdEprim1->SetBinContent(51,4);
  hdEprim1->SetBinContent(52,4);
  hdEprim1->SetBinContent(53,3);
  hdEprim1->SetBinContent(54,4);
  hdEprim1->SetBinContent(55,1);
  hdEprim1->SetBinContent(56,4);
  hdEprim1->SetBinContent(57,2);
  hdEprim1->SetBinContent(58,3);
  hdEprim1->SetBinContent(59,2);
  hdEprim1->SetBinContent(60,1);
  hdEprim1->SetBinContent(61,1);
  hdEprim1->SetBinContent(62,1);
  hdEprim1->SetBinContent(63,1);
  hdEprim1->SetBinContent(64,3);
  hdEprim1->SetBinContent(65,3);
  hdEprim1->SetBinContent(66,2);
  hdEprim1->SetBinContent(68,1);
  hdEprim1->SetBinContent(69,3);
  hdEprim1->SetBinContent(70,3);
  hdEprim1->SetBinContent(71,1);
  hdEprim1->SetBinContent(72,1);
  hdEprim1->SetBinContent(73,1);
  hdEprim1->SetBinContent(74,2);
  hdEprim1->SetBinContent(77,1);
  hdEprim1->SetBinContent(78,1);
  hdEprim1->SetBinContent(81,2);
  hdEprim1->SetBinContent(85,2);
  hdEprim1->SetBinContent(86,2);
  hdEprim1->SetBinContent(88,1);
  hdEprim1->SetBinContent(101,14);
  hdEprim1->SetFillColor(14);
  hdEprim1->GetXaxis()->SetTitle("energy deposited");
  hdEprim1->Draw("same");

  TLegend* l = new TLegend(.45, .7, .945, .945);
  l->AddEntry(hdE1, "with secondaries", "f");
  l->AddEntry(hdEprim1, "primaries only", "f");
  l->SetBorderSize(0);
  l->SetTextFont(132);
  l->SetTextSize(.04);
  l->SetFillColor(0);
  l->Draw();
  
  c1->Modified();
  c1->cd();

  c1->Print("deprim.eps");
  // 
}
// 

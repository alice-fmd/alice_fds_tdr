void 
coeff()
{
//=========Macro generated from canvas: c/C
//=========  (Mon Apr  5 13:09:35 2004) by ROOT version4.00/03
   TCanvas *c = new TCanvas("c", "C",13,54,800,400);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c->Range(0,0,1,1);
   c->SetBorderMode(0);
   c->SetBorderSize(0);
   c->SetFrameFillColor(0);
  
// ------------>Primitives in pad: p1
   TPad *p1 = new TPad("p1", "P1",0,0,0.53,1);
   p1->Draw();
   p1->cd();
   p1->Range(-6.25,-0.1875,-1.5,1.0625);
   p1->SetFillColor(0);
   p1->SetBorderMode(0);
   p1->SetBorderSize(0);
   p1->SetLeftMargin(0.2);
   p1->SetRightMargin(0);
   p1->SetTopMargin(0.05);
   p1->SetBottomMargin(0.15);
   
   TH1 *hr1 = new TH2F("hr1"," Primary/total",100,-5.3, -1.5,100,0,1);
   hr1->SetStats(0);
   hr1->GetXaxis()->SetTitle(""); // " #eta interval");
   hr1->GetXaxis()->SetNdivisions(106);
   hr1->GetXaxis()->SetLabelFont(132);
   hr1->GetXaxis()->SetLabelSize(0.06);
   hr1->GetXaxis()->SetTitleSize(0.08);
   hr1->GetXaxis()->SetTitleOffset(0.8);
   hr1->GetXaxis()->SetTitleFont(132);
   hr1->GetYaxis()->SetTitle("N_{primaries}/N_{total}");
   hr1->GetYaxis()->SetNdivisions(110);
   hr1->GetYaxis()->SetLabelFont(132);
   hr1->GetYaxis()->SetLabelSize(0.06);
   hr1->GetYaxis()->SetTitleSize(0.08);
   hr1->GetYaxis()->SetTitleOffset(1.2);
   hr1->GetYaxis()->SetTitleFont(132);
   hr1->Draw("");
   
   gre = new TGraphErrors(12);
   gre->SetName("Graph");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   gre->SetPoint(0, -2.1,0.31361);
   gre->SetPointError(0,0,0.017398);
   gre->SetPoint(1, -2.2,0.313967);
   gre->SetPointError(1,0,0.0168655);
   gre->SetPoint(2, -2.3,0.333677);
   gre->SetPointError(2,0,0.019588);
   gre->SetPoint(3, -2.4,0.378997);
   gre->SetPointError(3,0,0.0217861);
   gre->SetPoint(4, -2.5,0.396974);
   gre->SetPointError(4,0,0.0250777);
   gre->SetPoint(5, -2.6,0.403928);
   gre->SetPointError(5,0,0.0269407);
   gre->SetPoint(6, -2.7,0.457072);
   gre->SetPointError(6,0,0.0264253);
   gre->SetPoint(7, -2.8,0.521607);
   gre->SetPointError(7,0,0.0261035);
   gre->SetPoint(8, -2.9,0.541032);
   gre->SetPointError(8,0,0.0274093);
   gre->SetPoint(9, -3,0.546277);
   gre->SetPointError(9,0,0.0274148);
   gre->SetPoint(10, -3.1,0.549388);
   gre->SetPointError(10,0,0.0277695);
   gre->SetPoint(11, -3.2,0.533019);
   gre->SetPointError(11,0,0.0248341);
   gre->Draw("p");
   
   gre = new TGraphErrors(5);
   gre->SetName("Graph");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   gre->SetPoint(0, -1.8,0.366467);
   gre->SetPointError(0,0,0.0176249);
   gre->SetPoint(1, -1.9,0.365186);
   gre->SetPointError(1,0,0.0191352);
   gre->SetPoint(2, -2,0.319044);
   gre->SetPointError(2,0,0.0176142);
   gre->SetPoint(3, -2.1,0.295705);
   gre->SetPointError(3,0,0.0153867);
   gre->SetPoint(4, -0,1.2948e-42);
   gre->SetPointError(4,0,0);
   gre->Draw("p");
   p1->Modified();
   c->cd();
  



// ------------>Primitives in pad: p2
   p2 = new TPad("p2", "P2",0.53,0,1,1);
   p2->Draw();
   p2->cd();
   p2->Range(1.4,-0.1875,3.61053,1.0625);
   p2->SetFillColor(0);
   p2->SetBorderMode(0);
   p2->SetBorderSize(0);
   p2->SetLeftMargin(0);
   p2->SetRightMargin(0.05);
   p2->SetTopMargin(0.05);
   p2->SetBottomMargin(0.15);
   

   TH1 *hr = new TH2F("hr"," Primary/total",100,1.5, 5.3,100,0,1);
   hr->SetStats(0);
   hr->GetXaxis()->SetTitle("#eta");
   hr->GetXaxis()->SetNdivisions(107);
   hr->GetXaxis()->SetLabelFont(132);
   hr->GetXaxis()->SetLabelSize(0.06);
   hr->GetXaxis()->SetTitleSize(0.08);
   hr->GetXaxis()->SetTitleOffset(0.8);
   hr->GetXaxis()->SetTitleFont(132);
   hr->GetYaxis()->SetTitle("primary/total");
   hr->GetYaxis()->SetNdivisions(110);
   hr->GetYaxis()->SetLabelFont(132);
   hr->GetYaxis()->SetLabelSize(0.06);
   hr->GetYaxis()->SetTitleSize(0.08);
   hr->GetYaxis()->SetTitleOffset(1.2);
   hr->GetYaxis()->SetTitleFont(132);
   hr->Draw("");
   
   TGraphErrors *gre = new TGraphErrors(14);
   gre->SetName("Graph");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   gre->SetPoint(0, 2.3,0.388144);
   gre->SetPointError(0,0,0.0206559);
   gre->SetPoint(1, 2.4,0.409817);
   gre->SetPointError(1,0,0.0217636);
   gre->SetPoint(2, 2.5,0.430522);
   gre->SetPointError(2,0,0.0282861);
   gre->SetPoint(3, 2.6,0.467116);
   gre->SetPointError(3,0,0.0222509);
   gre->SetPoint(4, 2.7,0.566052);
   gre->SetPointError(4,0,0.0282925);
   gre->SetPoint(5, 2.8,0.577585);
   gre->SetPointError(5,0,0.0319649);
   gre->SetPoint(6, 2.9,0.593673);
   gre->SetPointError(6,0,0.029521);
   gre->SetPoint(7, 3,0.616173);
   gre->SetPointError(7,0,0.0308968);
   gre->SetPoint(8, 3.1,0.631935);
   gre->SetPointError(8,0,0.0314951);
   gre->SetPoint(9, 3.2,0.627767);
   gre->SetPointError(9,0,0.0277437);
   gre->SetPoint(10, 3.3,0.665988);
   gre->SetPointError(10,0,0.0316761);
   gre->SetPoint(11, 3.4,0.692011);
   gre->SetPointError(11,0,0.0304853);
   gre->SetPoint(12, 3.5,0.704092);
   gre->SetPointError(12,0,0.0309877);
   gre->SetPoint(13, 3.86553e+31,0);
   gre->SetPointError(13,0,7.35765e-34);
   gre->Draw("p");
   
   gre = new TGraphErrors(5);
   gre->SetName("Graph");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   gre->SetPoint(0, 1.8,0.38245);
   gre->SetPointError(0,0,0.0173441);
   gre->SetPoint(1, 1.9,0.403778);
   gre->SetPointError(1,0,0.0165668);
   gre->SetPoint(2, 2,0.361573);
   gre->SetPointError(2,0,0.0198448);
   gre->SetPoint(3, 2.1,0.345649);
   gre->SetPointError(3,0,0.0177961);
   gre->SetPoint(4,0,3.0465);
   gre->SetPointError(4,0,7.34442e-34);
   gre->Draw("p");
   
   gre = new TGraphErrors(14);
   gre->SetName("Graph");
   gre->SetTitle("Graph");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   gre->SetPoint(0, 3.7,0.416748);
   gre->SetPointError(0,0,0.0256385);
   gre->SetPoint(1, 3.8,0.432836);
   gre->SetPointError(1,0,0.028525);
   gre->SetPoint(2, 3.9,0.444592);
   gre->SetPointError(2,0,0.0259953);
   gre->SetPoint(3, 4,0.451598);
   gre->SetPointError(3,0,0.0281706);
   gre->SetPoint(4, 4.1,0.468405);
   gre->SetPointError(4,0,0.0286593);
   gre->SetPoint(5, 4.2,0.471466);
   gre->SetPointError(5,0,0.0288757);
   gre->SetPoint(6, 4.3,0.481118);
   gre->SetPointError(6,0,0.0293623);
   gre->SetPoint(7, 4.4,0.490201);
   gre->SetPointError(7,0,0.0341686);
   gre->SetPoint(8, 4.5,0.485137);
   gre->SetPointError(8,0,0.0269335);
   gre->SetPoint(9, 4.6,0.487929);
   gre->SetPointError(9,0,0.0315414);
   gre->SetPoint(10, 4.7,0.505752);
   gre->SetPointError(10,0,0.0354473);
   gre->SetPoint(11, 4.8,0.512704);
   gre->SetPointError(11,0,0.0342731);
   gre->SetPoint(12, 4.9,0.513179);
   gre->SetPointError(12,0,0.0349593);
   gre->SetPoint(13, 3.86553e+31,3.04955);
   gre->SetPointError(13,0,7.34491e-34);
   gre->Draw("p");
   p2->Modified();

   c->cd();
   c->Modified();
   c->cd();
c->Print("coeff.eps");

}

%%
%% $Id: designconsiderations.tex,v 1.86 2005-06-12 15:40:26 gulbrand Exp $
%%
\section{General Design Considerations} 
\CVSKeyword{$Id: designconsiderations.tex,v 1.86 2005-06-12 15:40:26 gulbrand Exp $}
\CVSIdShow 

\subsection{\FMD{}{}  Geometry}

The space available for a Forward Multiplicity Detector in \ALICE{},
and thus its pseudorapidity coverage, is limited by the presence of a
number of detector systems (\ITS{}, \TPC{} and muon arm) whose main
components, dimensions and locations were decided long before the
\FMD{}{} was conceived.

In the central rapidity region, the \ITS{} and its services (cooling,
cables), the \VZERO{} detector, the \TZERO{} array, and the \TPC{}
support structure limit both the distance from the nominal
intersection point (IP) and the maximum outer diameter of the complete
device.  At larger pseudorapidities, the limitations are imposed by the
outer diameter (with tolerances and installation clearances) of the
\ALICE{} vacuum chamber, including complications arising from the
placement of flanges, beam tube support elements, bellows and pumps.
It has been agreed that the envelope of the \FMD{}{} toward the
\ALICE{} vacuum chamber will extend inwards to a radius of
$R_{min}=\unit[42]{mm}$.

On the \RB{26} side of the IP, the presence of the muon
absorber and the \VZERO{} counter limits the maximum distance from the IP
to \unit[80]{cm} while the \ITS{} limits the closest
distance from the IP to about 60 cm. In order to
accommodate services for the \ITS{}, the outer envelope of the \FMD{}{}
has been defined to stay within an angle of 20.7 degrees relative
to the beam direction, as measured from the IP, corresponding to a
limit of $\eta=-1.7$.

On the other side (\RB{24}) of the IP, the \FMD{}{} elements may in
principle be placed at any distance with $z>\unit[60]{cm}$ from the IP. The
limit towards central pseudorapidities imposed by the \ITS{} remains
the same as on the \RB{26} side, but the additional space away from
the IP allows for a larger coverage at small angles to the beam
direction.  For reference, the inside of the door of the \LTHREE{}
magnet return yoke is about \unit[600]{cm} from the IP.

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=.8\textwidth]{fmd/figures/fmd3d}
    \caption{Conceptual layout of the \FMD{}{} detector system showing the
      five rings placed around the beam pipe. The three sub-detector systems
      are called \FMD{1}{} (left), \FMD{2}{} (middle) and \FMD{3}{}
      (right). The muon arm is to the right.}
    \label{fig:fmd:designconsiderations:conceptuallayout}
  \end{center}
\end{figure}


A further important factor that limits the maximum achievable rapidity
coverage is the significant background of secondary particles arising
from interactions in the vacuum chamber elements at small angles.
Simulations indicate that the background from secondary particles
increases to more than 200\% for the smallest angles covered. This,
combined with the desire to enable a common mechanical support for the
\FMD{1}{} , \PMD{}, \VZERO{A} and \TZERO{A}, limits the maximum
forward pseudorapidity to $\eta=5.0$.

Within these severe geometrical constraints, we have chosen a design 
for the \FMD{}{} based on single layers of single--sided silicon strip 
detectors in a ring geometry, conceptually similar to the forward
ring system of Phobos \cite{fmd:phobos}.
Ring detectors are placed around the beam pipe in each of the two 
forward regions of \ALICE{}. A large pseudorapidity coverage may
be obtained by placing rings of identical design at different
distances from the IP. In this way, it has been possible to reduce the overall
complexity of the system by using only two basic building blocks.

The detector system consists of five rings of $10\,240$ Si--strips
each, with three ``inner'' rings divided into 20 azimuthal sectors
each, and two ``outer'' rings of 40 sectors each (see
\figref{fig:fmd:designconsiderations:conceptuallayout}). Each sector
will be read out independently and comprises 512 and 256 detector
strips for inner and outer rings, respectively.

The design of the rings is intimately coupled to the choice of silicon
wafer technology. The elements that are combined to build the various
rings must be manufactured out of individual circular Si wafers.
Currently, a small number of suppliers manufacture 6~inch Si--wafers,
permitting the coverage of the desired pseudorapidity range with two
designs of counters (one for ``inner'' rings and one for ``outer''
rings). Basing the design on wafers of smaller dimensions would
require the use of three rings, leading to the system's higher overall
cost and complexity.

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=.7\textwidth]{fmd/figures/AliceFMD1}
    \caption{Cross section of the forward detector system layout on
      the \RB{26} side. The muon absorber is to the right.  The two
      rings of the \FMD{3}{} detector are mounted on one support cone.
      This cone also supports the beam pipe to the left of the
      indicated bellows. The \VZERO{C} and \TZERO{C} detectors can
      also be seen.}
    \label{fig:fmd:designconsiderations:forwardlayout1}
  \end{center}
\end{figure}

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=.7\textwidth]{fmd/figures/AliceFMD2}
    \caption{Cross section of \FMD{2}{} on the \RB{24} side of the IP.
      The two \FMD{2}{} rings are shown at $z=\unit[752]{mm}$ and
      $z=\unit[834]{mm}$ from the IP and attached to their
      cylindrical mechanical support. This support also supports the
      beam pipe.}
    \label{fig:fmd:designconsiderations:forwardlayout2}
  \end{center}
\end{figure}

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=.6\textwidth]{fmd/figures/AliceFMD3new}
    %% \includegraphics[width=.8\textwidth]{introduction/figures/fwd-outernewz}
    %% \includegraphics[width=.8\textwidth]{introduction/figures/fwd-outer}
    \caption{Drawing of the placement of the \FMD{1}{} and \FMD{2}{}
      rings together with \TZERO{A}, \VZERO{A} and the \PMD{} on the
      \RB{24} side. The mechanical support for the three forward
      detectors near $z =\unit[3.2]{m}$ is attached to a common
      structure mounted on the \TPC{} service support wheel.  This
      structure will also support the vacuum valve behind \TZERO{A}
      and the central beam pipe. The \FMD{2}{} cable path is
      indicated.}
    \label{fig:fmd:designconsiderations:forwardlayout3}
  \end{center}
\end{figure}
\begin{table}[htb]
  \begin{center}
    \caption{Geometrical size of each of the five \FMD{}{} rings.  The
      table lists the nominal distance, $z$, from the IP to the
      detector plane, the inner and outer radii, and the corresponding
      pseudorapidity coverage of each ring.}
    \label{tab:fmd:designconsiderations:etacoverage}
    \vglue0.2cm
    \begin{tabular}{|l|c|c|c|c|}
      \hline
      \textbf{Ring}  &
      \textbf{$z$ (\unit{cm})} &
      \textbf{$R_{in}$ (\unit{cm})}&
      \textbf{$R_{out}$ (\unit{cm})}&
      \textbf{$\eta$ coverage}\\
      \hline
      \FMD{1}{}  & 320.0 &  \phantom{0}4.2 & 17.2 & $3.68<\eta<5.03$\\
      \FMD{2}{i} &  \phantom{0}83.4 &  \phantom{0}4.2 & 17.2 & $2.28<\eta<3.68$\\
      \FMD{2}{o} &  \phantom{0}75.2 & 15.4 & 28.4 & $1.70<\eta<2.29$\\
      \FMD{3}{o} & $-75.2$ & 15.4 & 28.4 & $-2.29<\eta<-1.70$\\
      \FMD{3}{i} & $-62.8$ & \phantom{0}4.2 & 17.2 & $-3.40<\eta<-2.01$\\
      \hline
    \end{tabular}
  \end{center}
\end{table}



The positions and sizes of the various Si rings and the corresponding
pseudorapidity coverages are listed in
\tabref{tab:fmd:designconsiderations:etacoverage}.
\figsref{fig:fmd:designconsiderations:forwardlayout1},
\ref{fig:fmd:designconsiderations:forwardlayout2} and,
\ref{fig:fmd:designconsiderations:forwardlayout3} show details of the
geometry and placement of the \FMD{}{} rings in \ALICE{}.

Together with the \ITS{} inner pixel layer (dashed line in
\figref{fig:fmd:etacoverage}), the design ensures a full
pseudorapidity coverage in the range $-1.7<\eta<5.0$, and an overlap
between the \FMD{}{} and \ITS{} inner pixel layer system of about
${\Delta}{\eta}=0.2$. Note that the placement of the \FMD{2}{} and
\FMD{3}{} rings is not symmetric with respect to the IP. This
asymmetry ensures `seamless' pseudorapidity coverage between \FMD{1}{}
and \FMD{2}{} at the cost of less overlap between the inner and outer
ring of \FMD{2}{}.

\subsection{\FMD{}{} Segmentation}

The dimensions and the layout of the \FMD{}{} rings and their
segmentation in sensor elements are primarily determined by the
size of presently available silicon wafers. 

The segmentation of each sensor into strips has been driven by the
desire to keep the maximum mean number of hits per strip below a few
particles for most strips, even in central \PbPbCol{} collisions,
thereby enabling an accurate multiplicity reconstruction based on
total energy deposition in a single strip and possible determination
of multiplicity by statistical analysis of empty and filled channels,
while respecting the constraints imposed by matching to the front--end
(FE) electronics and limiting the number of read--out channels.

We have chosen to sub--divide each Si sensor into two phi sectors,
leading to an azimuthal segmentation of 20 for the inner rings and 40
for the outer rings. Due to fluctuations in the mean number of hits
and in the energy deposition per particle, each detector must be
segmented to have a linear response for signals with amplitudes up to 10
times the average signal. It follows that the FE electronics must
be able to handle a maximum signal deposition corresponding to about
20 MIPs. The chosen segmentation ensures total strip capacitance of
around \unit[10--20]{pF}, which is well adapted to the solution chosen
for the preamplifier chips of the FE electronics.

Based on these considerations, we arrive at the detector segmentation
given in \tabref{tab:fmd:table:stripdimension}.

\begin{table}[htb]
  \begin{center}
    \caption{Physical dimensions of Si segments and
      strips, together with the average number of charged particles
      impinging on each strip in simulated central \PbPbCol{} collisions.}
    \label{tab:fmd:table:stripdimension}
    \vglue0.2cm
    \begin{tabular}{|l|c|c|c|c|c|c|}
      \hline
      &
      \textbf{Radial} &
      \textbf{Particle}&
      \textbf{Azimuthal}&
      \textbf{Radial} &
      \textbf{Strip area} &
      \textbf{Average}\\
      &
      \textbf{coverage} &
      \textbf{flux} &
      \textbf{sectors} &
      \textbf{strips} &
      \textbf{($\unit{cm}^{2}$)}&
      \textbf{number} \\
      &
      \textbf{($\unit{cm}$)} &
      \textbf{($\unit{cm}^{-2}$)}&
      &
      &
      &
      \textbf{of hits} \\
      \hline
      \FMD{1}{} &
      \phantom{0}4.2--17.2&
      \phantom{0}6--27&
      20&
      512&
      0.03--0.14&
      0.6--1.3 \\ 
      \FMD{2}{i} &
      \phantom{0}4.2--17.2&
      \phantom{0}8--35&
      20&
      512&
      0.03--0.14&
      1.5--0.9 \\
      \FMD{2}{o}&
      15.4--28.4&
      \phantom{0}3--8\phantom{0} &
      40&
      256&
      0.12--0.23&
      1.2--0.7 \\
      \FMD{3}{i} &
      \phantom{0}4.2--17.2 &
      10--65&
      20& 512&
      0.03--0.14&
      2.7--1.2\\
      \FMD{3}{o} &
      15.4--28.4&
      \phantom{0}3--8\phantom{0} &
      40&
      256&
      0.12--0.23&
      1.0--0.6 \\
      \hline
    \end{tabular}
 \end{center}
\end{table}



%%\subsection{Multiplicity Calculation}

\subsection{Performance Simulations}

\subsubsection{Simulation Package}

All simulations of particle densities shown in this document have been
carried out using the \ALIROOT{} simulation package (version
3.09\footnote{In winter 2003/2004, the definition of the \ALICE{}
  coordinate system was changed so that $\eta \to -\eta$. However, in
  \ALIROOT{3.09} the old definition of $\eta$ was used, but in the
  plots in this document, the new definition has been applied.}) using
the \HIJING{} generator (which assumes that $dN/d\eta \approx 8000$
around $\eta=0$ for 0--5\% central \PbPbCol{} collisions at the LHC
design energy) as input. The simulation code calculates the energy
loss in each \FMD{}{} strip from all charged particles which hit the
strip, including secondary (background) particles, produced in
interaction with material in the beam pipe and in \ALICE{}.

\subsubsection{Charged Particle Densities in the \FMD{}{} }

The \FMD{}{} is not an isolated detector. The scattering of primary particles
on material in various other \ALICE{} detectors and support components
leads to the copious production of secondary particles. Such
secondaries contribute to the measured total energy and the number of
hits, hence to the determined multiplicity. The primary sources of
this background are:

\begin{tabular}{lp{.8\textwidth}}
  i)   & the beam pipe and its various flanges, collars, pumps etc.;\\
  ii)  & the \ITS{} with its mounting frame and services;\\
  iii) & numerous cables, mainly from \ITS{};\\
  iv)  & the \TZERO{} and \VZERO{} detectors with their mounting
         arrangements; and\\
  v)   & the absorber for the muon arm.
\end{tabular}


\begin{figure}[htbp]
  \centering
  \includegraphics[width=\textwidth]{fmd/figures/meanMultSigma}
  \caption{Average number of hits per strip as a function of the
    $\eta$ and radial distance to the beam pipe centre (top axis), for
    the five \FMD{}{} rings.  The simulation is for central collisions.
    The error bars show $RMS/\sqrt{N}$ for the distribution for each
    collection of strips.}
  \label{fig:fmd:designconsiderations:mean_number_of_hits}
\end{figure}


\Figref{fig:fmd:designconsiderations:mean_number_of_hits} shows the
average number of hits per strip as a function of the radial distance
from the centre of the beam pipe for the five \FMD{}{} rings, calculated
with \ALIROOT{}. A breakdown of the contribution from each of these is
shown in \figref{fig:fmd:designconsiderations:particleorigin} and
listed in \tabref{tab:fmd:designconsiderations:tableofparticles}.  It
is noted that the dominant sources of secondaries are the \ALICE{} beam
pipe and the \ITS{}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/contrib}
  \caption{Breakdown of the total number of particles (primary and
    secondary) impinging on each of the five \FMD{}{} rings. See also
    \tabref{tab:fmd:designconsiderations:tableofparticles}.}
  \label{fig:fmd:designconsiderations:particleorigin}
\end{figure}

\begin{table}[htbp]
  \begin{center}
    \caption{Number of particles per square centimetre impinging on
      the \FMD{}{} detectors. The table lists the total number of
      particles (primary and secondary) and a breakdown of  the
      secondaries produced in the beam pipe, the \ITS{}, the absorber
      in the muon arm, and the \TZERO{}.}
    \label{tab:fmd:designconsiderations:tableofparticles}
    \vglue0.2cm
    \footnotesize
    \begin{tabular}{|r|r|r|r|r|r|r|r|r|r|r|r|}
      \hline
      \multicolumn{1}{|c|}{\textbf{Radius }} &
      \multicolumn{1}{c|}{\textbf{Total }} &
      \multicolumn{1}{c|}{\textbf{Primary }} &
      \multicolumn{1}{c|}{\textbf{Primary/ }} &
      \multicolumn{1}{c|}{\textbf{Pipe }} &
      \multicolumn{1}{c|}{\textbf{Pipe/ }} &
      \multicolumn{1}{c|}{\textbf{\ITS{}}} &
      \multicolumn{1}{c|}{\textbf{\ITS{}/}} &
      \multicolumn{1}{c|}{\textbf{Abs }} &
      \multicolumn{1}{c|}{\textbf{Abs/  }} &
      \multicolumn{1}{c|}{\textbf{\TZERO{} }} &
      \multicolumn{1}{c|}{\textbf{\TZERO{}/}}\\
      &
      &
      &
      \multicolumn{1}{c|}{\textbf{total}} &
      &
      \multicolumn{1}{c|}{\textbf{total}} &
      &
      \multicolumn{1}{c|}{\textbf{total}} &
      &
      \multicolumn{1}{c|}{\textbf{total}} &
      &
      \multicolumn{1}{c|}{\textbf{total}} \\
      \hline
      \multicolumn{12}{|l|}{\FMD{1}{}} \\
      \hline
      5 & 27.1& 13.6& 0.50&  9.2& 0.33& 3.8& 0.14& 0.1& 0.00& 0.0& 0.00\\
      10& 13.3&  6.1& 0.46&  3.0& 0.22& 3.1& 0.23& 0.0& 0.00& 0.0& 0.00\\
      15&  6.9&  3.4& 0.48&  1.5& 0.20& 1.7& 0.24& 0.0& 0.00& 0.1& 0.01\\
      \hline
      \multicolumn{12}{|l|}{\FMD{2}{i}} \\
      \hline
      5 & 33.1& 23.5& 0.71&  5.7& 0.17& 3.6& 0.11& 0.0& 0.00& 0.0& 0.00\\
      10& 16.7&  9.2& 0.55&  2.6& 0.15& 3.7& 0.22& 0.1& 0.00& 0.0& 0.00\\
      15& 10.0&  4.6& 0.46&  1.0& 0.09& 4.0& 0.39& 0.0& 0.00& 0.0& 0.00\\
      \hline
      \multicolumn{12}{|l|}{\FMD{2}{o}}\\
      \hline
      16&  6.2&  2.4& 0.39&  0.6& 0.09& 3.1& 0.49& 0.0& 0.00& 0.0& 0.00\\
      21&  5.2&  2.0& 0.38&  0.4& 0.07& 2.7& 0.51& 0.0& 0.00& 0.0& 0.00\\
      26&  4.3&  1.8& 0.40&  0.4& 0.08& 2.1& 0.47& 0.0& 0.00& 0.0& 0.00\\
      \hline
      \multicolumn{12}{|l|}{\FMD{3}{o}}\\
      \hline
      16&  7.5&  2.4& 0.32&  0.9& 0.11& 3.2& 0.43& 0.2& 0.02& 0.1& 0.01\\
      21&  6.1&  1.8& 0.30&  0.8& 0.12& 2.7& 0.44& 0.2& 0.03& 0.0& 0.00\\
      26&  4.0&  1.6& 0.39&  0.3& 0.06& 1.6& 0.40& 0.2& 0.04& 0.1& 0.03\\
      \hline
      \multicolumn{12}{|l|}{\FMD{3}{i}}  \\
      \hline
      5 & 62.6& 25.7& 0.41& 33.1& 0.52& 3.2& 0.05& 0.0& 0.00& 0.0& 0.00\\
      10& 23.1& 11.2& 0.48&  5.0& 0.21& 6.1& 0.26& 0.2& 0.00& 0.1& 0.00\\
      15& 12.2&  4.4& 0.36&  1.1& 0.09& 5.9& 0.49& 0.3& 0.02& 0.1& 0.01\\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\subsubsection{Energy Loss Spectrum}

The energy loss spectrum of particles traversing a strip can be
described by a Landau distribution. For multiple hits, the spectrum is
a folding of Landau functions, as displayed in
\figref{fig:fmd:designconsiderations:landautails}, resulting in
progressively broader energy peaks with increasing multiplicity.
\Figref{fig:fmd:designconsiderations:energyloss} shows a typical
energy loss spectrum, both with and without background, for a single
strip in \FMD{3}{i}.

\begin{figure}[htbp]
  \centering
  \subfigure[]{%
    \label{fig:fmd:designconsiderations:landautails}%
    \includegraphics[width=.45\textwidth]{fmd/figures/ampl}}
  \subfigure[]{%
    \label{fig:fmd:designconsiderations:energyloss}%
    \includegraphics[width=.45\textwidth]{fmd/figures/deprim}}

  \caption{\subref{fig:fmd:designconsiderations:landautails} Typical
    energy loss distributions (Landau) for $1,2,\ldots, 6$ particles
    with minimum ionising energy (MIP) impinging on a detector element.
    Note how the width of the distribution broadens with increasing
    number of particles.  \newline
    \subref{fig:fmd:designconsiderations:energyloss} Typical energy
    loss spectrum (accumulated over several events) for a strip in
    \FMD{3}{i} for 5\% central collisions.  Simulations are shown with
    (light grey) and without (dark grey) the contribution from
    particles from secondary interactions.}
\end{figure}


\subsubsection{Multiplicity Reconstruction}

In general, there are two methods to determine the charged
particle multiplicity.

\begin{itemize}
\item Measuring the total deposited energy, above some appropriate
  threshold in a strip or a group of strips, and dividing this total
  energy by the average expected energy deposited by a particle (see
  \secref{sec:fmd:designconsiderations:m_from_e}).
  
\item Counting the number of strips in which energy is deposited above
  threshold and comparing it to the number of empty strips (see
  \secref{sec:fmd:designconsiderations:m_from_pattern}).
\end{itemize}

Several effects contribute to obscuring the primary multiplicity
information.

\begin{enumerate}
\item Background from secondary interactions in material extraneous to
  the Si detectors.

\item Distribution of the energy loss in a detector segment. The
  average energy loss of a single particle in the detector material
  (typically 300--400 ${\mu}m$ of Si) depends on the particle momentum.
  The energy loss distribution around the most probable value can be
  represented by a Landau function.

\item Counting statistics. The distribution of the number of particles
  that hit a detector segment follows a Poisson distribution.

\item Accumulated electronics noise. 
\end{enumerate}
These factors are addressed in
\secref{sec:fmd:designconsiderations:bg}.


\subsubsection{Counting Particles using the Deposited Energy}
\label{sec:fmd:designconsiderations:m_from_e}

The most direct way to determine the multiplicity of charged particles
in the \FMD{}{} is to divide the total energy signal measured in a
strip, or in a group of strips, by the average energy deposited by
particles originating from the collision. The latter number can either
be estimated from Monte Carlo calculations (e.g., \ALIROOT{}) or
deduced from the in--beam measurements of very low multiplicity events
themselves for single hits.  To arrive at the `primary' multiplicity,
the measured multiplicity must be corrected for the contribution from
secondaries as estimated from a Monte Carlo simulation of the
experiment.

The relative accuracy of the multiplicity determination can be
increased by adding signals from several strips e.g., by summing over
strips at different azimuthal angles but at similar rapidities, or
integrating over rapidity by grouping strips belonging to a given
azimuthal sector.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.4\textwidth]{fmd/figures/npart}
  \caption{Reconstructed multiplicity using the energy method
    versus generated multiplicity.  The error bars show the
    root--mean--square of the individual reconstructed multiplicity
    distributions.}
  \label{fig:fmd:designconsidertions:reconstructedversusincident}
\end{figure}

\Figref{fig:fmd:designconsidertions:reconstructedversusincident} shows
the reconstructed multiplicity as a function of the number of
generated particles impinging on the detector.  The simulations were
carried out using a single central \HIJING{} event for \PbPbCol{} and
integrating over strips in the pseudorapidity interval $3.58 < \eta
\leq 1.51$.  The energy loss spectrum was generated by modelling each
detector hit by a Landau distribution. The reconstructed multiplicity
was obtained by counting the number of hits in energy intervals
${\Delta}E_{n}$ up to the $n^{\text{th}}$ peak. The following iterative
procedure was used. In the first step the energy loss corresponding to
one particle was required to lie in the interval from zero to the
minimum of the sum of the Landau distributions corresponding to two
(${\Delta}E_{2}$).  Similarly, two hits were defined as energy
deposited in the interval from ${\Delta}E_{2}$ to ${\Delta}E_{3}$, the
latter being the minimum of the sum of three Landau distributions, and
so on.  In the next step, the set of interval boundaries
${\Delta}E_{2}, {\Delta}E_{3},\ldots$ was adjusted in order to
minimize the difference between the generated and reconstructed
multiplicity distributions.

\begin{figure}[htbp]
  \centering 
  \includegraphics[width=.8\textwidth]{fmd/figures/accc1}
  \includegraphics[width=.8\textwidth]{fmd/figures/accc2}
  \caption{Relative accuracy of multiplicity reconstruction.  Top panel
    shows the relative difference between the reconstructed and the
    input multiplicity in the $\eta$ range of the inner and outer
    \FMD{3}{} detector. The lower panel shows the distribution of
    residuals, summed over $\eta$ ranges.}
  \label{fig:fmd:designconsiderations:energymultresolution}
\end{figure}

The relative accuracy of the multiplicity reconstruction (based on 20
central \HIJING{} events) as a function of pseudorapidity
(${\Delta}{\eta} =0.1$) is shown in the top panel of
\figref{fig:fmd:designconsiderations:energymultresolution}. The lower
panel shows the relative accuracy of the multiplicity reconstruction
integrating over the pseudorapidity interval $1.9~\leq~\eta~\leq~3.5$,
corresponding to the pseudorapidity interval covered by the outer
rings \FMD{2}{} and \FMD{3}{}.  The distribution is Gaussian with $\sigma=7\%$
for \FMD{2+3}{o}.  Similarly, the relative reconstruction accuracy for
an azimuthal sector of \FMD{3}{i} is $\sigma=6\%$ and $\sigma=12\%$
for a sector of \FMD{3}{o}.


\subsubsection{Counting Particles using the Hit Pattern}
\label{sec:fmd:designconsiderations:m_from_pattern}

The average multiplicity can also be determined by studying the
pattern of hits across strips, provided that the average multiplicity
per strip is small (one hit or less on the average) and that the
average multiplicity is uniformly distributed over the section of
the detector being considered. This will typically be the case for
non--central \PbPbCol{} collisions. For very peripheral collisions or
\ppCol{} collisions the mean number of hits is so low that the
multiplicity of particles can be accurately obtained by simply
counting the number of pads hit.

The distribution of hits (m) on a strip is the Poisson distribution
$P\left\{m\right\} = \frac{\lambda ^{m} e^{-\lambda}}{m!}$, where
$\lambda$ is the mean number of hits. The probability
for no hits on a strip is $P\left\{0\right\} = e^{-\lambda }$. Since
$P\left\{0\right\} = {N_e}/{N_{tot}}$, where $N_{e}$ and $N_{tot}$
are the number of empty strips and the total number of strips in the
selected region of the detector, respectively, the average
multiplicity, $\lambda$, can be determined.

In the multiplicity reconstruction algorithm it has been assumed that
a strip is empty when the ADC signal is below a given threshold.  In
\figref{fig:fmd:designconsiderations:genrec} the reconstructed
multiplicity is shown versus the known input.  The intrinsic accuracy
of this method can be better than 3\%, as shown in
\figref{fig:fmd:designconsiderations:genrel} (circles).

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/genrec}
  \caption{Reconstructed multiplicity ($dN/d\eta$) from
    \HIJING{}, using the Poisson method, as a function of the
    generated multiplicity for three different pseudorapidity
    intervals.}
  \label{fig:fmd:designconsiderations:genrec}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/genrel}
  \caption{Relative error for $\eta=-1.7$
    (\FMD{3}{o}), $\eta=-3.4$ (\FMD{3}{i}) and $\eta=5$ (\FMD{1}{})
    using the Poisson method (circles).  Triangles show the relative
    error after subtracting the contribution of secondaries, which
    are known event by event from the simulations.}
  \label{fig:fmd:designconsiderations:genrel}
\end{figure}

%% \subsubsection{Corrections for secondaries.}
\subsubsection{Reconstruction of Multiplicity Distribution}
\label{sec:fmd:designconsiderations:bg}

To obtain the real (primary) multiplicity, the contribution from
secondary particles must be estimated and subtracted. The majority of
secondaries originate from interactions in the \ITS{} detector
structure and in the beam pipe (see
\tabref{tab:fmd:designconsiderations:tableofparticles}). The
correction coefficients as a function of $\eta$ have been determined
using a sample of 200 \HIJING{} events calculated for \PbPbCol{} collisions
with impact parameters in the range $\unit[0]{fm}<b<\unit[11.2]{fm}$.
These are shown in
\figref{fig:fmd:designconsiderations:backgroundcorrectioncoeff}.

The error on the correction coefficients depends on multiplicity.  For
most central events it is about a factor of 10 smaller than that shown
in \figref{fig:fmd:designconsiderations:backgroundcorrectioncoeff} for
minimum bias events.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/coeff}
  \caption{Multiplicity correction coefficient (ratio of primaries to
    total) as a function of $\eta$.  The discontinuity $\eta=3.6$ is
    due to the different background conditions for the \FMD{2}{i}
    ($z=\unit[83.4]{cm}$) and \FMD{1}{} ($z=\unit[345]{cm}$)
    detectors.}
  \label{fig:fmd:designconsiderations:backgroundcorrectioncoeff}
\end{figure}

\Figref{fig:fmd:designconsiderations:evlLR} shows the reconstructed
multiplicity distribution using the Poisson method (circles and
triangles) compared to the input \HIJING{} distribution (80 events).
The background correction factors of
\figref{fig:fmd:designconsiderations:backgroundcorrectioncoeff} have
been used.

\begin{figure}[htb]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/ev1}
  \caption{Reconstructed $dN/d\eta$ distribution for 80 \HIJING{}
    events (points) using the background correction factors shown in
    \figref{fig:fmd:designconsiderations:backgroundcorrectioncoeff}.
    The full drawn line is the input \HIJING{} distribution. The
    distribution of primary particles that hit the detector is shown
    by the short--dashed line (the difference relative to the input
    distribution is due to absorbed particles). The dashed--dotted line
    shows all the particle hits registered in the detector (primaries
    and secondaries). } 
  %% BSN:  Why are there holes in the positive $\eta$ distributions?
  %% ALLA: These are problem of binning in histogram, on the positive
  %%       side detectors are overlap
  \label{fig:fmd:designconsiderations:evlLR}
\end{figure}

In the general case, where the background correction coefficients are
not known because the shape of the primary multiplicity distribution is
not that predicted by \HIJING{}, an iterative procedure must be
employed. In this method, the first trial for the primary multiplicity
distribution is propagated through \ALIROOT{} and compared to the
measured multiplicity distribution. The ratio of this calculated
distribution to the measured distribution is used to scale the trial
multiplicity distribution, which again is propagated through
\ALIROOT{}. This process is repeated until convergence is achieved. In
practice, this requires only a few steps. A suitable trial input
distribution is the measured multiplicity distribution scaled by the
background correction factors calculated by propagating \HIJING{}
events through \ALIROOT{}.

The reliability of the method is shown in
\figref{fig:fmd:designconsiderations:stepiteration}. Here, the input
distribution is assumed to go to zero outside the interval
$\eta=2,3.3$. The left panel shows the first iteration, the right
panel shows convergence after two iterations. The main distribution is
recovered but the `edges' are smeared. In
\figref{fig:fmd:designconsiderations:flatiteration} a flat, wide
input distribution is used. The figure shows the three first iterations.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/eta}
  \caption{Iterative procedure to extract a narrow rapidity
    distribution with sharp edges (non--\HIJING{} shape).  The full
    drawn line is the input \HIJING{} distribution. The distribution
    of primary particles that hit the detector is shown with the
    short--dashed lines. The dotted line shows all the particle hits
    registered in the detector (primaries and secondaries).  The
    triangles show the reconstructed distribution. The figure shows
    the first three iterations.}
  %% Red: primary distribution,
  %% blue: particle hits in detector(primary + background),
  %% black: primaries hits in detector,
  %% points: reconstructed primary distribution.
  \label{fig:fmd:designconsiderations:stepiteration}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/flat}
  \caption{As \figref{fig:fmd:designconsiderations:stepiteration},
    assuming a wide and flat input rapidity distribution.  The figure
    shows the first three iterations.}
  \label{fig:fmd:designconsiderations:flatiteration}
\end{figure}

It is possible to give an estimate of the sensitivity to narrow local
multiplicity fluctuations using
\figref{fig:fmd:designconsiderations:stepiteration}. This figure shows
how a sharp edge signal is propagated into the detector at $\eta
\approx -2$.  It is noted that $\approx 70\% $ of the signal survives
and that the remaining 30\% is `amplified' and smeared in $\eta$,
leading to an effective signal-to-noise ratio $\approx 1/1$. This
smearing corresponds to an effective $\Delta \eta \approx 0.12$ and
$\Delta \phi \approx \unit[2]{{}^{\circ}}$. With $dN/d\eta \approx
8000$, such an interval will contain $\approx 5$ charged particles and
lead to a signal + background of $\approx 9$.  In this particular
example a multiplicity fluctuation in a narrow region e.g.,
$\Delta\eta \approx 0.12 $ and $\Delta \phi \approx
\unit[2]{{{}^{\circ}}}$, would need 15--20 particles to give a
significant signal. Sensitivity to broader fluctuations follow
straightforward statistics, but for each part of the detector the
signal-to-background has to be well known from Monte Carlo
calculations.

\subsubsection{Elliptic Flow}

Information provided by the \FMD{}{} can be used to study the
hydrodynamical and thermodynamical properties of the high density and
high temperature state produced in the heavy-ion collisions.  The
azimuthal distribution of produced particles emitted in a nuclear
collision is correlated with the orientation of the reaction plane
\cite{Gustafsson:ka}.  From measurements of azimuthal asymmetry of the
charged particle distribution, it is possible to determine the
magnitude of the directed and elliptic flow and their centrality
dependence.  The \FMD{}{} enables the study of flow effects via the
azimuthal dependence of the multiplicity of charged particles.

A Fourier expansion of the azimuthal distribution of particles
reads (with omission of higher order terms):

\begin{equation*}
  r\left( \phi \right) =\frac{1}{2\pi} \left(1 + 2v_{1} \cos
    \left(\phi - \psi_{1} \right) + 2 v_{2} \cos\left(2 \left[\phi
        -\psi_{2}\right]\right)\right) \quad,\eqno 
\end{equation*}
where $\phi $ is the azimuthal angle of the measured particles,
$\psi_{1}$ and $\psi _{2} $ are the angles of the first and second
order event planes respectively, and $v_{1} $ and $v_2$ denote the
first order and the second order flow coefficients (directed and
elliptic flow, respectively). In the simulations we have assumed $\psi
_{1} =\psi _{2} (=\psi _{R} )$, where $\psi _{R} $ is the angle of the
reaction plane. $\psi_R$ was chosen randomly for each event.

A simulation using $10^{4}$ events with the same impact parameter and
fixed values of $v_{1} $ , $v_{2} $, but different $\psi_R$ was
generated using the \HIJING{}Param +
\texttt{AfterBurnerFlow}\footnote{\HIJING{}Param is a parametrization  
  of \HIJING{} without flow.  \texttt{AfterBurnerFlow} is an
  afterburner that generates flow.} codes. To illustrate the
sensitivity to the elliptical flow, we have set $v_{1} =0 $ in the
simulations.

In the reconstruction, the n$^{th}$ order event plane $\psi _{n} $
was calculated from
\begin{equation}
  \psi_{n} = \frac{1}{n} \tan^{-1} \left( \frac{\sum\limits_{i} w_{i}
      \sin n\phi_{i}}{\sum\limits_{i}w_{i} \cos n\phi_{i}}
  \right)\quad, 
\end{equation}
where $\phi _{i}$ is the azimuthal angle of the i$^{th}$ outgoing
particle. The event plane was determined using the number of hits,
where all hits have been assigned equal weights $w_{i} $. The
event plane can also be determined using energy deposition. In
this case the weights are given by the energy deposited. The
parameter $v_{2}$ was determined as $v_{2} =\left\langle\cos 2\left( \phi
-\psi _{R} \right)
\right\rangle$.

The estimated event plane differs from the actual event plane.  the
calculation gives incorrect  $v'$ which should be corrected by a
Resolution Correction Factor (RCF) given by
\cite{Poskanzer:1998yz,fmd:Raniwala}:
\begin{equation}
  \label{eq:fmd:designconsiderations:RCF}
  RCF=\left\langle\cos \left( n\left( \psi _{n} -\psi _{R} \right)
    \right) \right\rangle=\frac{\sqrt{\pi } }{2\sqrt{2} } \chi _{n}
  \exp \left(-\chi_{n}^{2} / 4 \right) \left[ I_{\frac{n-1}{2}}
    \left(\chi_{n}^{2} / 4 \right) + I_{\frac{n+1}{2}}
    \left(\chi_{n}^{2} / 4\right)\right]\quad,
\end{equation}
where $I_{v}$ is a Bessel function of order $v$ and $\chi _{n} $ is
defined from the sub--event method \cite{Ollitrault:bk,Ollitrault:ba}.
In this approach each event is randomly divided into two equally sized
sub--events and flow angles $\psi_{n}^{a} $ and $\psi _{n}^{b} $ for
each sub--event are determined. The distribution $\left| \psi _{n}^{a}
  -\psi _{n}^{b} \right| $ can be calculated analytically
\cite{Poskanzer:1998yz,Ollitrault:bk}. The fraction of events in the
data sample yielding an angle between sub--events of more than $\pi
/2n$ is used to calculate $\chi _{n}$
\begin{equation}
  \label{eq:fmd:designconsiderations:rel}
  \frac{N_{events} \left(n\left| \psi_{n}^{a} -\psi_{n}^{b} \right| 
      > \frac{\pi}{2} \right)}{N_{total}} = \frac{e^{-\chi_{n}^{2}
      /4}}{2}\quad. 
\end{equation}

The dependence of the $v_{2}$ reconstruction error as a function of
the $v_{2}$ value is shown in
\figref{fig:fmd:designconsiderations:dependence_on_v2}

The \FMD{}{} sectors cover azimuthal angular ranges of $\Delta \phi=
18{^\circ}$ and $\Delta \phi= 9{^\circ}$, for \FMD{}{i} and \FMD{}{o},
respectively. As shown in
\figref{fig:fmd:designconsiderations:dependence_on_v2}, the uncertainty
on $v_2$ for 40 azimuthal segments is better than $\pm5\%$, while for
20 azimuthal sectors it is better than $\pm10\%$.

%% For large flow $v_{2} > 0.05$ a reconstruction precision better
%% than 10\% is achieved as shown in
%% \figref{fig:fmd:designconsiderations:dependence_on_v2}.


\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/dependence_on_v2}
  \caption{Accuracy of $v_{2}$ reconstruction as a function of $v_{2}$
    for 40 azimuthal sectors.} %
  \label{fig:fmd:designconsiderations:dependence_on_v2}
\end{figure}

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  designconsiderations tex cholm pseudorapidity IP IGB pF dN LHC
% LocalWords:  pseudorapidities JJG FMD MIP rapidities BSN HB fm Param RCF MIPs
% LocalWords:  AfterBurnerFlow minimize practice parametrization

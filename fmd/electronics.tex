%%
%% $Id: electronics.tex,v 1.60 2005-06-12 15:40:26 gulbrand Exp $
%%

\section{Electronics}
\label{sec:fmd:electronics}
\CVSKeyword{$Id: electronics.tex,v 1.60 2005-06-12 15:40:26 gulbrand Exp $}\CVSIdShow

\subsection{Overview}

In the design of the signal processing electronics for the \FMD{}{},
existing and proved solutions have been chosen whenever possible. This
philosophy has allowed us to use well--proved highly integrated
preamplifiers with multiplexed read-out (\texttt{VA1})
\cite{fmd:ideas:vaprime}, adapted to the radiation environment of
\ALICE{} and the timing of the \ALICE{} trigger and read--out system.
The existence of a fast ADC chip (\ALTRO{}) \cite{fmd:altro} developed
for the \ALICE{} \TPC{} has made it possible to perform
analog--to--digital conversion on the detector, consequently avoiding
long cables with fast analog signals. Furthermore, the adoption of the
\TPC{} analog--to--digital converter allows the use of the back--end
\TPC{} read--out electronics (\RCU{} \cite{fmd:rcu}) and results in a
significant saving in the development work of both hardware and
software related to the read--out system.

%% \enlargethispage{2ex}

\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=0.75\textwidth]{fmd/figures/electronics}
    \caption{ Architecture of the \FMD{}{} read--out electronics.  The
      \vaalice{} pre-amplifier--shaper chips are placed on the \FMD{}{}
      modules with their inputs bonded directly to the silicon
      sensors.  Multiplexed analog signals are digitised and stored by
      the \FMD{}{} digitizer cards containing \ALTRO{}
      analog--to--digital converters. The read--out and control of the
      \ALTRO{} is achieved by the \RCU{} module developed for the
      \ALICE{} \TPC{}.}
    \label{fig:fmd:electronics:architecture}
  \end{center}
\end{figure}

\Figref{fig:fmd:electronics:architecture} shows the architecture of
the \FMD{}{} read--out system starting from the silicon modules (top
left) to the data and control links going to the counting room (bottom
right).  A module assembly consisting of a silicon sensor and a hybrid
card contains the \vaalice{} pre--amplifier--shaper integrated chips
located very close to the silicon sensors.  The pre--amplifier inputs are bonded
directly to the Si strips through pitch adaptors.  After analog
storage in the \vaalice{} chips, the data is multiplexed through an analog
serial link to the \FMD{}{} digitizer card, where it is digitised in
the \ALTRO{} analog--to--digital converter chips and stored in a
digital multi--event buffer.  Further data transport uses the
Read--Out Control Unit (\RCU{}) to deliver the data into another
multi-event buffer and onward through the optical DDL link into the
\ALICE{} data acquisition system.  In addition to the read--out, the
\FMD{}{} digitizer distributes the \LVL{0} trigger
signal to the pre--amplifier--shapers and synchronizes
the read--out between the \vaalice{} and the \ALTRO{} chips.  The
\RCU{} module is the master of the \ALTRO{} read--out bus and handles
all communication upwards to the data acquisition, detector control
and trigger systems through the indicated links.  The \RCU{}
module is identical to the unit developed for the \ALICE{}
\TPC{} read--out system.

Each \vaalice{} chip has 128 independent channels of pre--amplification
and shaping circuits, followed by analog storage. The data storage is
strobed by the level zero trigger at a fixed time after the beam
interaction, common to all channels.  During the read--out, the 128
channels are multiplexed onto an analog link to a single
channel of the \ALTRO{} analog--to--digital converter, which is able to
digitise the 128 data values at the same frequency (10 MHz) at which
they arrive from the \vaalice{} chips.  Thus, each \ALTRO{} chip, with its 16
parallel ADC channels, can read out 16 \vaalice{} chips simultaneously
and store the digitized data for $16\times 128 = 2048$ strip channels in
its digital buffer. A digitizer card with three \ALTRO{} chips and a
read--out controller has enough channels to read out the 80 FE chips
for one half--ring of the \FMD{}{} detector.

The remainder of the read--out system, consisting of the \ALTRO{}
front--end bus and control network, has sufficient bandwidth to handle
the full \FMD{}{} detector with a single \RCU{}. However, in order to
limit the physical length of the front--end bus, three \RCU{} boards
and sets of optical links are envisaged for each of the read--outs of
\FMD{1}{}, \FMD{2}{} and \FMD{3}{}.
\tabref{tab:fmd:electronics:channels} summarizes the number of chips
and electronics boards needed.


%%\subsection{Front End Electronics}
\subsection{Pre--Amplifier}

\subsubsection{General Considerations}

The choice of Front--End Electronics (FEE) and detector
characteristics are closely linked. Furthermore, the timing imposed by
the \ALICE{} trigger, in particular \LVL{0}, is important for the
internal time constants of the circuit. The major issues related to
the matching of the FEE to the silicon sensors and the \ALICE{}
environment are:

\begin{enumerate}
\item Matching of the strip capacitance and detector leakage
  current to the pre--amplifier input circuit.  This relates to the
  expected signal--to--noise ratio.
\item The dynamic range of the pre--amplifier must be matched to the
  signal amplitude of the sensor for the expected range of particle
  hit densities per strip, and for the energy loss variation due to the
  particle composition and fluctuations in $dE/dx$.
\item The shaping time of the amplifier is imposed by the timing of
  the strobe generated by the \ALICE{} \LVL{0} trigger relative to the event
  time.
\item Matching of the electronics layout and the number of channels to
  the strip layout.
\item Resistance of the circuitry to radiation.
\end{enumerate}

\begin{table}[htb]
  \begin{center}
    \caption{The total number of strip channels per ring and the
      number of front--end pre--amplifier chips (128 channels per
      chip). Also shown is the number of \ALTRO{} read--out chips for
      each ring and the number of \FMD{}{} digitizer cards, assuming
      three \ALTRO{} chips per card. One Read--out Controller Unit
      (\RCU{}) is foreseen for each of the three \FMD{}{}
      sub-detectors.}
    \label{tab:fmd:electronics:channels}
    \vglue0.2cm
    \begin{tabular}{|l|c|c|c|c|c|}
      \hline
      &
      \textbf{FE Channels} &
      \textbf{FE Chips} &
      \textbf{\ALTRO{}} &
      \textbf{\FMD{}{} } &
      \textbf{\RCU{}}\\
      &
      &
      &
      \textbf{chips} &
      \textbf{Digitizers} &
      \textbf{modules}\\
      \hline
      \FMD{1}{}  & 10\,240 & \phantom{0}80 & \phantom{0}6 & \phantom{0}2  & 1\\
      \FMD{2}{i} & 10\,240 & \phantom{0}80 & \phantom{0}6 & \phantom{0}2  & 1\\
      \FMD{2}{o} & 10\,240 & \phantom{0}80 & \phantom{0}6 & \phantom{0}2  & \\
      \FMD{3}{i} & 10\,240 & \phantom{0}80 & \phantom{0}6 & \phantom{0}2  & 1\\
      \FMD{3}{o} & 10\,240 & \phantom{0}80 & \phantom{0}6 & \phantom{0}2  & \\
      \hline
      Total system &51\,200 & 400 & 30 & 10 & 3\\
      \hline
    \end{tabular}
  \end{center}
\end{table}


The signal--to--noise ratio is related to the strip capacitance, the
detector leakage current and the pre--amplifier characteristics. For a
\unit[300]{${\mu}m$} thick silicon strip detector, the capacitance
presented to the pre--amplifier through a \unit[5]{cm} long and
\unit[0.5]{mm} wide strip is about \unit[25]{pF} and the number of
electron--hole pairs generated on average by a single MIP particle is
about 22\,400 e$^{-}$, corresponding to \unit[3.6]{fC} of charge. The aim
is to keep the signal--to--noise ratio well above 10 for a single MIP
particle. This requirement means the pre--amplifier noise must be
below 2000 e$^{-}$ for a detector capacitance of \unit[25]{pF}.


Most existing pre--amplifier circuits for silicon strip detectors have
been designed to detect a charge equivalent to a few MIPs.  For the
\FMD{}{} the expected maximum number of charged particles hitting a
single strip is three, but slow particles (e.g., $\alpha$) may deposit
significantly larger energies.  Consequently, the \FMD{}{} electronics
must be able to measure a considerably higher charge deposition. We
have chosen to require an effective dynamic range of the pre--amplifier
from about 0.1 MIP to about 10 times the expected average energy
deposition per strip in central \PbPbCol{} collisions i.e., about
20 MIPs.  The lower limit assures a good signal--to--noise ratio
even for single particle hits. The higher limit is set in order to
accommodate fluctuations both in the number of particles and their
energy deposition around their averages, and to keep some safety margin
for adverse background or unexpected physics situations.

In the chosen architecture, the shaping time must be selected so that the
amplified signal peaks shortly after the arrival of the strobe. In
\ALICE{}, this time is \unit[1.2--1.3]{$\mu$s} after the beam
crossing.  It should be kept in mind that if the shaping time is
comparable to the average spacing between events, energy signals will
be corrupted by overlapping events.  For \PbPbCol{} running, the
expected event rate is about \unit[$8\times10^{3}$]{Hz}. Thus, the
average time between events is in the range of \unit[100]{$\mu$s} and
a shaping time in the microsecond range will not generate many
overlapping events.  For \ppCol{} or high luminosity light ion
(\ArArCol{}) running, however, the luminosity is considerably larger
and the event rate expected in \ALICE{} is \unit[$2\times10^{5}$]{Hz},
implying an average time between events down to \unit[5]{$\mu$s}. Due
to the low multiplicity, the issue of overlapping signals is still not
much of a problem in \ppCol{}, while in \ArArCol{} it may be necessary
to veto close events to get pulse height measurements.


While each strip channel must be seen directly by a relatively fast
pre--amplification and shaping circuit, it is possible to multiplex
many channels into the subsequent read--out, provided that the
individual analog signals can be stored in an analog buffer on the
detector and the read--out does not have excessive dead--time.

Front--end electronics located close to the \LHC{} beam pipe must be
radiation hard. Recent estimates made by the \ALICE{} collaboration
indicate that the integrated 10--year dose is about \unit[3300]{Gy}
(\unit[330]{krads}) at the inner radius of \FMD{1}{}, decreasing to
below \unit[200]{Gy} (\unit[20]{krads}) at the outer radius of
\FMD{2}{} and \FMD{3}{} \cite{fmd:radiation}.

The specifications for the FE electronics are summarized in
\tabref{tab:fmd:electronics:fe_specs}.

\begin{table}[htbp]
  \centering
  \caption{Specifications for the FE electronics}
  \label{tab:fmd:electronics:fe_specs}
  \vglue0.2cm
  \begin{tabular}{|ll|}
    \hline
    Radiation hardness:   & ${\geq}$\unit[5000]{Gy}(\unit[500]{krad})\\
    Peaking time:         & \unit[1.2--1.5]{${\mu}$s}, adjustable \\
    Noise:                & ${\leq}$ \unit[0.1]{MIPs} (2,200~$e^{-}$)\\
    Capacitance matching: & \unit[5--25]{pF}\\
    Dynamic range:        & \unit[0--20]{MIPs}\\
    Test and calibration circuits & yes\\
    Moderate power consumption:   & ${\leq}$\unit[1]{mW/channel}\\
    Early prototype version for detector testing & yes\\
    Affordable cost in relatively small quantities & yes\\
    \hline
  \end{tabular}
\end{table}


\subsubsection{The \vaalice{} Pre--Amplifier--Shaper}

The Viking Architecture (VA) family of pre--amplifiers
\cite{fmd:ideas:vaprime} has a long history as amplifiers for silicon
detectors. It is characterized by a high level of integration, low
noise and low power requirements.  The VA pre--amplifiers exist in a
variety of versions based on a common architecture and amplifier
design, but with different channel count, shaping times, capacitance
matching, chip technology, etc.  For the \FMD{}{}, we have based the
design on a slightly modified version of the \vaprime{}, a
128--channel chip made in \unit[0.35]{$\mu$m} AMS technology and
proved to resist radiation well beyond \unit[1]{Mrad}
\cite{fmd:belle:va}.
%% \enlargethispage{4ex}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.6\textwidth]{fmd/figures/va1_prime2-arch}
  %% \includegraphics[width=0.45\textwidth]{fmd/figures/va1_prime2-time}
  \caption{The \vaalice{} architecture.}
  \label{fig:fmd:electronics:va_arch}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.6\textwidth]{fmd/figures/va1_prime2-time}
  \caption{The \vaalice{} normal read--out sequence.}
  \label{fig:fmd:electronics:va_time}
\end{figure}

\Figref{fig:fmd:electronics:va_arch} shows the architecture of the
\vaalice{} \cite{fmd:ideas:vaalice} chip.  The chip consists of 128
identical charge--sensitive amplifiers, each with a pre--amplifier,
shaper and sample--hold circuit strobed by a common `hold' signal. The
outputs of all amplifiers enter a 128--channel analog multiplexer
controlled by a 128--cell bit register, which is used to direct each
of the sample/hold values one at a time to the same differential
output buffer.  One bit in the register moves through the channels in
sequence by clocking the \texttt{ckb} input while the
\texttt{shift\_in\_b} is held up. A normal read--out sequence is
displayed in \figref{fig:fmd:electronics:va_time}. Each
of the 128 analog values are transferred onto the output line
(\texttt{Ana. Out}), synchronized by the input clock (\texttt{ckb}).
The \unit[$5\times 6$]{mm${}^2$} chip shown in
\figref{fig:fmd:electronics:va} features a single calibration charge
input and external biases to adjust the shaper parameters within a
limited range.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\textwidth]{fmd/figures/VA_chip}
  \caption{The \vaalice{} pre--amplifier chip. The input pads of the 128
    channels are visible along the left side of the chip, while control
    and output lines are placed along the right--hand side.}
  \label{fig:fmd:electronics:va}
\end{figure}

The existing \vaprime{} fulfils most of the \FMD{}{} requirements,
except for the shaping time, which is too short. For this reason, we
have a contract with the IDEAS company to modify the design to our
specifications and produce a new \vaalice{} chip with a peaking time
of \unit[1.2--1.5]{$\mu$s} and improved leakage current compensation.
\tabref{tab:fmd:electronics:va_specs} summarizes the specifications of
this new amplifier chip, with the expected noise performance and power
consumption calculated by IDEAS simulations.
\Figref{fig:fmd:electronics:va_noise} shows the calculated noise
performance versus capacitance with a realistic value for the leakage current.
\tabref{tab:fmd:electronics:controllines} gives a list of the power,
control and output pads that must be connected on the \vaalice{} chips.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.75\textwidth]{fmd/figures/va_noise}
  \caption{ENC noise versus detector load for the \vaalice{} chip.
    The leakage current was \unit[3]{nA} and the peaking time was
    set at \unit[1.35]{$\mu$s}.}
  \label{fig:fmd:electronics:va_noise}
\end{figure}
\begin{table}[htb]
  \centering
  \caption{Specifications of the \vaalice{} chip}
  \label{tab:fmd:electronics:va_specs}
  \let\\\tablenewline %%\def\baselinestretch{1}
  \vglue0.2cm
  \begin{tabular}[t]{|l|ll|}
    \hline
    Process        & \multicolumn{2}{l|}{\unit[0.35]{$\mu$m} N--well
      CMOS, double--poly, triple metal} \\
    Die size       & \multicolumn{2}{l|}{\unit[4.95]{mm} $\times$
      \unit[6.12]{mm},  thickness: $\sim$\unit[725]{$\mu$m}}\\
    Input bonding pads & \multicolumn{2}{l|}{Four rows}\\
    & Pad size:  & \unit[50]{$\mu$m} $\times$ \unit[90]{$\mu$m} \\
    & Pad pitch: & \unit[91.2]{$\mu$m} \\
    & Row pitch: & \unit[170]{$\mu$m} \\
    Output pads    & \multicolumn{2}{l|}{Single row} \\
    & Pad size:  & \unit[90]{$\mu$m} $\times$ \unit[90]{$\mu$m} \\
    & Pad pitch: & \unit[200]{$\mu$m} \\
    Channels       & \multicolumn{2}{l|}{128 per chip} \\
    Power rails    & Vdd: &  \unit[+1.3]{V}\\
    & Vss: & \unit[-2.0]{V} \\
    Currents drawn & dvdd: & $<$\unit[10]{$\mu$A} \\
    &  dvss: & $<$\unit[10]{$\mu$A} \\
    &  avdd: &    \unit[11.5]{mA} \\
    &  avss: &    \unit[-73]{mA} \\
    &  gnd:  &    \unit[61.5]{mA} \\
    Peaking time   & Nominal:    & $\sim$\unit[1.35]{$\mu$s} \\
    & Adjustable: &
    $\sim$\unit[1.2]{$\mu$s}--\unit[1.5]{$\mu$s} minimum \\
    Power dissipation & Quiescent: & \unit[80]{mW} per chip \\
    &            & \unit[650]{$\mu$W} per channel \\
    Input stage    & \multicolumn{2}{l|}{PMOS referenced to gnd, input potential:
      $\sim$\unit[-1.2]{V}--\unit[-1.3]{V}}  \\
    Gain           & \multicolumn{2}{l|}{Differential current gain about $\pm$\unit[10]{$\mu$A/fC} at \unit[1]{MIP}} \\
    Linear range   & \multicolumn{2}{l|}{minimum $\pm$\unit[10]{MIP}, or \unit[0--20]{MIP} single polarity} \\
    Noise          & \multicolumn{2}{l|}{$\sim240 + 6/$\unit{pF} for \unit[1.35]{$\mu$s} peaking time (calculated)} \\
    Read--out       & \multicolumn{2}{l|}{Controlled via 128--bit shift register} \\
    & \multicolumn{2}{l|}{Max. read--out speed: \unit[10]{MHz}} \\
    Calibration/test & \multicolumn{2}{p{.6\textwidth}|}{Voltage step applied via external \unit[1.8]{pF} capacitor,
      \unit[2]{mV} step
      represents \unit[1]{MIP}
      (=\unit[22400]{e$^-$} or \unit[3.6]{fC})} \\
    \hline
  \end{tabular}
\end{table}
  
\begin{table}[htb]
  \centering
  \caption{List of connections on the \vaalice{} chip.}
  \label{tab:fmd:electronics:controllines}
  \vglue0.2cm
  \begin{tabular}{|l|l|l|l|}
    \hline
    \textbf{Signal name}  & \textbf{Type} & \textbf{Description} & \textbf{Nominal value} \\
    \hline
    \texttt{gnd }       & power         &       signal ground                   & $\unit[\phantom{+}0.0]{V}$   \\
    \texttt{dvdd }      & power         &       digital \texttt{vdd}            & $\unit[+1.3]{V}$        \\
    \texttt{dvss }      & power         &       digital \texttt{vss}            & $\unit[-2.0]{V}$        \\
    \texttt{avss }      & power         &       analog \texttt{vss}             & $\unit[-2.0]{V}$        \\
    \texttt{avdd }      & power         &       analog \texttt{vdd}             & $\unit[+1.3]{V}$        \\
    \texttt{holdb }     & digital in    &       hold analog data                & logical       \\
    \texttt{shift\_in\_b }& digital in  &       start pulse for read--out       & logical       \\
    \texttt{ckb }       & digital in    &       clock for read--out register    & logical       \\
    \texttt{dreset }    & digital in    &       reset of digital part           & logical       \\
    \texttt{test\_on }  & digital in    &       turns chip into test mode (calibration) & logical       \\
    \texttt{cal }       & analog in     &       test input signal               & \unit[1]{MIP}         \\
    \texttt{outm }      & analog out    &       negative output signal (current)& varies      \\
    \texttt{outp }      & analog out    &       positive output signal (current)& varies      \\
    \texttt{pre\_bias } & analog in     &       bias current for pre--amplifiers & $\unit[500]{\mu A}$    \\
    \texttt{sha\_bias } & analog in     &       bias current for shaper amplifiers& $\unit[\phantom{0}22]{\mu A}$   \\
    \texttt{ibuf }      & analog in     &       bias current for output buffer  & $\unit[140]{\mu A}$    \\
    \texttt{vfs }       & analog in     &       control voltage for shaper      & $\unit[+0.7]{V}$        \\
    \texttt{vfp }       & analog in     &       control voltage for pre--amp.   & $\unit[-0.2]{V}$        \\
    \hline
  \end{tabular}
\end{table}

\subsection{Hybrid Cards}

The hybrid cards hold the front--end pre--amplifier chips and serve
two purposes: mechanical support for the Si sensors and printed
circuit board for the pre--amplifier circuitry.

Locating the pre--amplifiers as close to the Si sensor as possible
is essential to avoid degradation of the signal--to--noise ratio from
pickup on the transfer lines. For this reason, the \vaalice{} chips
will be mounted on hybrid boards, to which the silicon sensor itself
will be directly attached and the strips bonded.  A hybrid board has
essentially the same shape but slightly smaller size compared to the
Si sensor, and the assembly of sensor and hybrid card constitutes
an independent detector unit, an \FMD{}{} \emph{module}.
\Figref{fig:fmd:electronics:hybridlayout} shows an inner and an outer
module, seen from the hybrid side with the Si sensors below.

The hybrid substrate will be  a ``sandwich'' of a
\unit[0.5-1.0]{mm} thick ceramic plate on the implant side of the
silicon sensor and a multilayer printed circuit board made of halogen--free
FR4. The flatness of the ceramic material assures a good gluing
surface for the brittle silicon and allows the back side of the FR4 to
be a fully metallised electrical ground plane.

Electrically, the purpose of the hybrid cards is to support the 
necessary number of
pre--amplifier chips and distribute their services (power
distribution, bias voltages, test and calibration circuitry,
read--out lines and cable connectors). Power regulators, read--out
logic, and other electronic components that need not be placed in
the immediate neighbourhood of the front-end chips will not be placed on the
hybrid card, but rather on the \FMD{}{} digitizer card. Wherever
possible, the electric signal lines are placed on the FR4 circuit
of the hybrid card.  However, due to the small pitch of the input
pads of the \vaalice{} chip, it will be necessary to have a
special pitch adaptor to fan in the signals from the
\unit[250]{$\mu$m} (and \unit[500]{$\mu$m}) pitch of strips in the inner
(and outer) sensors to the \unit[45]{$\mu$m} pitch of the \vaalice{}
input pads. This pitch adaptor will be made on a ceramic
substrate and glued to the FR4 substrate in front of each \vaalice{}
chip.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.9\textwidth]{fmd/figures/hybrids}
    \includegraphics[width=.7\textwidth]{fmd/figures/bonding_section}
    \caption{Layout of the hybrid cards showing the main
      components on an inner (left) and an outer module (right).  The
      \vaalice{} chips and pitch adaptors are placed along the sides
      of the hybrid, directly opposite the bonding pads of the silicon
      sensor strips. The cable connectors are located near the upper edge.
      Also shown (bottom) is the cross sectional view of the inner
      wafer at C-C. The bonding wires from the pads of the silicon sensor
      strips to the pitch adaptor are illustrated; the bonding of the 
      \vaalice{} chip is also shown.}
    \label{fig:fmd:electronics:hybridlayout}
  \end{center}
\end{figure}

The main components to be found on the hybrid
card are displayed in \figref{fig:fmd:electronics:hybridlayout} and
are listed below.

\begin{enumerate}
\item \vaalice{} chips, described in the previous section. Inner
  modules will hold eight chips, four on each edge of the hybrid; outer
  modules will hold only four chips, two on each edge.
\item Pitch adaptors are thin ceramic circuits about
  \unit[4]{mm}$\times$\unit[32]{mm} size, used to fan in the signals
  from a pitch of \unit[250]{$\mu$m} to a pitch of \unit[45]{$\mu$m}.
\item Bonds from the pads on the silicon sensors to the pitch
  adaptors, from the pitch adaptors to the \vaalice{} input pads, and
  from the \vaalice{} output and control pads to the FR4 substrate.
\item Connectors for low voltage power, and control and read--out
  lines for the \vaalice{}.  Short cables will go from these lines to the
  digitizer boards.  \Figref{fig:fmd:electronics:hybridlayout} also
  shows a connector receiving the bias voltage for the silicon sensor
  which will be connected directly to an individual channel of a HV
  power supply placed outside the \LTHREE{} magnet. A short cable will
  connect the bias voltage from the hybrid to the back of the sensor.
\item Circuitry and passive components (not shown in
  \figref{fig:fmd:electronics:hybridlayout}) distributing power,
  control and readout lines from the \vaalice{} chips to the
  connectors. All chips will be read out in parallel synchronously.
\end{enumerate}

\subsection{\FMD{}{} Digitizer Cards}

It has been found to be advantageous to digitise signals close to
the detector and store the digital information in a multi--event
buffer before transfer to the DAQ.  This digitization of the data
synchronously with the read--out from the \vaalice{} chips is the main
function of the \FMD{}{} Digitizer (\FMD{}{}D) boards and is done by
three \ALTRO{} analog--to--digital converter chips on each board. The
boards also contain the power regulation and bias setting for the
hybrid cards and their \vaalice{} chips. Many of the
\FMD{}{}D details, relating to the \ALTRO{}s and the communication buses to
the \RCU{} module can be taken from the \TPC{} Front End Card
(FEC) design, while the \vaalice{} services are specific to the
\FMD{}{}D.

Each digitizer card will service the detector modules of one
half--ring (40 \vaalice{} chips) and be placed on the rear side of the
honeycomb support plate of the half--ring within a few tens of
centimetres of all the hybrid cards. Thus a half--ring becomes a
functional unit of the read--out up to and including the multi-event
buffers on these cards.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.9\textwidth]{fmd/figures/digitizer}
  \caption{Schematic layout of the \FMD{}{} digitizer card}
  \label{fig:fmd:electronics:digitizer}
\end{figure}

\Figref{fig:fmd:electronics:digitizer} shows the functional layout of
a \FMD{}{} digitizer card. The signals are digitized by
the \ALTRO{} chips designed for the \ALICE{} \TPC{} read--out in
\unit[0.25]{$\mu$m} IBM radiation--hard technology.  The \ALTRO{}
contains 16 independent channels of 10--bit ADCs, each capable of
sampling the input at a rate of \unit[10]{MHz}, followed by various
digital processing capabilities and a multi-event result buffer.  For
the \FMD{}{}, only the ADC functionality and digital output buffer is
essential, while baseline subtraction and zero--suppression
will be optional, possibly depending on the finally observed strip hit
rate.

The read-out controller is the same FPGA that will control the 
initialization and read-out sequence of the \ALTRO{}s. On the \FMD{}{}D,
the added functionality is to receive and distribute the \LVL{0} trigger
signal used to freeze the analog values in the \vaalice{}
chips and, in case of a positive subsequent \LVL{1} trigger, 
to issue a read-out sequence as the one shown in 
\figref{fig:fmd:electronics:va_arch} which is synchronized to the same
clock as the \ALTRO{} digitization circuit.
\enlargethispage{4ex}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.6\textwidth]{fmd/figures/altro_test}
  \caption{Test set--up of \ALTRO{} read--out at NBI.  The box on the
    right contains a Si detector and a \vaprime{} pre--amplifier chip.
    The large print--card on the left is an \ALTRO{} test--board.  The
    smaller card in the middle is a custom--built clock and
    synchronization card.  Data is read out into a PC running
    LabVIEW.}
  \label{fig:fmd:electronics:altro_test}
\end{figure}

\Figref{fig:fmd:electronics:altro_test} shows an early prototype
set--up to test the synchronization of the VA multiplexing and the
ALTRO digitization clocks. An \ALTRO{} test card \cite{fmd:altrotest}
is used with a synchronization module based on FPGA logic and a Si
sensor with a \vaprime{} chip.

\subsection{Readout Control Unit (\RCU{})}

The Read--out Control Units (see \figref{fig:fmd:electronics:rcu})
interface the digitizer cards to the Data Acquisition System (DAQ),
the Timing and Trigger System (TTC), and the Detector Control System
(DCS).  They are responsible for controlling the event data read--out
of the \FMD{}{} and for initializing and monitoring the digitizer
cards. The units are identical to those developed for the \TPC{},
and apart from configuration data and custom routines, the software
will also be identical.  \Figref{fig:fmd:electronics:rcuphoto} shows a
photo of the \RCU{} board in a \TPC{} setup.
 
Three units will be used to interface the 10 digitizer cards. The use
of one \RCU{} per \FMD{}{} ring system guarantees that the length of
the buses between the \RCU{} and the corresponding digitizer cards
remain below $\sim$\unit[3]{m}.  Two separate buses --- for data and
controls, respectively --- provide the interface to the front-end
electronics on flat, flexible cables.

The \RCU{} board design is based on a single FPGA containing the logic
and on-board memory. It is interfaced to the various links
communicating with the external systems.  The interfacing to the
Trigger and the DAQ systems follows the standard architecture of
\ALICE{}, using the optical TTC and Detector Data Links (DDL),
respectively.  The interface to the DCS system is through an Ethernet
connection in the current design, although Profibus is still an
option.

\LVL{1} and \LVL{2} triggers with event number and timing information
are received from the TTC interface and used by the \RCU{} to initiate
the event read-out.  The \RCU{} collects event data from the \ALTRO{}
chips on the digitizer cards, assembles a sub-event, reformats the
data if necessary, and transfers the event to the DAQ via the optical,
full duplex \ALICE{} Detector Data Link (DDL). A multi--event buffer is
implemented in the FPGA memory.

A dedicated bus between the \RCU{} and the digitizers is used for
voltage, current and temperature monitoring of the digitizers. At the
same time, the \RCU{} is the supervisor for the \ALTRO{} chips and 
handles the configuration of the \ALTRO{} chips at start-up or in case
of errors. Error conditions can be transmitted to the DCS system
through the DCS interface.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.9\textwidth]{fmd/figures/rcu}
  \caption{Schematic layout of the \RCU{} card.  The card is identical to the
    \TPC{} \RCU{} card. Two buses, for read-out and control purposes
    respectively, connect to the digitizer cards controlled by the
    module. The \RCU{} provides separate interfaces to the DAQ
    (DDL), DCS (Ethernet or Profibus) and the Trigger and Timing Control 
    (TTC).}
  \label{fig:fmd:electronics:rcu}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.9\textwidth]{fmd/figures/RCUphoto}
  \caption{Photograph of the \TPC{} \RCU{} card and the two parallel bus
    connections to a \TPC{} Front-End Card (FEC) used for read-out
    and control respectively.  For the \FMD{}{}, the FECs are replaced
    by the digitizer cards and the backplanes are replaced by flexible cables. The
    interfaces to Trigger, DCS and DAQ are placed on mezzanine cards
    while the front--end buses and the board controller FPGA are
    located on the motherboard.}
  \label{fig:fmd:electronics:rcuphoto}
\end{figure}


\subsection{Power Consumption}

The \vaalice{} chips consume about \unit[80]{mW} each. A detector
module will thus dissipate about \unit[0.6]{W} in the inner ring and
\unit[0.3]{W} in the outer ring, which adds up to
\unit[6]{W} for a full ring. To estimate the total power budget, we
assume a similar consumption from the rest of the circuitry on the
hybrid and digitizer boards, arriving at a total power
requirement of \unit[12]{W} for each fully equipped ring.


%% We do this to keep all the figures in this section. 
%% \clearpage 

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  tex cholm DDL LVL pre dE dx MIP pC pF ns JJG LHC Gy krads SPD mW
% LocalWords:  SSD krad Mrad ckb nA BSN poly Vdd Vss dvdd dvss avdd mA avss gnd
% LocalWords:  PMOS fC mV vdd vss holdb dreset outm outp sha ibuf vfs vfp HV
% LocalWords:  pre--amplifiers DAQ FEC ADC's ALTRO FPGA NBI LabView IGB TPC MIPs
% LocalWords:  digitizer digitizers

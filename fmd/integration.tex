%%
%% Id: integration.tex,v 1.19 2004/04/28 13:33:18 cholm Exp $
%%
\section{Integration in \ALICE{}}
\CVSKeyword{$Id: integration.tex,v 1.52 2005-06-12 15:40:27 gulbrand Exp $}\CVSIdShow

\subsection{Mechanical Support for \FMD{}{} }

The three \FMD{}{} sub--detectors require different mechanical support
systems.

The most complex support system is on the \RB{26} side, where the
support structure has the form of a cone. This cone must not only
support the \FMD{3}{} inner and outer detector rings but also the
beam pipe, via a wire and collar arrangement. A design study has
been performed based on carbon fibre materials. The drawings of
the cone are shown in \figref{fig:fmd:mechanical:fmdcones1}.

The weight of each ring, including the honeycomb support plate and
cables, is estimated to be, at most, \unit[4]{kg}. The \ALICE{}
vacuum chamber group has specified that the four wires holding the beam
tube must be tightened with a force of up to \unit[100]{N} each.  The
cone will be manufactured from carbon fibre.  Based on this, a
finite element simulation of stresses and deformations of the proposed
support structure has been carried out.  The proposed arrangement
ensures that the cone does not experience deformations exceeding
\unit[20]{$\mu{}$m} at any point (see
\figsref{fig:fmd:mechanical:displacment} and
\ref{fig:fmd:mechanical:displacment2}).  This is sufficient for the
positioning (and repositioning) of the \FMD{}{}.

The design of the support structures for \FMD{1}{} and \FMD{2}{} has
not yet been finalised. A possible layout for \FMD{2}{} is shown
in \figref{fig:fmd:designconsiderations:forwardlayout2}. It is based
on a cylindrical aluminium cylinder attached to the \ITS{}/\TPC{}
support structure. Apart from holding \FMD{2}{}, this cylinder also
provides the attachment for the four wires holding the vacuum tube behind
\FMD{2}{}.

A preliminary drawing of the support of \FMD{1}{} is shown in
\figref{fig:fmd:designconsiderations:forwardlayout3}. The \FMD{1}{}
discs are mounted on a carbon fibre wheel structure supported in a
cantilever fashion from the vacuum valve behind \TZERO{A}. This
support structure will also hold \VZERO{A}, \TZERO{A} and one point of
the beam pipe.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.9\textwidth]{fmd/figures/Cone_eng}
  \caption{Preliminary technical drawing of the \FMD{3}{} cone}
  \label{fig:fmd:mechanical:fmdcones1}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/displ_tot_back}
  \caption{Calculated displacement of the \FMD{3}{} support cone under
    the load of the detector and the tension wires of the beam pipe, as
    seen from the back. The cone is split in two halves horizontally,
    while the vertical plane is a symmetry plane in the calculation.}
  \label{fig:fmd:mechanical:displacment}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/displ_cyl_0}
  \caption{Calculated displacement of the nose of the \FMD{3}{}
    support cone. The biggest stress is where the tensioning wires for
    the beam pipe pull the cylindrical part of the structure.}
  \label{fig:fmd:mechanical:displacment2}
\end{figure}


%\begin{figure}[htbp]
%  \centering
%  \FIXME[B\o{}rge]{Need these figures}
%  %%  \includegraphics[width=0.9\textwidth]{fmd/figures/Cone_eng}
%  \caption{Schematic of the \FMD{2}{} support structure}
%  \label{fig:fmd:mechanical:fmdcones2}
%\end{figure}

\iffalse
\begin{figure}[htbp]
  \centering
  \FIXME[B\o{}rge]{I think nothing can be put in in time here}
  %%  \includegraphics[width=0.9\textwidth]{fmd/figures/Cone_eng}
  \caption{Schematic of the  \FMD{1}{} support structure}
  \label{fig:fmd:mechanical:fmdcones3}
\end{figure}
\fi

The sequence of installation steps of the forward detectors and the
\ITS{} is complex and will be covered for all \FWD{} in a
separate section.

\subsection{Cabling}

\tabref{tab:fmd:integration:cables} gives an overview of the cables
required to power, control and read out the \FMD{}{} system for each
of the five rings, \FMD{3}{i}, \FMD{3}{o}, \FMD{2}{i}, \FMD{2}{o} and
\FMD{1}{}.

\begin{table}[htbp]
  \centering
  \caption{List of cables for each of the \FMD{}{} rings, showing the
  number of cables with the number of wires in each cable in parentheses.}
  \label{tab:fmd:integration:cables}
  \vglue0.2cm
  \begin{tabular}{|l|ccccc|}
    \hline
    \textbf{Cable}   & \FMD{3}{i} & \FMD{3}{o} & \FMD{2}{i} &\FMD{2}{o} &\FMD{1}{} \\
    \hline
    \multicolumn{6}{|l|}{\em Between digitizers and hybrids:} \\
    \hline
    Hybrid power     &$10(\times4)$&$20(\times4)$&$10(\times4)$&$20(\times4)$&$10(\times4)$\\
    Hybrid control   &$10(\times6)$&$20(\times6)$&$10(\times6)$&$20(\times6)$&$10(\times6)$\\
    Hybrid data      &$10(\times16)$&$20(\times8)$&$10(\times16)$&$20(\times8)$&$10(\times16)$\\
    \hline
    \multicolumn{6}{|l|}{\em Between digitizers and \RCU{}s:} \\
    \hline
    Data bus         &\multicolumn{2}{l}{$1(\times50)$}&\multicolumn{2}{l}{$1(\times50)$}&$1(\times50)$\\
    Control bus      &\multicolumn{2}{l}{$1(\times26)$}&\multicolumn{2}{l}{$2(\times26)$}&$1(\times26)$\\
    \hline
    \multicolumn{6}{|l|}{\em Between detectors and racks in UX25:} \\
    \hline
    Si bias voltage   &$10(\times1)$&$20(\times1)$&$10(\times1)$&$20(\times1)$&$10(\times1)$\\
    Digitizer power   &$2(\times12)$&$2(\times12)$&$2(\times12)$&$2(\times12)$&$2(\times12)$\\
    Digitizer control &$2(\times4)$ &$2(\times4)$&$2(\times4)$&$2(\times4)$&$2(\times4)$\\
    Digitizer \LVL{0}    &$1(\times1)$ &$1(\times1)$&$1(\times1)$&$1(\times1)$&$1(\times1)$\\
    \hline
    \multicolumn{6}{|l|}{\em Between \RCU{}s and racks in UX25 or counting rooms:} \\
    \hline
    \RCU{} power     &\multicolumn{2}c{$2(\times4)$}&\multicolumn{2}c{$2(\times4)$}&$2(\times4)$\\
    \RCU{} controls  &\multicolumn{2}c{1(Ethernet)}&\multicolumn{2}c{1(Ethernet)}&1(Ethernet)\\
    \RCU{} TTC       &\multicolumn{2}c{1(TTC)}&\multicolumn{2}c{1(TTC)}&  1(TTC)\\
    \RCU{} DDL       &\multicolumn{2}c{1(DDL)}&\multicolumn{2}c{1(DDL)}&
    1(DDL)\\
    \hline
  \end{tabular}
\end{table}

The \vaalice{} chips on the hybrid cards need a connection to the
digitizer card with six electrical lines for controls, plus one
differential analog data output connection and four power and ground
lines. The control and power lines can be shared between VA chips on
the same hybrid card, but the data output lines are individual for
each chip.  These connections will be made by short, flexible
multiwire cables between each hybrid card and the digitizer card on
the half--ring.

Low voltage power to the hybrids is brought in per half--ring through
the digitizer card, where it is regulated and distributed to each of
the hybrids. In addition, each Si sensor will be supplied with a bias
voltage input ($\approx$\unit[100]{V}). These will be routed directly from
the power supply channels to each sensor through the hybrid card.

Between digitizer cards and the \RCU{}s, separate data and control
buses will provide communication on flexible multiwire flat
cables. Up to four digitizer boards (for \FMD{2}{} and \FMD{3}{}) and the
corresponding \RCU{} are connected together on one data and one
control bus, with the \RCU{} placed about \unit[3]{m} away (just behind
the muon absorber on the \FMD{3}{} side). The digitizer cards also
need cables for the low voltage power, the \LVL{0} trigger signals and a
separate JTAG connection.

After the \RCU{}, the data stream is optical (DDL), the controls
network is based on Ethernet, and the trigger information is carried
by the TTC system.  The total system contains three \RCU{}s, each with a
separate DDL link. The \RCU{}s will be powered separately.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.8\textwidth]{fmd/figures/ITS_MockUp_3}
  \caption{Photo of a 1:1 scale model of the \ITS{} and \FMD{}{} used
    to study aspects of the mechanical installation and cabling.  The
    space available for patch panels is represented by white blocks.}
  \label{fig:fmd:integration:mockup}
\end{figure}

The layout of the cable paths from the detector to the outside world
is covered in more detail in \secref{cha:integration}.  In order to be
able to assemble and disassemble \ALICE{}, all cables from the
detectors past the muon absorber must be connected through a patch
panel at the indicated position. \Figref{fig:fmd:integration:mockup}
shows a photo of a 1:1 scale model of the \ITS{} and \FMD{3}{} on the
muon absorber side, with some cables mounted in a preliminary
configuration.  The cable layout on the \RB{24} side has not yet been
studied to the same level of detail. However, in
\figref{fig:fmd:designconsiderations:forwardlayout3}, the foreseen
cable path from \FMD{2}{} to the end of the \TPC{} is indicated. It
follows the cable ducts that mainly serve the \ITS{} on the \RB{24}
side.


\subsection{Cooling}

The Front End electronics is cooled by a directed flow of dry air
inside the volume of the \FMD{}{} detector. At present the temperature
conditions at the \FMD{}{} positions have not yet been resolved in
detail, but preliminary simulations indicate a very elevated temperature
in the area ($\approx\unit[75]{{}^{\circ}C}$), which will require
additional work on the air flow system from the \ALICE{}
infrastructure group. If necessary the \FMD{}{} FE electronics boards
can be equipped with liquid cooling.

\subsection{Power Supplies}

Each \FMD{}{} sensor will be supplied with a separate positive bias
voltage in the range \unit[70--150]{V}. Positive and negative low
voltages will be supplied to the digitizers and \RCU{} modules while
the hybrid cards are supplied through power regulators on the
digitizer boards, as described above.  Remotely controlled power
supplies with current monitoring capabilities for both high and low
voltages will be of a standard type, presumably a CAEN 1527 system or
similar.  The power supplies will be mounted in racks outside the
\LTHREE{} magnet (see \secref{sec:fmd:integration:racks}), and
controlled by the DCS system, which already contains pieces of code
for the desired functionality.

\subsection{Racks}
\label{sec:fmd:integration:racks}

Two racks (B2--16 and B2--27) have been reserved for \FMD{}{}
electronics, see \figref{fig:fmd:integration:racks}. These will
contain low voltage power supplies and trigger related
electronics. High voltage power supplies will presumably be placed
in the counting rooms above the cavern.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/racks}
  \caption{Rack placement in the \ALICE{} cavern. Racks reserved for the
    \FMD{}{} are marked in grey.}
  \label{fig:fmd:integration:racks}
\end{figure}

\subsection{Detector Control System (DCS)}
\label{sec:fmd:integration:dcs}

The FMD Detector Control System will be fully compatible with the
\ALICE{}-wide system. As illustrated in
\figref{fig:fmd:integration:dcs}, it will consist of branches for
monitoring and controlling the high voltage and low voltage power
supplies, and separate branches for communication with the digitizer
and \RCU{} units.  All will be common solutions already devised
within \ALICE{}.

The CAEN OPCserver connection to the CAEN 1527 or similar system is
used in many other \ALICE{} subsystems. It will be used to control and
monitor each high and low voltage channel.

Communication with the \RCU{} module is based on Ethernet and the
software will be available from the \TPC{} group. With this system it
is possible to configure the \RCU{} and digitizer cards, including
downloading parameters to the \ALTRO{} and VA chips and monitoring
currents and temperatures on both \RCU{} and digitizer cards.

For direct communication to the FPGA chips on the digitizer cards, we
foresee the use of dedicated JTAG connections. 
Other \ALICE{} detectors plan to use similar connections.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/dcs}
  \caption{\FMD{}{} Detector Control System.}
  \label{fig:fmd:integration:dcs}
\end{figure}

\subsection{Data Acquisition System (DAQ)}
\label{sec:fmd:integration:daq}

Each of the three \RCU{} boards is connected to a 
Read-Out Receiver Card (RORC) \cite{intro:tdr:daq}
in the \ALICE{} DAQ system through standard DDL optical communication
links. The link is used for event data flowing from the \RCU{} to the
RORC and possibly for system configuration in the reverse direction.
As the \RCU{} modules are identical to the ones used in the \TPC{},
the essential communication software will also be copied.

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  tex cholm UX  LVL TTC DDL JTAG BSN Barberis CAEN DCS IGB
% LocalWords:  digitizer digitizers

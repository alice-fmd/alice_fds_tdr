%%
%% $Id: sirings.tex,v 1.56 2005-06-12 15:40:27 gulbrand Exp $
%%
\section{Silicon Rings}
\CVSKeyword{$Id: sirings.tex,v 1.56 2005-06-12 15:40:27 gulbrand Exp $}\CVSIdShow

\subsection{Overview}

The Forward Multiplicity Detector is composed as follows (see
\figsref{fig:fmd:sirings:numbering}--\ref{fig:fmd:si_rings:assembly}).
Each of the two \emph{types} of silicon \emph{sensors} must be cut
from \unit[300]{$\mu$m} thick silicon wafers. Each sensor is
subdivided into two azimuthal \emph{sectors}, whose active elements,
\emph{strips}, are arranged as narrow rings with the nominal beam
position as centre.  A sensor is glued onto a thin ceramic plate,
which is in turn glued to a hybrid PC board containing the
preamplifier electronics. Such a unit constitutes a detector
\emph{module}. Several modules (5 for the inner type and 10 for the
outer type) are mounted on a light support plate to form a
\emph{half--ring}. Two half--rings are attached to a support structure
and constitute a detector \emph{ring}. The five detector rings (called
\FMD{1}{}, \FMD{2}{} inner and outer and \FMD{1}{} inner and outer)
constitute the full \FMD{}{} \emph{detector}.

For identification purposes we use the following notation:
\FMD{$\{1,2,3\}$}{$\{i,o\}$}$(j,k)$, where the index runs over
sector number ($j=(0,19)$ for inner, $j=(0,39)$ for outer) and strip
number ($k=(0,511)$ for inner, $k=(0,255)$ for outer).  For example,
the 137\textsuperscript{th} strip of the 4\textsuperscript{th}
sector of the inner ring of \FMD{2}{} would be denoted
\FMD{2}{i}(4,137). Sectors are labelled counter--clockwise looking
down the beam line toward the muon absorber, starting from the
horizontal line pointing toward the \LHC{} centre.  Strips are
numbered from the smallest to the largest radius.  See also
\figref{fig:fmd:sirings:numbering}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{fmd/figures/numbering}
  \caption{Numbering scheme for the FMD strips and sectors in the
   ALICE coordinate system.}
  \label{fig:fmd:sirings:numbering}
\end{figure}

\Figref{fig:fmd:si_rings:inandoutsegment} shows how to assemble the
ring detectors from sensor segments cut out of 6~inch silicon wafers.
The figure shows how the two chosen ring geometries can be constructed
from 10 and 20 sensors, respectively.

\begin{figure}[htpb]
  \begin{center}
    %% \includegraphics[width=.33\textwidth]{fmd/figures/wafer_layout_inner}
    %% \includegraphics[width=.56\textwidth]{fmd/figures/wafer_layout_outer}
    \includegraphics[width=.9\textwidth]{fmd/figures/InnerOuterRings}
    \caption{Assembly of an inner ring from 10 modules (left) and an outer
      ring from 20 modules (right). The size and shape of each module is
      determined by limitations imposed by the fabrication of sensors from
      6~inch silicon wafers, see \figsref{fig:fmd:si_rings:innersensor} and 
      \ref{fig:fmd:si_rings:outersensor}.}
    \label{fig:fmd:si_rings:inandoutsegment}
  \end{center}
\end{figure}

\Figref{fig:fmd:si_rings:assembly} shows a photograph of a model of
the \FMD{3}{} assembly.  The silicon sensors (not fully equipped) are
mounted on flat support plates, in turn supported by the conical
structure.

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=.6\textwidth]{fmd/figures/ModelCone}
    \caption{Photograph of model showing the assembly of the \FMD{3}{}
      inner and outer silicon ring (with a partly equipped upper half)
      inside its support cone.  The cone and the detector support
      plates can be split along the horizontal axis.}
    \label{fig:fmd:si_rings:assembly}
  \end{center}
\end{figure}

\subsection{Silicon Sensors}

\Figref{fig:fmd:si_rings:sensorcut} shows a schematic cross--section
of a silicon sensor based on bulk n--type silicon with p+ type
implants. At the bottom is the aluminium high voltage connection plane
(+HV). At the top, p+ type implants are shown for each of the active
strips, for a guard ring and for a bias ring. Bias resistors connect
the active strips to the aluminium bias ring. Each p+ strip is
capacitively coupled to a corresponding aluminium strip, which is
terminated by a bonding pad near the edge of the detector, allowing
for bonding to the front--end electronics hybrid card. For quality
control purposes, a d.c.\ bonding pad with direct connection to the
strip p+ implant is also foreseen.  This sensor design follows closely
what has recently been produced for the silicon tracker of the CMS
experiment \cite{fmd:cms}, with the exception of the use of higher
resistivity bulk silicon and changes necessary due to the differences
in geometry.

\begin{figure}[htpb]
  \begin{center}
     \includegraphics[width=.9\textwidth]{fmd/figures/si_det_3d}
     \caption{Left: a three--dimensional schematic view of the corner
       of a silicon sensor, showing the placement of guard ring,
       voltage distribution, etc.  Right: a two--dimensional
       cross--section through the silicon wafer parallel to the
       direction of the active strips.}
    \label{fig:fmd:si_rings:sensorcut} %% fig:fmd:si_rings:inandoutsegment
  \end{center}
\end{figure}

The geometry of the sensors is matched to the effective size available
on a 6~inch silicon wafer. The wafers are \unit[300]{$\mu$m} thick
with a diameter of \unit[150]{mm}. Due to details of the manufacturing
process, the full area of the silicon wafers cannot be used as an
active detector.  For the chosen manufacturer (Hamamatsu Photonics
K.K.), the active sensor area is limited to lie within a fiducial
circle of diameter \unit[134]{mm}.

\figsref{fig:fmd:si_rings:innersensor}
and~\ref{fig:fmd:si_rings:outersensor} show the overall sensor
geometry for the two types, inner and outer, respectively.  The
outside radii given in
\tabref{tab:fmd:designconsiderations:etacoverage} correspond to the
maximum radial dimensions of the sensors. Due to limitations on the
useful wafer diameter, the outer corners of the sensors are cut as
indicated. This entails a loss in azimuthal coverage of the rings for
approximately 10\% of the strips at the outermost radii.

\begin{figure}[htpb]
  \begin{center}                
    \includegraphics[scale=.5]{fmd/figures/Si-geometry-134inner}
    \caption{Geometry of an inner silicon strip sensor manufactured
      from a 6 inch Si wafer. The active parts of the detector
      must stay within the circle of diameter \unit[134]{mm} for
      manufacturing reasons.  
      The active area of the wedge--shaped sensor is
      outlined with solid lines, while the slightly larger area
      indicated with the dashed line represents the physical size of
      the cut wafer, including bias and guard ring structures.
      Each sector is subdivided into two
      azimuthal sectors, each with 512 strips at constant pitch (a
      smaller number is shown for clarity).}
    \label{fig:fmd:si_rings:innersensor}
  \end{center}
\end{figure}

\begin{figure}[htpb]
  \begin{center}
    %% [scale=.707107]
    \includegraphics[scale=.5]{fmd/figures/Si-geometry-134outer}
    \caption{Geometry of an outer silicon strip sensor manufactured
      from a 6 inch Si wafer. The active parts of the detector
      must stay within the circle of diameter \unit[134]{mm} for
      manufacturing reasons.  
      The active area of the wedge--shaped sensor is
      outlined with solid lines, while the slightly larger area
      indicated with the dashed line represents the physical size of
      the cut wafer, including bias and guard ring structures.
      Each sector is subdivided into two
      azimuthal sectors, each with 256 strips at constant pitch (a
      smaller number is shown for clarity).}
    \label{fig:fmd:si_rings:outersensor}
  \end{center}
\end{figure}

The sensors are electrically subdivided into two equal azimuthal
sectors with the median as the symmetry axis. On one side of each sensor
(called the back side) a single Al contact covers the full active
surface to supply the bias voltage (in the range \unit[100--200]{V}) to
the detector. On the other side (called the implant side) the detector
is subdivided into circular strips of equal pitch, centred at the
nominal beam position.  Sectors of the inner type have 512 strips
corresponding to a pitch of approximately \unit[250]{$\mu$m}, while
the outer type sectors have 256 strips with a pitch of approximately
\unit[500]{$\mu$m}.

\tabref{tab:fmd:si_rings:sensorspecs} lists some of the specifications
for the Si sensors. The higher silicon resistivity of
\unit[5]{k$\Omega$cm} compared to the \unit[1.5--3]{k$\Omega$cm} used
for the inner CMS detectors was chosen because of the relatively low,
yet significant, radiation dose expected for the \FMD{}{} over a
10--year period \cite{fmd:radiation}. The passivation of the implant
side is a protection of this side of the sensor, which will be used as
a gluing surface to fix the sensor to its hybrid board, see
\secref{sec:fmd:simodules}. The Table lists the expected values for
initial operational parameters, such as operational voltages and
leakage currents.

\Figref{fig:fmd:si_rings:striplayout} shows a preliminary layout of
the strips near the outer edge of an inner type sensor, indicating the
geometry of the guard and bias rings, poly--silicon bias resistors,
and rectangular bonding pads near the sensor edges which will be used
both for the final bonding and near the centre line, which will be
used for testing.

\begin{longtable}{|l|p{.5\textwidth}|}%[htbp]
  %% \centering
  %% \begin{tabular}{|l|l|}
  \caption{Silicon sensor design parameters.}
  \label{tab:fmd:si_rings:sensorspecs}
  \\
  \endfirsthead
  \hline
  \endhead
  \hline
  \endfoot
  \endlastfoot
  \hline
  \multicolumn{2}{|c|}{\textbf{External parameters}}\\
  \hline
  Radiation dose (10 years)           & \unit[5\,000]{Gy} \\
  Hadron flux (10 years)              & \unit[$1\times10^{13}$]{cm${}^{-2}$}\\
  Neutron flux (10 years)             & \unit[$2\times10^{12}$]{cm${}^{-2}$}\\
  1 MeV n eq. flux (10 years)         & \unit[$3\times10^{13}$]{cm${}^{-2}$}\\
  Operational temperature             & \unit[$20^{\circ}$]{C}\\
  \hline
  \multicolumn{2}{|c|}{\textbf{Geometrical parameters}}\\
  \hline
  Wafer diameter                       & \unit[150]{mm} \\
  Effective sensor diameter            & \unit[134]{mm} \\
  Silicon thickness                    & 310 $\pm$ \unit[10]{$\mu$m} \\
  Number of radial strips              & 512 (inner) or 256 (outer) \\
  Strip pitch                          & \unit[250]{$\mu$m}
  (inner)or \unit[500]{$\mu$m} (outer) \\
  Strip length                         & 13--\unit[50]{mm}
  (inner) or
                                          24--\unit[42]{mm} (outer)\\
  Guard and biasing ring width         & $\sim$\unit[1]{mm} \\
  Dimension of bonding pads            & $\sim$ \unit[100]{$\mu$m}$\times$\unit[300]{$\mu$m}\\
  \hline
  \multicolumn{2}{|c|}{\textbf{Silicon bulk parameters}}\\
  \hline
  Silicon bulk type                    & n--type \\
  Silicon lattice orientation\footnote{This is solid--state notation for the
    lattice orientation relative to the surface $\langle
    n_x,n_y,n_z\rangle$.}              & $\langle100\rangle$\\
  Silicon resistivity                  & $\sim$ \unit[5]{k$\Omega{}$cm} \\
  \hline
  \multicolumn{2}{|c|}{\textbf{Silicon mask parameters}}\\
  \hline
  Metal strip width                    & slightly larger than the p+ implant width \\
  Metal strip thickness                & \unit[1]{$\mu$m} Al \\
  p+ strip width/pitch ratio           & 0.20--0.25 \\
  Metal back side                      & \unit[1]{$\mu$m} Al \\
  Passivation on implant side          & \unit[1]{$\mu$m} PECVD \\
  Alignment reference                  & reference mark on implant side mask \\
  \hline
  \multicolumn{2}{|c|}{\textbf{Sensor electrical parameters}}\\
  \hline
  Full Depletion voltage               &  50--\unit[100]{V} \\
  Operational voltage                  & 100--\unit[200]{V} \\
  Breakdown voltage                    & $>$ \unit[200]{V} \\
  Total leakage current                & $<$ \unit[3]{$\mu$A} \\
  Strip leakage current                & $<$ \unit[5]{nA} \\
  Strip coupling capacitance           & 5--\unit[25]{pF} \\
  Polysilicon bias resistors           & $\sim$ \unit[20]{M$\Omega$} \\
  Bad strips                           & $<$ 1\% \\
  \hline
\end{longtable}

In addition to the actual sensor, test structures will be placed on the
unused part of the wafer for quality control purposes.  Examples
of such structures are p--n diodes, small MOS devices, a polysilicon
resistor chain, a.c.\ coupling capacitors and, possibly, a
mini-sensor.

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=.9\textwidth]{fmd/figures/Sistriplayout}
    \includegraphics[width=.9\textwidth]{fmd/figures/Sistriplayout2}
    \caption{Strip layout of the outer (top) and inner (bottom) edge
      of an inner silicon strip sensor. This preliminary design from
      Hamamatsu Photonics K.K.\ shows, in addition to the strip
      structure, the guard and bias rings, the polysilicon bias
      resistors and the rectangular aluminium bonding pads.}
    \label{fig:fmd:si_rings:striplayout}
  \end{center}
\end{figure}


\subsection{Silicon Modules}
\label{sec:fmd:simodules}

Each silicon sensor must be held in place by a rigid system, with
minimal strain on the very thin Si material, in conditions of varying
temperature and humidity. We have chosen to glue the Si sensor (on the
implant side of the silicon material) onto a thin (\unit[1]{mm})
ceramic plate. This in turn is glued onto a PCB hybrid card containing
the front-end (pre-amplifier) electronics circuitry to form a silicon
module.

Both the ceramics plate and the hybrid PC card will be manufactured
with dimensions slightly smaller than the Si sensor so the bonding
pads on the sensor are visible beyond the edge of the cards.  This
will allow for thin wire bonding from the bonding pads on the edge of
the sensor to
corresponding bonding pads on the hybrid cards.  Details of the hybrid
card design and the bonding are given in the electronics section
(\secref{sec:fmd:electronics}).

Gluing the ceramics and bonding the hybrid card to the Si sensor will
be carried out under controlled conditions, allowing for the precise
relative adjustment of the two elements. The further alignment of the
hybrid card in the assembled detector is secured by three small
`legs', which mount the module on the support plate. The alignment
will be made by comparing reference marks on the Si wafer to marks
previously laid out on the hybrid card, to a precision better than
\unit[50]{$\mu$m}.

The sensor strip will be electrically bonded to the hybrid card after
the sensor and hybrid have been glued into a module.  This bonding
will be carried out at CERN.
%%Electrical bonding of the sensor strips to the hybrid card will be
%%done on already glued modules at CERN. 
Bonding pads on sensors and hybrids line up with each other (see
\figref{fig:fmd:si_rings:bonding}).  The relatively large pitch ($=
\unit[250]{\text{$\mu$}m}$ and \unit[500]{$\mu$m}) will allow for
several bonding wires per strip, thereby avoiding badly bonded strips.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.5\textwidth]{fmd/figures/bonding_3d}
  \caption{Illustration of the bonding of the strips on silicon wafers
    to the hybrid card.  The Si sensor is shown at the bottom with
    bonding pads along the edge.  On top is the slightly smaller
    hybrid board with a pitch adapter to which thin wires are bonded.
    Also shown is the bonding of a front-end chip to the pitch adaptor
    and the hybrid card.}
  \label{fig:fmd:si_rings:bonding}
\end{figure}


The assembled detector module is an independent unit that can be
connected to digitizer and read--out electronics, and fully tested.

\subsection{Silicon Half--Rings}

The wedge--shaped modules consisting of silicon sensors and their
hybrid cards are mounted on mechanically stiff half--ring support
plates manufactured from \unit[10]{mm} thick honeycomb plates
laminated with \unit[1]{mm} aluminium on both sides (to provide for
precision drilling of holes). Half--rings of the inner and outer types
hold 5 and 10 silicon modules, respectively.

Silicon modules are attached to the honeycomb support plate by small
supporting legs, see \figref{tabfig:fmd:si_rings:waferhybridplate}.
Dead space between the active surfaces of the Si modules in azimuth
due to inactive edges on the modules is avoided by staggering
neighbouring modules by about \unit[5]{mm} in $z$.  To avoid double
counting of hits, there is no overlap between the active surfaces.

Cables to and from the hybrid cards are supplied via connectors
mounted near the outer rim of the hybrid cards.  These connectors pass
through cut--outs in the honeycomb plate, where they are connected to
the digitizer cards. The digitizer cards will be mounted on the back
of the honeycomb support plate, with matching connectors near their
outer rim.

The silicon half-ring units constitute independent detector units
that can be tested prior to installation.

\enlargethispage{3ex}
\begin{figure}[htpb]
  \begin{center}
    \includegraphics[height=.6\textheight]{fmd/figures/RingAssembly}
    \caption{Exploded view of the assembly of an inner \FMD{}{} ring,
      showing the two honeycomb support plates, the hybrid cards, and
      the Si sensors. On the hybrid cards, the VA front-end chips and
      their pitch adaptors are visible along the two radial edges, the
      connectors for the cables are close to the outer rim, and the
      three support legs attach each module to the support plate.
      Adjacent hybrids and sensors are staggered slightly to allow for
      overlaps in azimuth of the inactive part of the sensors.}
    \label{tabfig:fmd:si_rings:waferhybridplate}
  \end{center}
\end{figure}




%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  sirings tex cholm FMD HV CMS polysilicon Gy Mev MeV Passivation
% LocalWords:  PECVD nA pF pre CERN IGB BSN RO digitizer

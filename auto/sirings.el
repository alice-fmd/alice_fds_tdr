(TeX-add-style-hook "sirings"
 (lambda ()
    (LaTeX-add-labels
     "fig:fmd:sirings:numbering"
     "fig:fmd:si_rings:inandoutsegment"
     "fig:fmd:si_rings:assembly"
     "fig:fmd:si_rings:sensorcut"
     "fig:fmd:si_rings:innersensor"
     "fig:fmd:si_rings:outersensor"
     "tab:fmd:si_rings:sensorspecs"
     "fig:fmd:si_rings:striplayout"
     "sec:fmd:simodules"
     "fig:fmd:si_rings:bonding"
     "tabfig:fmd:si_rings:waferhybridplate")))


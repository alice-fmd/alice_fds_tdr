(TeX-add-style-hook "integration"
 (lambda ()
    (LaTeX-add-labels
     "cha:integration"
     "fig:integration:1"
     "fig:integration:1a"
     "fig:integration:4"
     "fig:integration:2"
     "fig:integration:3"
     "fig:fmd:integration:sequence"
     "fig:fmd:integration:sequence_cont")))


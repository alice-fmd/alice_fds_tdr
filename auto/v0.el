(TeX-add-style-hook "v0"
 (lambda ()
    (LaTeX-add-labels
     "cha:v0")
    (TeX-run-style-hooks
     "v0/objectives"
     "v0/generaldesign"
     "v0/performance"
     "v0/arrays"
     "v0/lightpulse"
     "v0/fee"
     "v0/commissioning"
     "v0/organization"
     "v0/timetable")))


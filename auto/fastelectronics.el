(TeX-add-style-hook "fastelectronics"
 (lambda ()
    (LaTeX-add-labels
     "cha:t0:fastel"
     "fig:t0:fastelectronics:diagram"
     "sec:T0:shoebox"
     "fig:t0:fastelectronics:shoeboxes"
     "fig:t0:fastelectronics:cfd"
     "fig:t0:fastelectronics:tvdc"
     "fig:t0:fastelectronics:vertex"
     "fig:t0:fastelectronics:multiplicity"
     "fig:t0:fastelectronics:timemeaner"
     "fig:t0:fastelectronics:perftimemean"
     "fig:t0:fastelectronics:delay"
     "sec:t0:fastelectronics:qtc"
     "fig:t0:fastelectronics:qtc")))


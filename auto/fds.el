(TeX-add-style-hook "fds"
 (function
  (lambda ()
    (message "Loading FDS style  hooks")
    (LaTeX-add-environments
     "Itemize"
     "Enumerate"
     "Description")
    (TeX-add-symbols
     '("FIXME" ["argument"] 1)
     '("ChapterBib" 1)
     "CVSKeyword"
     "inputfile"
     "printfixme"
     "item"
     "printfixme"
     "tablenewline"
     "baselinestretch"
     "Otemize"
     "Onumerate"
     "Oescription"
     "Nospacing"
     "Topspac"
     "Botspac"
     '("figref" TeX-arg-ref)
     '("tabref" TeX-arg-ref)
     '("secref" TeX-arg-ref)
     '("appref" TeX-arg-ref)
     '("charef" TeX-arg-ref)) 
    (setq TeX-complete-list
        (append '(("\\\\figref{\\([^{}\n\r\\%,\(\\)" 1 LaTex-label-list "}")
		  ("\\\\tabref{\\([^{}\n\r\\%,\(\\)" 1 LaTex-label-list "}")
		  ("\\\\secref{\\([^{}\n\r\\%,\(\\)" 1 LaTex-label-list "}")
		  ("\\\\appref{\\([^{}\n\r\\%,\(\\)" 1 LaTex-label-list "}")
		  ("\\\\secref{\\([^{}\n\r\\%,\(\\)" 1 LaTex-label-list "}"))
		TeX-complete-list))
    )))


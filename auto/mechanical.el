(TeX-add-style-hook "mechanical"
 (function
  (lambda ()
    (LaTeX-add-labels
     "fig:fmd:mechanical:displacment"
     "fig:fmd:mechanical:displacment2"
     "fig:fmd:mechanical:fmdcones1"
     "fig:fmd:mechanical:fmdcones2"
     "fig:fmd:mechanical:fmdcones3"))))


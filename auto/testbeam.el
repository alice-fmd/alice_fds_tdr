(TeX-add-style-hook "testbeam"
 (lambda ()
    (LaTeX-add-labels
     "cha:t0:inibeamtest"
     "fig:t0:testbeam:f6"
     "fig:t0:testbeam:f9"
     "tab:t0:timeres"
     "tab:t0:timeres2")))


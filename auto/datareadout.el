(TeX-add-style-hook "datareadout"
 (lambda ()
    (LaTeX-add-labels
     "fig:t0:datareadout:blread"
     "fig:t0:datareadout:demul"
     "fig:t0:datareadout:dare"
     "fig:t0:datareadout:trm")))


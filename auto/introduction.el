(TeX-add-style-hook "introduction"
 (lambda ()
    (LaTeX-add-labels
     "cha:introduction"
     "fig:introduction:schematic"
     "fig:introduction:planeview"
     "tab:physics:etacoverage"
     "fig:intro:coordinates")))


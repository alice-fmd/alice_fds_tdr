(TeX-add-style-hook "pmt"
 (lambda ()
    (LaTeX-add-labels
     "fig:t0:pmt:f3"
     "fig:t0:pmt:f4"
     "fig:t0:pmt:f5"
     "fig:t0:pmt:magnet")))


(TeX-add-style-hook "bib"
 (lambda ()
    (LaTeX-add-bibitems
     "V0:benjamin"
     "V0:V0note1"
     "V0:ppr"
     "V0:V0note2"
     "V0:pythia"
     "V0:hijing"
     "V0:lhcb"
     "V0:cuautle"
     "V0:kim"
     "V0:star"
     "V0:bicron"
     "V0:kuraray"
     "V0:hamamatsu"
     "V0:nino"
     "V0:hptdc"
     "V0:dcs"
     "V0:caen"
     "V0:radeffects")))


(TeX-add-style-hook "lightpulse"
 (lambda ()
    (LaTeX-add-labels
     "sec:V0:light"
     "sec:V0:PMT"
     "tab:V0:PMT"
     "fig:V0:gain"
     "fig:V0:time"
     "sec:V0:stage")))


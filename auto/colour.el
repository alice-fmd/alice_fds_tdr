(TeX-add-style-hook "colour"
 (lambda ()
    (LaTeX-add-labels
     "fig:colour:overview"
     "fig:colour:schematic"
     "fig:colour:integration:1"
     "fig:fmd:colour:mockup"
     "fig:t0:colour:photo2"
     "fig:t0:colour:T0_integration"
     "fig:t0:colour:shoebox00"
     "fig:t0:colour:patchpanel"
     "fig:v0:colour:V0Cdesign"
     "fig:v0:colour:V0Cphoto3"
     "fig:v0:colour:V0Cphoto1"
     "fig:v0:colour:V0Cphoto2"
     "fig:fmd:colour:conceptuallayout"
     "tabfig:fmd:colour:waferhybridplate"
     "fig:fmd:colour:readouttest"
     "fig:fmd:colour:displacment2")
    (TeX-add-symbols
     "thefigure")))


(TeX-add-style-hook "detector"
 (lambda ()
    (LaTeX-add-labels
     "sec:T0:PMTtube"
     "fig:t0:detector:feu187"
     "sec:t0:detector:cherenkov"
     "fig:t0:detector:wvlenght"
     "sec:t0:detector:arrays"
     "fig:t0:detector:f1WHT"
     "fig:t0:detector:f2ab"
     "fig:t0:detector:f2c"
     "fig:t0:detector:LaserCal1"
     "fig:t0:detector:LaserCal2"
     "fig:t0:detector:LaserCal3"
     "sec:t0:detector:voltages"
     "fig:t0:detector:PMToper1"
     "fig:t0:detector:PMToper2"
     "fig:t0:detector:Muon_abs"
     "fig:t0:detector:T0_integration"
     "fig:t0:detector:Patch_panels")))


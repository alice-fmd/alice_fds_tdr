(TeX-add-style-hook "electronics"
 (lambda ()
    (LaTeX-add-labels
     "sec:fmd:electronics"
     "fig:fmd:electronics:architecture"
     "tab:fmd:electronics:channels"
     "tab:fmd:electronics:fe_specs"
     "fig:fmd:electronics:va_arch"
     "fig:fmd:electronics:va_time"
     "fig:fmd:electronics:va"
     "fig:fmd:electronics:va_noise"
     "tab:fmd:electronics:va_specs"
     "tab:fmd:electronics:controllines"
     "fig:fmd:electronics:hybridlayout"
     "fig:fmd:electronics:digitizer"
     "fig:fmd:electronics:altro_test"
     "fig:fmd:electronics:rcu"
     "fig:fmd:electronics:rcuphoto")))


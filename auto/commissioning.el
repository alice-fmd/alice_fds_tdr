(TeX-add-style-hook "commissioning"
 (lambda ()
    (LaTeX-add-labels
     "sec:V0:commissioning"
     "sec:V0:lyon"
     "fig:V0:adjustmentinlab"
     "sec:V0:cal1"
     "sec:V0:adj1"
     "sec:V0:adj2"
     "fig:V0:HPTDCconnections"
     "fig:V0:timemealignment"
     "fig:V0:clocksandwindows"
     "sec:V0:adj3"
     "sec:V0:cern"
     "fig:V0:adjustmentinpp"
     "sec:V0:adj4"
     "sec:V0:adj5"
     "fig:V0:MIPscanning"
     "sec:V0:cal2"
     "sec:V0:adj6"
     "sec:V0:adj7"
     "sec:V0:adj8"
     "tab:V0:parameters")))


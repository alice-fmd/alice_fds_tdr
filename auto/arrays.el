(TeX-add-style-hook "arrays"
 (lambda ()
    (LaTeX-add-labels
     "sec:V0:V0arrays"
     "sec:V0:V0retd"
     "sec:V0:V0design1"
     "fig:V0:timeresolutionmex"
     "sec:V0:V0design2"
     "fig:V0:V0Cdesign"
     "fig:V0:V0Ctimelight"
     "sec:V0:V0design"
     "sec:V0:V0Aassemb"
     "fig:V0:V0Akeyhole"
     "fig:V0:V0Aconnector"
     "sec:V0:V0Cassemb"
     "fig:V0:V0Cmontage"
     "fig:V0:V0Csector"
     "fig:V0:V0Cbox"
     "fig:V0:V0Ccuts")))


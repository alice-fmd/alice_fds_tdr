(TeX-add-style-hook "fmd"
 (lambda ()
    (LaTeX-add-labels
     "cha:fmd")
    (TeX-run-style-hooks
     "fmd/physics"
     "fmd/designconsiderations"
     "fmd/sirings"
     "fmd/electronics"
     "fmd/integration"
     "fmd/commissioning"
     "fmd/survey"
     "fmd/safety"
     "fmd/time"
     "fmd/organisation")))


(TeX-add-style-hook "montecarlo"
 (lambda ()
    (LaTeX-add-labels
     "fig:t0:montecarlo:f10"
     "fig:t0:montecarlo:f11"
     "fig:t0:montecarlo:f12"
     "fig:t0:montecarlo:f13"
     "fig:t0:montecarlo:f14"
     "tab:t0:ppeff"
     "fig:t0:montecarlo:f16"
     "fig:t0:montecarlo:f17"
     "fig:t0:montecarlo:f18"
     "fig:t0:montecarlo:f19"
     "fig:t0:montecarlo:f20"
     "fig:t0:montecarlo:f21"
     "fig:t0:montecarlo:f22")))


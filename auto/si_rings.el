(TeX-add-style-hook "si_rings"
 (function
  (lambda ()
    (LaTeX-add-labels
     "fig:fmd:si_rings:inandoutsegment"
     "fig:fmd:si_rings:sensorcut"
     "fig:fmd:si_rings:innersensor"
     "fig:fmd:si_rings:outersensor"
     "tab:fmd:si_rings:sensorspecs"
     "fig:fmd:si_rings:bonding"
     "tabfig:fmd:si_rings:waferhybridplate"
     "tab:fmd:si_rings:materialbudget"))))


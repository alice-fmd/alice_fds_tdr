(TeX-add-style-hook "finalbeamtest"
 (lambda ()
    (LaTeX-add-labels
     "fig:t0:finalbeamtest:detconf"
     "fig:t0:finalbeamtest:shoeprot"
     "fig:t0:finalbeamtest:rack"
     "sec:t0:finalbeamtest:quartz"
     "fig:t0:finalbeamtest:tofspec"
     "tab:t0:diam"
     "fig:t0:finalbeamtest:outpmt"
     "fig:t0:finalbeamtest:thresh"
     "fig:t0:finalbeamtest:resp26"
     "fig:t0:finalbeamtest:resp20"
     "fig:t0:finalbeamtest:wave"
     "tab:t0:finalbeamtest:detmod")))


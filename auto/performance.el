(TeX-add-style-hook "performance"
 (lambda ()
    (LaTeX-add-labels
     "sec:V0:perfsim"
     "sec:V0:secondaries"
     "fig:V0:V0vertex"
     "sec:V0:triggerefficiency"
     "fig:V0:V0effic"
     "sec:V0:bbmultiplicity"
     "sec:V0:pp-multiplicity"
     "fig:V0:V0ppmult"
     "sec:V0:PbPb-multiplicity"
     "fig:V0:dnsurdeta"
     "fig:V0:V0pbpbmult"
     "sec:V0:pgasrejection"
     "fig:V0:pppgastime")))


(TeX-add-style-hook "alice_tdr_011"
 (lambda ()
    (TeX-run-style-hooks
     "ifthen"
     "fds"
     "latex2e"
     "alicetdr10"
     "alicetdr"
     "openright"
     "a4paper"
     "tdr-defs"
     "version"
     "fds-title"
     "author"
     "introduction/introduction"
     "t0/t0"
     "v0/v0"
     "fmd/fmd"
     "integration/integration"
     "organization/organization")))


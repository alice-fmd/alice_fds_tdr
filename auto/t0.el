(TeX-add-style-hook "t0"
 (lambda ()
    (LaTeX-add-labels
     "cha:t0")
    (TeX-run-style-hooks
     "t0/naming"
     "t0/physics"
     "t0/designconsiderations"
     "t0/detector"
     "t0/pmt"
     "t0/testbeam"
     "t0/montecarlo"
     "t0/fastelectronics"
     "t0/finalbeamtest"
     "t0/datareadout"
     "t0/detconsys"
     "t0/organization")))


%%
%% $Id: introduction.tex,v 1.48 2004-09-09 16:28:47 cholm Exp $
%%

\chapter{Introduction}
\label{cha:introduction}
\CVSKeyword{$Id: introduction.tex,v 1.48 2004-09-09 16:28:47 cholm Exp $}
\CVSIdShow

\noindent 
The forward detector considered in the present document
comprises:
\begin{itemize}
\item \TZERO{} --- two arrays of \Cherenkov{} radiators.
\item \VZERO{} --- two rings of plastic scintillators.
\item \FMD{}{} --- five rings of silicon strip detectors.
\end{itemize}


The initial concept of \ALICE{} as defined in the Technical Proposal
\cite{intro:multichannel} mentions a set of Micro--Channel Plate (MCP)
detectors as the preferred solution to providing the Level 0 trigger and
multiplicity information in the forward/backward regions ($| \eta | >
1.5$).  The MCP option would have been a novel and elegant way to
build a forward detector system, but it was realized that it would
have required substantial funds and a major R\&D effort and that, the
desired functionality could be achieved with existing and proved
technologies based on \Cherenkov{} radiators, scintillators, and
Si-strip detectors.  This led to the division into the \TZERO{},
the \VZERO{} and the \FMD{}{}, for convenience named the Forward Detectors
(\FWD{}). These systems provide different functionalities, but also to
some extent functional overlap and complementarity, which is
considered an advantage for \ALICE{}.

Since then, work on the three sub-detectors has proceeded
independently, although on issues such as integration, electronics,
simulations, etc. there has always been good co-operation between the
relevant detectors.  Therefore, recognizing the common roots and the physical
proximity of the modules and anticipating even closer collaboration in
the future, it was decided to submit a joint Technical Design Report
on the Forward Detectors (\FMD{}{}, \TZERO{} and \VZERO{} TDR).

\Figref{fig:introduction:schematic} shows schematically the location
and layout of the Forward Detectors together with the Inner Tracking
System and beam pipe. A detailed vertical cut of the same area is
shown in a scaled technical drawing in
\figref{fig:introduction:planeview}.

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{introduction/figures/FWD-3d-bw}
    \caption{Schematic of the placement of \TZERO{}, \VZERO{} and \FMD{}{}
      on both sides of the interaction point of \ALICE{}. The five
      layers of \ITS{} are sketched in the central region.}
    \label{fig:introduction:schematic}
  \end{center}
\end{figure}


\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.95\textwidth]{introduction/figures/fwd-overview}
  %% \includegraphics[width=0.3\textwidth]{introduction/figures/fwd-outer}
  \includegraphics[width=0.3\textwidth]{introduction/figures/fwd-outernewz}
  \includegraphics[width=0.65\textwidth]{introduction/figures/fwd-inner}

  \caption{Plane view of \TZERO{}, \VZERO{} and \FMD{}{} 
    together with the ITS and surrounding detector systems in the
    inner parts of ALICE.}
  \label{fig:introduction:planeview}
\end{figure}

All three detector systems are located at small radial distances from
the beam line. The systems consist of detectors located on both sides
(labelled `A' towards RB24, and labelled `C' towards RB26) of the
interaction point (IP) and at various distances from the IP.  Each system
builds on a small number detector assemblies for maximum modularity,
ease of manufacture, and reduction of cost. The pseudorapidity coverage
of the three detectors is listed in \tabref{tab:physics:etacoverage}.

\begin{table}[htbp]
  \centering
  \caption{Overview of pseudorapidity covered by the three
    forward detectors.}
  \vglue0.2cm
  \begin{tabular}{|l|r@{\space/\space}l|r@{\space/\space}l|}
    \hline
    \textbf{Detector} & 
    \multicolumn{4}{c|}{\textbf{$\mathrm{\eta_{min}/\eta_{max}}$}} \\
    & \multicolumn{2}{c}{A side} & 
    \multicolumn{2}{c|}{C side}\\
    & \multicolumn{2}{c}{(RB24)} & 
    \multicolumn{2}{c|}{(RB26)}\\
    \hline 
    \TZERO{}  & $4.5$ & $5.0$ & $-3.3$ & $-2.9$ \\
    \VZERO{}  & $2.8$ & $5.1$ & $-3.7$ & $-1.7$ \\ 
    \FMD{}{}  & $1.7$ & $5.0$ & $-3.4$ & $-1.7$ \\
    \hline
  \end{tabular}
  \label{tab:physics:etacoverage}
\end{table}

Throughout this document, the coordinate system and definitions laid
out in Ref. \cite{intro:coordinates} will be used. The right--handed
Cartesian system has its origin at the IP and the $z$ axis is parallel to the
main beam direction with positive $z$ in the direction of RB24 of the LHC machine
(opposite the muon absorber). The $x$ points towards the LHC centre,
and the azimuthal angle $\phi$ and polar angle $\theta$ are defined
according to the usual conventions.  Each side of the experiment
from the IP is called A and C, also known as RB24 and
RB26, respectively.  \Figref{fig:intro:coordinates} summarizes these
conventions.  Due to the recent \ALICE{}-wide decision to
change the direction of the $z$ axis,
$z$ and derived quantities (e.g. pseudorapidity $\eta$) are not
necessarily consistent with previous \ALICE{} TDRs
\cite{intro:tdr:hmpid, intro:tdr:phos, intro:tdr:zdc, intro:tdr:its,
  intro:tdr:muon, intro:tdr:pmd, intro:tdr:tpc, intro:tdr:tof,
  intro:tdr:trd, intro:tdr:daq, intro:ppr}
 
\begin{figure}[htbp]
  \centering
  \includegraphics[width=.8\textwidth]{introduction/figures/naming}
  \caption{Definition of the \ALICE{} coordinate system axes,
    polar angles and detector sides taken from Ref.~\cite{intro:coordinates}}
  \label{fig:intro:coordinates}
\end{figure}

In the following we briefly outline the main physics functionality
associated with each of the considered systems.

\section{The \TZERO{} detector}

The \TZERO{} detector consists of 2 arrays of PMTs equipped with
\Cherenkov{} radiators. The arrays are on the opposite sides of the
Interaction Point (IP). The main task of \TZERO{} is to supply fast
timing signals which will be used in the \LVL{0} trigger for \ALICE{}, to provide
a wake-up call for \TRD{} and to deliver collision time reference for
Time-of-Flight (\TOF{}) detector. \TZERO{} covers pseudorapidity range of
$-3.3 < \eta < -2.9$ and $4.5 < \eta < 5$. The time resolution of
\TZERO{} is better than \unit[50]{ps} ($\sigma$). The triggering
efficiency varies from about 50\% for \ppCol{} collisions up to 100\%
for \AACol{} collisions.  The main trigger signal will be
\TZERO{}-vertex confirming the location of the IP within the
pre-defined limits with accuracy of better than \unit[1.5]{cm}.  For
\AACol{} collisions, \TZERO{} will also give fast evaluation of the
multiplicity using a pre-programmed 3-grade scale (minimum bias,
central and semi-central).


\section{The \VZERO{} detector}

The \VZERO{} system consists of 2 disks of modestly segmented (8
segments) plastic scintillator tiles read out by optical fibres. The
pseudorapidity coverage of the \VZERO{} system is approximately equal
to that of the \FMD{}{}, providing redundancy, although the
segmentation is much smaller so the mean number of hits per
detector segment is much higher.  The main functionality of the
\VZERO{} system is to provide the on--line \LVL{0} centrality trigger
for \ALICE{} by setting a threshold on deposited energy, and to provide
a background rejection capability for the di--muon arm. An additional
function is to contribute to the rejection of asymmetric
beam--gas events, although the modest timing performance of this
detector ($\approx \unit[0.6]{ns}$) does not yield precise vertex or
event timing information.

\section{The \FMD{}{} detector}

The \FMD{}{} consists of 51,200 silicon strip channels distributed
over 5 ring counters of two types which have 20 and 40 sectors
each in azimuthal angle, respectively.  The main function of the \FMD{}{}
system is to provide (off--line) precise charged particle multiplicity
information in the pseudorapidity range $-3.4<\eta<-1.7$ and
$1.7< \eta<5.0$.  The read--out time for the system ($\approx
\unit[13]{\mu\text{s}}$) only allows it to participate in the
\ALICE{} trigger hierarchy at \LVL{2} and above.

Together with the pixel system of the \ITS{}, the \FMD{}{} will
provide charged particle multiplicity distributions for all collision
types in the range $-3.4<\eta<5.0$.  Small overlaps between the
various rings and with the \ITS{} inner pixel layer provide redundancy
and important checks of analysis procedures. The maximum mean number
of hits for very central \PbPbCol{} collisions will be less than 3
charged particles per strip. The \FMD{}{} will also allow the
study of multiplicity fluctuations on an event by event basis and enable 
flow analysis (relying on the azimuthal segmentation).

%% Local Variables:
%%   TeX-master: "../alice_tdr_011"
%%   TeX-parse-self: t
%%   TeX-auto-save: t
%%   TeX-style-local: "auto/"
%%   TeX-auto-local: "auto/"
%%   ispell-local-dictionary: "british"
%% End:

% LocalWords:  tex borge FMD TDR
